using System;

namespace AssetOverview.Tests;

public class AssetOverviewTest
{
    private readonly ITestOutputHelper output;

    public AssetOverviewTest(ITestOutputHelper output)
    {
        this.output = output;
    }


    /// <summary>
    /// Test für die Gruppierung in Assets
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1OnlyGrouping()
    {
        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 126, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 127, Coin = "EOS", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);

        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(2, liste.Count);
        Assert.Equal(2, liste[0].Transactions.Count);
        Assert.Equal(1, liste[1].Transactions.Count);
    }

    /// <summary>
    /// Test für die auswahl der Transaktionen für den jeweilig eingegebenen Benutzer
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1OnlyRelevantTransactionsFromCorrectUser()
    {
        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 126, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 127, Coin = "EOS", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);

        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(2, liste.Count);
        Assert.Equal(150, liste[0].Transactions[0].Value);
        Assert.Equal(150, liste[0].Transactions[1].Value);
        Assert.Equal(2, liste[0].Transactions.Count);
    }

    /// <summary>
    /// Test für die Bilder in der AssetOverview
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1Pictures()
    {
        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 126, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 127, Coin = "EOS", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 50, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);
        output.WriteLine(liste[0].LinkToImage);
        output.WriteLine(liste[1].Name);


        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(2, liste.Count);
        Assert.NotNull(liste[0].LinkToImage);
        Assert.NotNull(liste[1].LinkToImage);
        Assert.Equal("https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579", liste[0].LinkToImage);
    }

    /// <summary>
    /// Test für die Haltefrist der Coins in der Asset Overview
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1HoldingPeriod()
    {
        //Transaction trans = new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2022, 7, 4), Tradeable = new DateTime(2023, 7, 4), Deleted = false};

        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 4), Tradeable = new DateTime(2024, 7, 4), Deleted = false},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},

        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);

        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(9, liste[0].Quantity);
        Assert.Equal(6, liste[0].QuantityOverHoldingPeriod);
    }

    /// <summary>
    /// Test für einen Verkauf in der Asset Overview
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1Sale()
    {
        //Transaction trans = new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2022, 7, 4), Tradeable = new DateTime(2023, 7, 4), Deleted = false};

        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Verkauf, Fee = (decimal) 0.50, Rate = 50, Amount = 2, Value = 150, Remainder = 2, OwnerId = 40, PurchaseDate = new DateTime(2022, 7, 4), Tradeable = new DateTime(2023, 7, 4), Deleted = false},
            new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 4), Tradeable = new DateTime(2024, 7, 4), Deleted = false},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},

        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);

        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(7, liste[0].Quantity);
        Assert.Equal(4, liste[0].QuantityOverHoldingPeriod);
    }

    /// <summary>
    /// Test für einen gelöschte Transaktion in der Asset Overview
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Fact]
    public void Test1Deleted()
    {
        //Transaction trans = new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2022, 7, 4), Tradeable = new DateTime(2023, 7, 4), Deleted = false};

        var mockContext = new Mock<MvcAccountContext>();
        var Transactionlist = new List<Transaction>
        {
            new Transaction {
                Id = 123,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Fee = (decimal) 0.50,
                Rate = 50,
                Amount = 3,
                Value = 150,
                Remainder = 3,
                OwnerId = 40,
                PurchaseDate = new DateTime(1997, 7, 4),
                Tradeable = new DateTime(1998, 7, 4),
                Deleted = false},
            new Transaction {Id = 124, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},
            new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Verkauf, Fee = (decimal) 0.50, Rate = 50, Amount = 2, Value = 150, Remainder = 2, OwnerId = 40, PurchaseDate = new DateTime(2022, 7, 4), Tradeable = new DateTime(2023, 7, 4), Deleted = false},
            new Transaction {Id = 130, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 150, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 4), Tradeable = new DateTime(2024, 7, 4), Deleted = true},
            new Transaction {Id = 125, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal) 0.50, Rate = 50, Amount = 3, Value = 500, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(1997, 7, 4), Tradeable = new DateTime(1998, 7, 4), Deleted = false},

        }.AsQueryable();

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(Transactionlist.Provider);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(Transactionlist.Expression);
        mockSet.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(Transactionlist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);

        TransactionContainer cont = new TransactionContainer(mockContext.Object);
        var liste = cont.CreateAssets(40);

        Assert.Equal("Bitcoin", liste[0].Name);
        Assert.Equal("Ethereum", liste[1].Name);
        Assert.Equal(4, liste[0].Quantity);
        Assert.Equal(4, liste[0].QuantityOverHoldingPeriod);
    }

}