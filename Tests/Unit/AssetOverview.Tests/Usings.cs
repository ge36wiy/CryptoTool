global using Xunit;
global using Moq;
global using Projekt.Models;
global using Projekt.Utilities;
global using Microsoft.EntityFrameworkCore;
global using Projekt.Container;
global using Xunit.Abstractions;
global using System.Linq;
