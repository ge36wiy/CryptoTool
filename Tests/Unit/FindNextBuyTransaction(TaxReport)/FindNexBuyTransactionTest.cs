﻿using Projekt.Models;
using Projekt.Utilities;

namespace TaxReportUnitTest
{
    /// <summary>
    /// Hier wird die FindNexBuyTransaction der Klasse TaxReport getestet
    /// Codeowner: Vladyslav Kovganko
    /// </summary>
    public class FindNexBuyTransactionTest
    {
        // Methode die getestet wird.
        // Die Methode gibt den am längsten in der Vergangenheit liegenden Kauf zurück
        // <=> Coins die verkauft werden
        private static Transaction FindNextBuyTransaction(List<Transaction> transactions, string coin)
        {
            foreach (Transaction t in transactions)
            {
                if ((t.Type == TransactionType.Kauf || t.Type == TransactionType.Mining) && t.Coin == coin && t.Remainder != 0)
                {
                    return t;
                }
            }
            throw new GhostCoinException(coin, "Inkonsistente Transaktionsliste");
        }


        /// <summary>
        /// Codeowner: Vladyslav Kovganko
        /// </summary>
        [Fact]
        public void AverageCase()
        {
            List<Transaction> transactions = new List<Transaction>
                {
                    new Transaction { Id = 1000, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false },
                    new Transaction { Id = 1001, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 2, 4), Tradeable = new DateTime(2024, 2, 4), Deleted = false },
                    new Transaction { Id = 1002, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 8, OwnerId = 40, PurchaseDate = new DateTime(2023, 3, 2), Tradeable = new DateTime(2024, 3, 2), Deleted = false },
                    new Transaction { Id = 1002, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1003, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 1230, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false }
                };


            Assert.Equal(1000,FindNextBuyTransaction(transactions, "Bitcoin").Id);
            Assert.Equal(1001,FindNextBuyTransaction(transactions, "cAAVE").Id);
            Assert.Equal(1002,FindNextBuyTransaction(transactions, "Ethereum").Id);
            Assert.Equal(1003,FindNextBuyTransaction(transactions, "Starter.xyz").Id);
        }

        /// <summary>
        /// Test, wenn einige Coins bereits verkauft wurden. (z.B. im Vorjahr)
        /// Codeowner: Vladyslav Kovganko
        /// </summary>
        [Fact]
        public void RemainderIsZero()
        {
            List<Transaction> transactions = new List<Transaction>
                {
                    new Transaction { Id = 1000, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false },
                    new Transaction { Id = 1001, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2023, 2, 4), Tradeable = new DateTime(2024, 2, 4), Deleted = false },
                    new Transaction { Id = 1002, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 8, OwnerId = 40, PurchaseDate = new DateTime(2023, 3, 2), Tradeable = new DateTime(2024, 3, 2), Deleted = false },
                    new Transaction { Id = 1003, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1004, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 1230, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false }
                };


            Assert.Equal(1000, FindNextBuyTransaction(transactions, "Bitcoin").Id);
            Assert.Equal(1002, FindNextBuyTransaction(transactions, "cAAVE").Id);
            Assert.Equal(1003, FindNextBuyTransaction(transactions, "Ethereum").Id);
            Assert.Equal(1004, FindNextBuyTransaction(transactions, "Starter.xyz").Id);
        }

        /// <summary>
        /// Keine Coins stehen zur Verfügung 
        /// Codeowner: Vladyslav Kovganko
        /// </summary>
        [Fact]
        public void GhostCoinException1()
        {
            List<Transaction> transactions = new List<Transaction>
            {
                new Transaction { Id = 1000, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false }
            };

            try
            {
                FindNextBuyTransaction(transactions, "Dogecoin");
                Assert.True(false);
            }
            catch (Exception ex)
            {
                Assert.True(ex is GhostCoinException);
            }
        }

        /// <summary>
        /// Keine Coins stehen zur Verfügung 
        /// Codeowner: Vladyslav Kovganko
        /// </summary>
        [Fact]
        public void GhostCoinException2()
        {
            List<Transaction> transactions = new List<Transaction>
                {
                    new Transaction { Id = 1000, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false },
                    new Transaction { Id = 1001, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2023, 2, 4), Tradeable = new DateTime(2024, 2, 4), Deleted = false },
                    new Transaction { Id = 1002, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2023, 3, 2), Tradeable = new DateTime(2024, 3, 2), Deleted = false },
                    new Transaction { Id = 1003, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1004, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 1230, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false }
                };

            try
            {
                FindNextBuyTransaction(transactions, "cAAVE");
                Assert.True(false);
            }
            catch (Exception ex)
            {
                Assert.True(ex is GhostCoinException);
            }
        }
    }
}