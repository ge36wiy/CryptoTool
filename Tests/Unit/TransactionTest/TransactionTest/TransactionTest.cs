using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Projekt.Utilities;
using System.Security.Claims;
using System.Security.Principal;
using Projekt.ViewModels;

namespace TransactionTest
{
    public class TransactionTest
    {
        [Fact]
        public void CreateValid()
        {
            Transaction createdTransaction = null;

            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "test", Password = "pw", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var transactionlist = new List<Transaction>().AsQueryable();
            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(transactionlist.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(transactionlist.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(transactionlist.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => transactionlist.GetEnumerator());
            mockSetTransaction.Setup(m => m.Add(It.IsAny<Transaction>())).Callback((Transaction transaction) => createdTransaction = transaction);      //F�ngt an die Datenbank �bergebene Transaktion ab

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("test", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };


            Transaction originalTransaction = new Transaction()
            {
                Id = 42,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Amount = 100,
                Fee = 2,
                OwnerId = 1,
                Rate = 0.1,
                Deleted = true,
                PurchaseDate = DateTime.UtcNow,
                Value = 50,
                Tradeable = DateTime.UtcNow,
                Remainder = 11,
            };
            var res = testController.Create(originalTransaction);

            //Asserts f�r R�ckgabe
            Assert.IsType<RedirectToActionResult>(res);
            RedirectToActionResult rtar = (RedirectToActionResult)res;
            Assert.Equal("TransactionsOfCurrentUser", rtar.ActionName);
            Assert.Equal("Transaction", rtar.ControllerName);

            //Asserts f�r �bergebene Transaktion
            Assert.NotNull(createdTransaction);
            Assert.Equal(originalTransaction.Id, createdTransaction.Id);
            Assert.Equal(originalTransaction.Type, createdTransaction.Type);
            Assert.Equal(originalTransaction.Amount, createdTransaction.Amount);
            Assert.Equal(originalTransaction.Coin, createdTransaction.Coin);
            Assert.Equal(originalTransaction.PurchaseDate, createdTransaction.PurchaseDate);
            Assert.False(createdTransaction.Deleted);
            Assert.Equal(createdTransaction.PurchaseDate.AddYears(1), createdTransaction.Tradeable);
        }
        [Fact]
        public void CreateSellWithInsuffiecentAsset()
        {
            Transaction createdTransaction = null;

            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "test", Password = "pw", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var transactionlist = new List<Transaction>
            {
                new Transaction()
            {
                Id = 42,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Amount = 100,
                Fee = 2,
                OwnerId = 1,
                Rate = 0.1,
                Deleted = true,
                PurchaseDate = DateTime.UtcNow,
                Value = 50,
                Tradeable = DateTime.UtcNow,
                Remainder = 11,
            }
        }.AsQueryable();
            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(transactionlist.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(transactionlist.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(transactionlist.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => transactionlist.GetEnumerator());
            mockSetTransaction.Setup(m => m.Add(It.IsAny<Transaction>())).Verifiable();                  //Erm�glicht �berpr�fung von DB-Zugriff

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("test", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };


            Transaction originalTransaction = new Transaction()
            {
                Id = 42,
                Coin = "Bitcoin",
                Type = TransactionType.Verkauf,
                Amount = 110,
                Fee = 2,
                OwnerId = 1,
                Rate = 0.1,
                Deleted = true,
                PurchaseDate = DateTime.UtcNow,
                Value = 50,
                Tradeable = DateTime.UtcNow,
                Remainder = 11,
            };
            var res = testController.Create(originalTransaction);

            //Asserts f�r R�ckgabe
            Assert.IsType<ViewResult>(res);
            Assert.IsType<TransactionViewModel>(((ViewResult)res).Model);

            //Sicherstellen, dass keine Transaktion hinzugef�gt wurde
            mockSetTransaction.Verify(m => m.Add(It.IsAny<Transaction>()), Times.Never);
        }
        [Fact]
        public void EditValid()
        {
            Transaction createdTransaction = new()
            {
                Id = 404,
                Amount = 404
            };
            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "test", Password = "pw", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var transactionlist = new List<Transaction>
            {
                new Transaction()
            {
                Id = 42,
                Coin = "Bitcoin",
                Type = TransactionType.Kauf,
                Amount = 100,
                Fee = 2,
                OwnerId = 1,
                Rate = 0.1,
                Deleted = true,
                PurchaseDate = DateTime.Today,
                Value = 50,
                Tradeable = DateTime.Today,
                Remainder = 11,
            }
        }.AsQueryable();
            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(transactionlist.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(transactionlist.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(transactionlist.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => transactionlist.GetEnumerator());
            mockSetTransaction.Setup(m => m.Update(It.IsAny<Transaction>())).Callback((Transaction transaction) => createdTransaction = transaction);

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("test", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };
            var res = testController.Edit(42, new TransactionViewModel()
            {
                Transaction = new Transaction()
                {
                    Id = 42,
                    Coin = "Bitcoin",
                    Type = TransactionType.Kauf,
                    Amount = 150,                  //ge�nderte Menge
                    Fee = 2,
                    OwnerId = 1,
                    Rate = 0.1,
                    Deleted = true,
                    PurchaseDate = DateTime.Today,
                    Value = 50,
                    Tradeable = DateTime.Today,
                    Remainder = 11,
                }
            });

            //Asserts f�r R�ckgabe
            Assert.IsType<RedirectToActionResult>(res);
            RedirectToActionResult rtar = (RedirectToActionResult)res;
            Assert.Equal("TransactionsOfCurrentUser", rtar.ActionName);

            //Asserts f�r an DB �bergebene Transaktion
            Assert.NotNull(createdTransaction);
        }

        //CodeOwner: Sebastian Klee
        //Testet TransactionController.Delete Methode, wenn Transaktion problemlos gelöscht werden darf
        [Fact]
        public void DeleteValid()
        {
            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var TransactionList = new List<Transaction>
        {
            new Transaction {Id = 5, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 1, PurchaseDate = DateTime.Now, Tradeable = DateTime.Now.AddDays(366), Deleted = false},
        }.AsQueryable();

            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(TransactionList.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(TransactionList.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(TransactionList.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => TransactionList.GetEnumerator());
            mockSetTransaction.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => TransactionList.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("cu1", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };


            var result = testController.Delete(5);
            Assert.IsType<ViewResult>(result);

            var res = (ViewResult)result;
            TransactionViewModel vm = (TransactionViewModel)res.ViewData.Model;
            Assert.Equal(1, vm._Customer.Id);
            Assert.Equal(5, vm.Transaction.Id);
            Assert.True(vm.isValid);
        }

        //CodeOwner: Sebastian Klee
        //Testet TransactionController.Delete Methode, wenn Transaktion zu negativen Assets führt
        [Fact]
        public void DeleteInValid()
        {
            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var TransactionList = new List<Transaction>
        {
            new Transaction {Id = 5, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 1, PurchaseDate = DateTime.Now, Tradeable = DateTime.Now.AddDays(366), Deleted = false},
            new Transaction {Id = 6, Coin = "Bitcoin", Type = TransactionType.Verkauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 1, PurchaseDate = DateTime.Now.AddDays(1), Tradeable = DateTime.Now.AddDays(367), Deleted = false},
        }.AsQueryable();

            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(TransactionList.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(TransactionList.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(TransactionList.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => TransactionList.GetEnumerator());
            mockSetTransaction.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => TransactionList.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("cu1", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };


            var result = testController.Delete(5);
            Assert.IsType<ViewResult>(result);

            var res = (ViewResult)result;
            TransactionViewModel vm = (TransactionViewModel)res.ViewData.Model;
            Assert.Equal(1, vm._Customer.Id);
            Assert.Equal(5, vm.Transaction.Id);
            Assert.False(vm.isValid);
        }

        //CodeOwner: Sebastian Klee
        //Testet TransactionController.Delete Methode, wenn Transaktion von falschem Kunden gelöscht wird
        [Fact]
        public void DeleteFalseUser()
        {
            var mockContext = new Mock<MvcAccountContext>();

            var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

            var mockSetCustomer = new Mock<DbSet<Customer>>();
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
            mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
            mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

            var TransactionList = new List<Transaction>
        {
            new Transaction {Id = 5, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 1, PurchaseDate = DateTime.Now, Tradeable = DateTime.Now.AddDays(366), Deleted = false},
            new Transaction {Id = 6, Coin = "Bitcoin", Type = TransactionType.Verkauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 1, PurchaseDate = DateTime.Now.AddDays(1), Tradeable = DateTime.Now.AddDays(367), Deleted = false},
        }.AsQueryable();

            var mockSetTransaction = new Mock<DbSet<Transaction>>();
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(TransactionList.Provider);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(TransactionList.Expression);
            mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(TransactionList.ElementType);
            mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => TransactionList.GetEnumerator());
            mockSetTransaction.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => TransactionList.FirstOrDefault(d => d.Id == (int)ids[0]));

            mockContext.Setup(m => m.SaveChanges()).Verifiable();
            mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
            mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

            var testController = new TransactionController(mockContext.Object);

            var identity = new GenericIdentity("cu2", "test");
            var contextUser = new ClaimsPrincipal(identity);
            testController.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext { User = contextUser }
            };


            var result = testController.Delete(5);
            Assert.IsType<RedirectToActionResult>(result);
        }
    }
}