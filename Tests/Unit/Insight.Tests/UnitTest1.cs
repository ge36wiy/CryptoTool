using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projekt.Controllers;
using Projekt.ViewModels;

namespace Insight.Tests;

public class UnitTest1
{


    //CodeOwner: Sebastian Klee
    //Testet Insight Methode, wenn Einsicht gewährt wurde
    [Fact]
    public void TestInsightCorrect()
    {
        var mockContext = new Mock<MvcAccountContext>();

        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        var consultantlist = new List<Consultant>
        {
            new Consultant {Id=3, Username = "co1", Password = "co1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Consultant {Id=4, Username = "co2", Password = "co2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetConsultant = new Mock<DbSet<Consultant>>();
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(consultantlist.Provider);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(consultantlist.Expression);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(consultantlist.ElementType);
        mockSetConsultant.As<IQueryable<Consultant>>().Setup(m => m.GetEnumerator()).Returns(() => consultantlist.GetEnumerator());

        var ConsultantToCustomerList = new List<ConsultantToCustomer>
        {
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 1, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.None},
        }.AsQueryable();

        var mockSetConsultantToCustomer = new Mock<DbSet<ConsultantToCustomer>>();
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(ConsultantToCustomerList.Expression);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(ConsultantToCustomerList.ElementType);
        mockSetConsultantToCustomer.As<IQueryable<ConsultantToCustomer>>().Setup(m => m.GetEnumerator()).Returns(() => ConsultantToCustomerList.GetEnumerator());


        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
        mockContext.Setup(c => c.Consultant).Returns(mockSetConsultant.Object);
        mockContext.Setup(c => c.ConsultantToCustomer).Returns(mockSetConsultantToCustomer.Object);

        var testController = new ConsultantController(mockContext.Object, null);

        var identity = new GenericIdentity("co1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        var result = testController.Insight(1);
        Assert.IsType<ViewResult>(result);

        var res = (ViewResult)result;
        InsightViewModel vm = (InsightViewModel)res.ViewData.Model;
        Assert.Equal(1, vm._Customer.Id);
        Assert.Equal(3, vm._Consultant.Id);

    }


    //CodeOwner: Sebastian Klee
    //Testet Insight Methode, wenn Einsicht nicht gewährt wurde
    [Fact]
    public void TestInsightIncorrect()
    {
        var mockContext = new Mock<MvcAccountContext>();

        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        var consultantlist = new List<Consultant>
        {
            new Consultant {Id=3, Username = "co1", Password = "co1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Consultant {Id=4, Username = "co2", Password = "co2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetConsultant = new Mock<DbSet<Consultant>>();
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(consultantlist.Provider);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(consultantlist.Expression);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(consultantlist.ElementType);
        mockSetConsultant.As<IQueryable<Consultant>>().Setup(m => m.GetEnumerator()).Returns(() => consultantlist.GetEnumerator());

        var ConsultantToCustomerList = new List<ConsultantToCustomer>
        {
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 1, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.None},
        }.AsQueryable();

        var mockSetConsultantToCustomer = new Mock<DbSet<ConsultantToCustomer>>();
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(ConsultantToCustomerList.Expression);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(ConsultantToCustomerList.ElementType);
        mockSetConsultantToCustomer.As<IQueryable<ConsultantToCustomer>>().Setup(m => m.GetEnumerator()).Returns(() => ConsultantToCustomerList.GetEnumerator());


        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
        mockContext.Setup(c => c.Consultant).Returns(mockSetConsultant.Object);
        mockContext.Setup(c => c.ConsultantToCustomer).Returns(mockSetConsultantToCustomer.Object);

        var testController = new ConsultantController(mockContext.Object, null);

        var identity = new GenericIdentity("co1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        var result = testController.Insight(2);
        Assert.IsType<RedirectToActionResult>(result);
    }


    //CodeOwner: Sebastian Klee
    //Testet ExtendedInsight Methode, wenn erweiterte Einsicht gewährt wurde
    [Fact]
    public void TestExtendedInsightCorrect()
    {
        var mockContext = new Mock<MvcAccountContext>();

        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        var consultantlist = new List<Consultant>
        {
            new Consultant {Id=3, Username = "co1", Password = "co1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Consultant {Id=4, Username = "co2", Password = "co2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetConsultant = new Mock<DbSet<Consultant>>();
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(consultantlist.Provider);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(consultantlist.Expression);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(consultantlist.ElementType);
        mockSetConsultant.As<IQueryable<Consultant>>().Setup(m => m.GetEnumerator()).Returns(() => consultantlist.GetEnumerator());

        var ConsultantToCustomerList = new List<ConsultantToCustomer>
        {
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 1, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.None},
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 2, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.Insight},
        }.AsQueryable();

        var mockSetConsultantToCustomer = new Mock<DbSet<ConsultantToCustomer>>();
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(ConsultantToCustomerList.Expression);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(ConsultantToCustomerList.ElementType);
        mockSetConsultantToCustomer.As<IQueryable<ConsultantToCustomer>>().Setup(m => m.GetEnumerator()).Returns(() => ConsultantToCustomerList.GetEnumerator());

        var TransactionList = new List<Transaction>
        {
            new Transaction {Id = 1, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 2, PurchaseDate = DateTime.Now, Tradeable = DateTime.Now.AddDays(366), Deleted = false},
        }.AsQueryable();

        var mockSetTransaction = new Mock<DbSet<Transaction>>();
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(TransactionList.Expression);
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(TransactionList.ElementType);
        mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => TransactionList.GetEnumerator());


        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
        mockContext.Setup(c => c.Consultant).Returns(mockSetConsultant.Object);
        mockContext.Setup(c => c.ConsultantToCustomer).Returns(mockSetConsultantToCustomer.Object);
        mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

        var testController = new ConsultantController(mockContext.Object, null);

        var identity = new GenericIdentity("co1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        var result = testController.ExtendedInsight(2);
        Assert.IsType<ViewResult>(result);
        var res = (ViewResult)result;
        InsightViewModel vm = (InsightViewModel)res.ViewData.Model;
        Assert.Equal(2, vm._Customer.Id);
        Assert.Equal(3, vm._Consultant.Id);
        Assert.Equal("Bitcoin", vm.AssetList[0].Name);
    }

    //CodeOwner: Sebastian Klee
    //Testet ExtendedInsight Methode, wenn erweiterte Einsicht nicht gewährt wurde
    [Fact]
    public void TestExtendedInsightIncorrect()
    {
        var mockContext = new Mock<MvcAccountContext>();

        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cu1", Password = "cu1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Customer {Id=2, Username = "cu2", Password = "cu2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        var consultantlist = new List<Consultant>
        {
            new Consultant {Id=3, Username = "co1", Password = "co1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
            new Consultant {Id=4, Username = "co2", Password = "co2", Firstname = "B", Lastname = "B", Address = "B", Email = "B", Pathtoimage = "B"},
        }.AsQueryable();

        var mockSetConsultant = new Mock<DbSet<Consultant>>();
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(consultantlist.Provider);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(consultantlist.Expression);
        mockSetConsultant.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(consultantlist.ElementType);
        mockSetConsultant.As<IQueryable<Consultant>>().Setup(m => m.GetEnumerator()).Returns(() => consultantlist.GetEnumerator());

        var ConsultantToCustomerList = new List<ConsultantToCustomer>
        {
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 1, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.None},
            new ConsultantToCustomer {IdConsultant = 3, IdCustomer = 2, InsightStatusSimple = InsightStatus.Insight, InsightStatusExtended=InsightStatus.Insight},
        }.AsQueryable();

        var mockSetConsultantToCustomer = new Mock<DbSet<ConsultantToCustomer>>();
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(ConsultantToCustomerList.Expression);
        mockSetConsultantToCustomer.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(ConsultantToCustomerList.ElementType);
        mockSetConsultantToCustomer.As<IQueryable<ConsultantToCustomer>>().Setup(m => m.GetEnumerator()).Returns(() => ConsultantToCustomerList.GetEnumerator());

        var TransactionList = new List<Transaction>
        {
            new Transaction {Id = 1, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = 0, Rate = 20123, Amount = 2, Value = 40246, Remainder = 2, OwnerId = 2, PurchaseDate = DateTime.Now, Tradeable = DateTime.Now.AddDays(366), Deleted = false},
        }.AsQueryable();

        var mockSetTransaction = new Mock<DbSet<Transaction>>();
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Provider).Returns(ConsultantToCustomerList.Provider);
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.Expression).Returns(TransactionList.Expression);
        mockSetTransaction.As<IQueryable<MvcAccountContext>>().Setup(m => m.ElementType).Returns(TransactionList.ElementType);
        mockSetTransaction.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => TransactionList.GetEnumerator());


        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);
        mockContext.Setup(c => c.Consultant).Returns(mockSetConsultant.Object);
        mockContext.Setup(c => c.ConsultantToCustomer).Returns(mockSetConsultantToCustomer.Object);
        mockContext.Setup(c => c.Transaction).Returns(mockSetTransaction.Object);

        var testController = new ConsultantController(mockContext.Object, null);

        var identity = new GenericIdentity("co1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        var result = testController.ExtendedInsight(1);
        Assert.IsType<RedirectToActionResult>(result);
    }
}