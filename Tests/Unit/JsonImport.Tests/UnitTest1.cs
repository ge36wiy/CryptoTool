using System;
using System.Text;
using System.Text.Json;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using Projekt.Controllers;
namespace JsonImportTest;
/// <summary>
/// Die Klasse <c>UnitTest1</c> überprüft den JSON upload mithilfe eines Unit-Tests
/// CodeOwner: Alexander Rioux
/// </summary>
public class UnitTest1
{
    /// <summary>
    /// Überprüft die Eingabe einer richtigen JSON
    /// CodeOwner: Alexander Rioux
    /// </summary>
    [Fact]
    public void TestRichtigeJson()
    {

        var mockContext = new Mock<MvcAccountContext>();


        var Transactionslist = new List<Transaction>
        { }.AsQueryable();
        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cus1", Password = "cus1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
        }.AsQueryable();

        string jsonData = @"[
            {
                ""Coin"": ""Bitcoin"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            },
            {
                ""Coin"": ""Bitcoin"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1998-07-04T00:00:00""
            },
            {
                ""Coin"": ""Ethereum"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            },
            {
                ""Coin"": ""EOS"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            }
        ]";

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.Provider).Returns(Transactionslist.Provider);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.Expression).Returns(Transactionslist.Expression);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.ElementType).Returns(Transactionslist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionslist.GetEnumerator());

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);

        var mockFormFile = new Mock<IFormFile>();
        mockFormFile.Setup(f => f.FileName).Returns("testFile.json");
        mockFormFile.Setup(f => f.Length).Returns(jsonData.Length);
        mockFormFile.Setup(f => f.OpenReadStream()).Returns(new MemoryStream(Encoding.UTF8.GetBytes(jsonData)));


        var testController = new TransactionController(mockContext.Object);

        var identity = new GenericIdentity("cus1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        testController.ImportFile(mockFormFile.Object);

        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(4));
        mockSet.Verify(m => m.Add(It.IsAny<Transaction>()), Times.Exactly(4));
    }
    /// <summary>
    /// Überprüft die Eingabe einer fehlerhaften JSON 
    /// Wichtig es werden bei einer fehlerhaften JSON auch richtige Werte ignoriert
    /// CodeOwner: Alexander Rioux
    /// </summary>
    [Fact]
    public void TestFehlerhafteJson()
    {

        var mockContext = new Mock<MvcAccountContext>();


        var Transactionslist = new List<Transaction>
        { }.AsQueryable();
        var customerlist = new List<Customer>
        {
            new Customer {Id=1, Username = "cus1", Password = "cus1", Firstname = "A", Lastname = "A", Address = "A", Email = "A", Pathtoimage = "A"},
        }.AsQueryable();

        string jsonData = @"[
            {
                ""Coin"": ""EOS"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            },
            {
                ""Coin"": ""Bitcoaaaaaaaaaaain"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            },
            {
                ""Coin"": ""Bitcoin"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": -150.0,
                ""PurchaseDate"": ""1998-07-04T00:00:00""
            },
            {
                ""Coin"": ""Ethereum"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04Tadsfhg00:00:00""
            },
            {
                ""Coin"": ""EOS"",
                ""Type"": ""Kaasgaguf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            },
            {
                ""Coin"": ""EOS"",
                ""Type"": ""Kauf"",
                ""Fee"": 0.5,
                ""Rate"": 50.0,
                ""Amount"": 3.0,
                ""Value"": 150.0,
                ""PurchaseDate"": ""1997-07-04T00:00:00""
            }
        ]";

        var mockSet = new Mock<DbSet<Transaction>>();
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.Provider).Returns(Transactionslist.Provider);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.Expression).Returns(Transactionslist.Expression);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.ElementType).Returns(Transactionslist.ElementType);
        mockSet.As<IQueryable<Transaction>>().Setup(m => m.GetEnumerator()).Returns(() => Transactionslist.GetEnumerator());

        var mockSetCustomer = new Mock<DbSet<Customer>>();
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.Provider).Returns(customerlist.Provider);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.Expression).Returns(customerlist.Expression);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.ElementType).Returns(customerlist.ElementType);
        mockSetCustomer.As<IQueryable<Customer>>().Setup(m => m.GetEnumerator()).Returns(() => customerlist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Transaction).Returns(mockSet.Object);
        mockContext.Setup(c => c.Customer).Returns(mockSetCustomer.Object);

        var mockFormFile = new Mock<IFormFile>();
        mockFormFile.Setup(f => f.FileName).Returns("testFile.json");
        mockFormFile.Setup(f => f.Length).Returns(jsonData.Length);
        mockFormFile.Setup(f => f.OpenReadStream()).Returns(new MemoryStream(Encoding.UTF8.GetBytes(jsonData)));


        var testController = new TransactionController(mockContext.Object);

        var identity = new GenericIdentity("cus1", "test");
        var contextUser = new ClaimsPrincipal(identity);
        testController.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext { User = contextUser }
        };

        testController.ImportFile(mockFormFile.Object);

        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(0));
        mockSet.Verify(m => m.Add(It.IsAny<Transaction>()), Times.Exactly(0));

    }

}