using System.Security.Cryptography.Xml;

namespace TaxReportNamespace
{
    public class TaxReportTest
    {
        /// <summary>
        /// Erstellung der Transaktionslisten, die den TaxReport Konstruktor �bergeben wird
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="TestCase"> Entscheidet dar�ber welche Liste an Transaktionen �bergeben werden soll, je nachdem welchen Fall man testen m�chte</param>
        public static List<Transaction> InitTransactionList(int TestCase)
        {
            // Transaktionen, alle im Kalenderjahr und Steuerrelevant, nur K�ufe
            if (TestCase == 0) {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1000, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false },
                    new Transaction { Id = 1001, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 2, 4), Tradeable = new DateTime(2024, 2, 4), Deleted = false },
                    new Transaction { Id = 1002, Coin = "Litecoin", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 8, OwnerId = 40, PurchaseDate = new DateTime(2023, 3, 2), Tradeable = new DateTime(2024, 3, 2), Deleted = false },
                    new Transaction { Id = 1002, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1003, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 1230, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false }
                };
                return Transactionlist;
            }
            //Transaktionen, alle im Kalenderjahr und Steuerrelevant, K�ufe und Verk�ufe
            else if (TestCase == 1)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1004, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0.5, OwnerId = 40, PurchaseDate = new DateTime(2023, 1, 1),Tradeable = new DateTime(2024, 1, 1), Deleted = false },
                    new Transaction { Id = 1005, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 2, 4), Tradeable = new DateTime(2024, 2, 4), Deleted = false },
                    new Transaction { Id = 1006, Coin = "Litecoin", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 8, OwnerId = 40, PurchaseDate = new DateTime(2023, 3, 2), Tradeable = new DateTime(2024, 3, 2), Deleted = false },
                    new Transaction { Id = 1007, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1008, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 1230, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false },
                    new Transaction { Id = 1009, Coin = "Ethereum", Type = TransactionType.Verkauf, Fee = (decimal)1.00, Rate = 8000, Amount = 2.35, Value = 18800, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 6), Tradeable = new DateTime(2024, 5, 6), Deleted = false },
                    new Transaction { Id = 1010, Coin = "cAAVE", Type = TransactionType.Verkauf, Fee = (decimal)2.50, Rate = 2, Amount = 112, Value = 224.00, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 7), Tradeable = new DateTime(2024, 7, 7), Deleted = false }
                };
                return Transactionlist;
            }
            //Transaktionen, Steuerrelevant und Steuerunrelevant, nur K�ufe
            else if (TestCase == 2)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1011, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2020, 1, 1),Tradeable = new DateTime(2021, 1, 1), Deleted = false },
                    new Transaction { Id = 1012, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2021, 2, 4), Tradeable = new DateTime(2022, 2, 4), Deleted = false },
                    new Transaction { Id = 1013, Coin = "Litecoin", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2022, 3, 2), Tradeable = new DateTime(2023, 3, 2), Deleted = false },
                    new Transaction { Id = 1014, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2013, 5, 5), Tradeable = new DateTime(2014, 5, 5), Deleted = false },
                    new Transaction { Id = 1015, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2003, 6, 6), Tradeable = new DateTime(2004, 6, 6), Deleted = false }
                };
                return Transactionlist;
            }
            //Transaktionen, Steuerrelevant und Steuerunrelevant, Verk�ufe und K�ufe
            else if (TestCase == 3)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1017, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2021, 2, 4), Tradeable = new DateTime(2022, 2, 4), Deleted = false },
                    new Transaction { Id = 1018, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1019, Coin = "Ethereum", Type = TransactionType.Verkauf, Fee = (decimal)1.00, Rate = 8000, Amount = 2.35, Value = 18800, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false },
                    new Transaction { Id = 1020, Coin = "cAAVE", Type = TransactionType.Verkauf, Fee = (decimal)2.50, Rate = 2, Amount = 112, Value = 224.00, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 7), Tradeable = new DateTime(2024, 7, 7), Deleted = false }
                };
                return Transactionlist;
            }
            //FiFo Prinzip
            else if (TestCase == 4)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1017, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 4000, Amount = 1, Value = 4000, Remainder = 1, OwnerId = 40, PurchaseDate = new DateTime(2022, 4, 4), Tradeable = new DateTime(2023, 4, 4), Deleted = false },
                    new Transaction { Id = 1018, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 5000, Amount = 2, Value = 10000, Remainder = 2, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1019, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 6000, Amount = 3, Value = 18000, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1020, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.50, Rate = 8000, Amount = 3, Value = 24000, Remainder = 3, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2024, 5, 5), Deleted = false },
                    new Transaction { Id = 1021, Coin = "Ethereum", Type = TransactionType.Verkauf, Fee = (decimal)1.00, Rate = 10000, Amount = 7, Value = 70000, Remainder = 7, OwnerId = 40, PurchaseDate = new DateTime(2023, 6, 6), Tradeable = new DateTime(2024, 6, 6), Deleted = false },
                };
                return Transactionlist;
            }
            // Freigrenze �berpr�fen
            else if (TestCase == 5)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1017, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 4000, Amount = 1, Value = 4000, Remainder = 1, OwnerId = 40, PurchaseDate = new DateTime(2023, 4, 4), Tradeable = new DateTime(2023, 4, 4), Deleted = false },
                    new Transaction { Id = 1018, Coin = "Ethereum", Type = TransactionType.Verkauf, Fee = (decimal)1.00, Rate = 4500, Amount = 1, Value = 4000, Remainder = 1, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2023, 5, 5), Deleted = false }
                };
                return Transactionlist;
            }
            //Ghostcoin Exception
            else if (TestCase == 6)
            {
                List<Transaction> Transactionlist = new List<Transaction>
                {
                    new Transaction { Id = 1016, Coin = "Bitcoin", Type = TransactionType.Kauf, Fee = (decimal)0.50, Rate = 35000, Amount = 0.5, Value = 17500, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2020, 1, 1),Tradeable = new DateTime(2021, 1, 1), Deleted = false },
                    new Transaction { Id = 1017, Coin = "cAAVE", Type = TransactionType.Kauf, Fee = (decimal)2.50, Rate = 1.35, Amount = 112, Value = 151.20, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2021, 2, 4), Tradeable = new DateTime(2022, 2, 4), Deleted = false },
                    new Transaction { Id = 1018, Coin = "Litecoin", Type = TransactionType.Kauf, Fee = (decimal)12.50, Rate = 170, Amount = 8, Value = 1360.0, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2022, 3, 2), Tradeable = new DateTime(2023, 3, 2), Deleted = false },
                    new Transaction { Id = 1019, Coin = "Ethereum", Type = TransactionType.Kauf, Fee = (decimal)1.00, Rate = 7565, Amount = 2.35, Value = 17777.75, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 5), Tradeable = new DateTime(2014, 5, 5), Deleted = false },
                    new Transaction { Id = 1020, Coin = "Starter.xyz", Type = TransactionType.Kauf, Fee = (decimal)0.00, Rate = 0.0023, Amount = 1230, Value = 2.829, Remainder = 0, OwnerId = 40, PurchaseDate = new DateTime(2003, 6, 6), Tradeable = new DateTime(2004, 6, 6), Deleted = false },
                    new Transaction { Id = 1009, Coin = "Ethereum", Type = TransactionType.Verkauf, Fee = (decimal)1.00, Rate = 8000, Amount = 2.35, Value = 18800, Remainder = 2.35, OwnerId = 40, PurchaseDate = new DateTime(2023, 5, 6), Tradeable = new DateTime(2024, 5, 6), Deleted = false },
                    new Transaction { Id = 1010, Coin = "cAAVE", Type = TransactionType.Verkauf, Fee = (decimal)2.50, Rate = 2, Amount = 112, Value = 224.00, Remainder = 112, OwnerId = 40, PurchaseDate = new DateTime(2023, 7, 7), Tradeable = new DateTime(2024, 7, 7), Deleted = false }
                };
                return Transactionlist;
            } else
            {
                return new List<Transaction>();
            }

        }

        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 0 : Transaktionen, alle im Kalenderjahr und Steuerrelevant, nur K�ufe
        ///      
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase0_ReturnsTrue()
        {
            List<Transaction> transactionList = InitTransactionList(0);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            Assert.Equal(0, taxReport.profit);
            Assert.Equal(0, taxReport.tax_relevant_profit);
            Assert.Equal(0, taxReport.total_profit);
            Assert.Equal(0, taxReport.fees);
            Assert.Equal(0, taxReport.mining_profit);
            //Dictonary
        }

        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 1 : Transaktionen, alle im Kalenderjahr und Steuerrelevant, K�ufe und Verk�ufe
        /// 
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase1_ReturnsTrue()
        {
            double coinProfitEthereum = 0;
            double coinTaxRelevantProfitEthereum = 0;
            double coinProfitcAAVE = 0;
            double coinTaxRelevantProfitcAAVE = 0;
            List<Transaction> transactionList = InitTransactionList(1);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            Assert.Equal(0, taxReport.profit);
            Assert.Equal(1088.05, taxReport.tax_relevant_profit);
            Assert.Equal(1088.05, taxReport.total_profit);
            Assert.Equal(7, taxReport.fees);
            Assert.Equal(0, taxReport.mining_profit);

            //Ethereum Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["Ethereum"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitEthereum += profitPair.profit;
                    }
                    else
                    {
                        coinTaxRelevantProfitEthereum += profitPair.profit;
                    }

                }
            }
            //cAAVE Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["cAAVE"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitcAAVE += profitPair.profit;
                    } else
                    {
                        coinTaxRelevantProfitcAAVE += profitPair.profit;
                    }
                }
            }
            Assert.Equal(0, coinProfitEthereum);
            Assert.Equal(1020.25, coinTaxRelevantProfitEthereum);
            Assert.Equal(0, coinProfitcAAVE);
            // 0.01 � Rundungstoleranz
            Assert.Equal(67.80, coinTaxRelevantProfitcAAVE, 0.01);
        }


        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 2 : Transaktionen,unterschiedliche Kalenderjahr, Steuerrelevant und Steuerirrlevant, nur K�ufe        
        /// 
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase2_ReturnsTrue()
        {
            List<Transaction> transactionList = InitTransactionList(2);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            Assert.Equal(0, taxReport.profit);
            Assert.Equal(0, taxReport.tax_relevant_profit);
            Assert.Equal(0, taxReport.total_profit);
            Assert.Equal(0, taxReport.fees);
            Assert.Equal(0, taxReport.mining_profit);
            //Dictonary
        }




        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 3 : Transaktionen, alle unterschiedliche Kalenderjahre Steuerrelevant und Steuerirrelevante, K�ufe und Verk�ufe
        /// 
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase3_ReturnsTrue()
        {
            double coinProfitEthereum = 0;
            double coinTaxRelevantProfitEthereum = 0;
            double coinProfitcAAVE = 0;
            double coinTaxRelevantProfitcAAVE = 0;
            List<Transaction> transactionList = InitTransactionList(3);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(67.8, taxReport.profit ,0.01);
            Assert.Equal(1020.25 , taxReport.tax_relevant_profit, 0.01);
            Assert.Equal(1088.05, taxReport.total_profit);
            Assert.Equal(7, taxReport.fees);
            Assert.Equal(0, taxReport.mining_profit);

            //Ethereum Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["Ethereum"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitEthereum += profitPair.profit;
                    }
                    else
                    {
                        coinTaxRelevantProfitEthereum += profitPair.profit;
                    }

                }
            }
            //cAAVE Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["cAAVE"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitcAAVE += profitPair.profit;
                    }
                    else
                    {
                        coinTaxRelevantProfitcAAVE += profitPair.profit;
                    }
                }
            }
            Assert.Equal(0, coinProfitEthereum);
            Assert.Equal(1020.25, coinTaxRelevantProfitEthereum);
            // 0.01 � Rundungstoleranz
            Assert.Equal(67.8, coinProfitcAAVE, 0.01);
            Assert.Equal(0, coinTaxRelevantProfitcAAVE); 
        }

        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 4 : Testet FiFo Prinzip
        ///         
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase4_ReturnsTrue()
        {
            double coinProfitEthereum = 0;
            double coinTaxRelevantProfitEthereum = 0;
            List<Transaction> transactionList = InitTransactionList(4);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(5998.85, taxReport.profit, 0.01);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(23996.64, taxReport.tax_relevant_profit, 0.01);
            Assert.Equal(29995.5, taxReport.total_profit);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(4.5, (double) taxReport.fees, 0.01);
            Assert.Equal(0, taxReport.mining_profit);

            //Ethereum Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["Ethereum"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitEthereum += profitPair.profit;
                    }
                    else
                    {
                        coinTaxRelevantProfitEthereum += profitPair.profit;
                    }
                }
            }
            Assert.Equal(5998.85, coinProfitEthereum, 0.01);
            Assert.Equal(23996.64, coinTaxRelevantProfitEthereum, 0.01);
        }

        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 5 : Testet die Steuer Freigrenze 
        /// 
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase5_ReturnsTrue()
        {
            double coinProfitEthereum = 0;
            double coinTaxRelevantProfitEthereum = 0;
            List<Transaction> transactionList = InitTransactionList(5);
            TaxReport taxReport = new TaxReport(2023, transactionList);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(498, taxReport.profit, 0.01);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(0, taxReport.tax_relevant_profit, 0.01);
            Assert.Equal(498, taxReport.total_profit);
            //0.01� Toleranz f�r Rundungsfehler
            Assert.Equal(2, (double)taxReport.fees, 0.01);
            Assert.Equal(0, taxReport.mining_profit);

            //Ethereum Profit / TaxRelevant Profit f�r alle einzelnen Verk�ufe, gemaess dem FiFo Prinzip, zusammenaddieren 
            foreach (Sell sell in taxReport.tables["Ethereum"])
            {
                foreach (BuyProfitPair profitPair in sell.buyProfits)
                {
                    if (!profitPair.taxRelevant)
                    {
                        coinProfitEthereum += profitPair.profit;
                    }
                    else
                    {
                        coinTaxRelevantProfitEthereum += profitPair.profit;
                    }
                }
            }
            // Steuerirrelevanter Umsatz
            Assert.Equal(0, coinProfitEthereum, 0.01);
            // Steuerrelevanter Umsatz
            Assert.Equal(498, coinTaxRelevantProfitEthereum, 0.01);
        }

        /// <summary>
        /// Unit Test f�r die Steuerauswertung. TestCase 6 : Wirft Ghostcoin Ausnahme
        ///         
        /// TaxReport.profit = Steuerirrelevanter Profit �ber alle Tranksaktionen kumuliert
        /// TaxReport.tax_relevant_profit = Steuerrelevanter Gewinn �ber alle Tranksaktionen kumuliert
        /// TaxReport.total_profit = Umsatz gesamt (irrelevant + relevant) �ber alle Tranksaktionen kumuliert
        /// TaxReport.fees = Geb�hren �ber alle Tranksaktionen kumuliert
        /// coinProfit<CoinName> = Steuerirrelevanter Profit aller Transaktionen mit Coin "<CoinName>"
        /// coinTaxRelevantProfit<CoinName> = Steuerlich Relevanter Profit aller Transaktion mit Coin "<CoinName>"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void TaxReport_TestCase6_ReturnsTrue()
        {
            List<Transaction> transactionList = InitTransactionList(6);
            Assert.Throws<GhostCoinException>(() => new TaxReport(2023, transactionList));
        }
    }
}

