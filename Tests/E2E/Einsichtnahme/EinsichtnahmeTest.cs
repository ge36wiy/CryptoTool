namespace Einsichtnahme
{
    /// <summary>
    /// E2E Testklasse um Interaktionen mit Zuweisungen und Einsichtnahmen zu testen.
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class EinsichtnahmeTest
    {

        //Login Daten f�r die E2E Tests
        public static string consultantUsername = "testsb";
        public static string consultantPassword = "testsb";
        public static string customerUsername = "test";
        public static string customerPassword = "test";
        public static string adminUsername = "admin";
        public static string adminPassword = "Team08";
        public static string loginUrl = "http://sopro.isse.de:49861/Account/Login";
        public static string logoutUrl = "http://sopro.isse.de:49861/Account/Logout";
        //Namen der f�r die E2E Tests Relevanten Buttons, sind f�r die einfache Einsichtnahme Beantragen Funktion wichtig
        public static string erweiterteEinsichtButton = "ErweiterteEinsichtnahme40";
        public static string erweiterteEinsichtDropDown = "ErweiterteEinsichtnahmeAnfragen40";
        public static string einfacheEinsichtButton = "EinfacheEinsichtnahme40";
        public static string einfacheEinsichtDropDown = "EinfacheEinsichtnahmeAnfragen40";
        public static string einfacheEinsichtbestaetigen = "EinfacheEinsichtnahmeBestaetigen39";
        public static string erweiterteEinsichtbestaetigen = "ErweiterteEinsichtnahmeBestaetigen39";
        public static string einfacheEinsichtAblehnen = "EinfacheEinsichtnahmeAblehnen39";
        public static string erweiterteEinsichtAblehnen = "ErweiterteEinsichtnahmeAblehnen39";
        public static string einfacheEinsichtWiderrufenButton = "EinfacheEinsichtnahmeWiderrufen39";
        public static string erweiterteEinsichtWiderrufenButton = "ErweiterteEinsichtnahmeWiderrufen39";



        /// <summary>
        /// Login Set Up Funktion, �ffnet einen Webdriver, gibt Login Daten ein. Wird von den TestFunktionen als SetUp verwendet
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="UserName">Username der in das Username Input Feld eingef�gt wird</param>
        /// <param name="PasswordString">Passwort das in das Passwort Input Feld eingef�gt wird</param>
        public static IWebDriver LoginSetUp(string UserName, String PasswordString)
        {
            IWebDriver webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl(loginUrl);

            IWebElement Username = webDriver.FindElement(By.Name("Username"));
            IWebElement Password = webDriver.FindElement(By.Id("form2Example2"));

            Username.SendKeys(UserName);
            Password.SendKeys(PasswordString);

            IWebElement button = webDriver.FindElement(By.ClassName("btn"));
            button.Click();
            return webDriver;
        }

        /// <summary>
        /// Entfernt die Zuweisung zwischen Test Steuerberater und Test Kunden
        /// CodeOwner: Johannes Bulun
        /// </summary>
        public static void deleteRelation()
        {
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            webDriver.Navigate().GoToUrl("http://sopro.isse.de:49861/Assigned/AddCustomerRelation");
            IWebElement customer = webDriver.FindElement(By.Id("CustomerDataList"));
            IWebElement consultant = webDriver.FindElement(By.Id("ConsultantDataList"));

            customer.SendKeys("40 Dieter  Gerald");
            consultant.SendKeys("39 Hans Peter");

            IWebElement button = webDriver.FindElement(By.Id("loeschen"));
            button.Click();

            webDriver.Quit();
        }

        /// <summary>
        /// Beginn beim Steuerberater: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der entweder eine Einfache Einsichtnahme oder eine Erweiterte Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, und Bearbeitet im Anschluss die View, wie er es m�chte.
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="DropDownOeffnen">Oeffnet das Dropdownmenue um die Einfache/Erweiterte Einsichtnahme zu beantragen</param>
        /// <param name="EinsichtAnfragen">Zum finden des Anfragen Knopfs f�r die Einfache/Erweiterte Einsichtnahme</param>
        /// <param name="EinsichtnahmeVerarbeiten"> Kunde best�tigt/lehnt die Einfache/Erweiterte Einsichtnahme ab</param>

        public static IWebDriver einfacheEinsichtnahmeBeantragen(string DropDownOeffnen, string EinsichtAnfragen, string EinsichtnahmeVerarbeiten)
        {
            deleteRelation();
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            webDriver.Navigate().GoToUrl("http://sopro.isse.de:49861/Assigned/AddCustomerRelation");
            IWebElement customer = webDriver.FindElement(By.Id("CustomerDataList"));
            IWebElement consultant = webDriver.FindElement(By.Id("ConsultantDataList"));

            customer.SendKeys("40 Dieter  Gerald");
            consultant.SendKeys("39 Hans Peter");

            IWebElement button = webDriver.FindElement(By.Id("hinzufuegen"));

            button.Click();

            IWebElement Einsichtbutton = webDriver.FindElement(By.Name(DropDownOeffnen));
            IWebElement EinsichtDropDownbutton = webDriver.FindElement(By.Name(EinsichtAnfragen));

            EinsichtDropDownbutton.Click();
            Einsichtbutton.Click();

            webDriver.Quit();

            IWebDriver webDriver2 = LoginSetUp(customerUsername, customerPassword);

            webDriver2.Navigate().GoToUrl("http://sopro.isse.de:49861/Customer/AccessGrantedOverview");

            IWebElement Einsichtbestaetigen = webDriver2.FindElement(By.Name(EinsichtnahmeVerarbeiten));

            Einsichtbestaetigen.Click();

            
            return webDriver2;
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine Einfache Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, Bestaetigt die Anfrage. 
        /// Validierung: Kunde hat M�glichkeit auf Knopf: "Einsichtnahe Widerrufen" zu klicken, welcher nur erscheint, wenn die Einsichtnahme im Vorfeld bestaetigt wurde
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void einfacheEinsichtnahme_EinsichtnahmeBestaetigen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(einfacheEinsichtDropDown, einfacheEinsichtButton, einfacheEinsichtbestaetigen);
            IWebElement EinsichtWiderrufenButton = webDriver.FindElement(By.Name(einfacheEinsichtWiderrufenButton));

            Assert.True(EinsichtWiderrufenButton.Enabled && EinsichtWiderrufenButton.Displayed);
            webDriver.Quit();
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine Erweiterte Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, Bestaetigt die Anfrage. 
        /// Validierung: Kunde hat M�glichkeit auf Knopf: "Einsichtnahe Widerrufen" zu klicken, welcher nur erscheint, wenn die Einsichtnahme im Vorfeld bestaetigt wurde
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void erweiterteteEinsichtnahme_EinsichtnahmeBestaetigen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(erweiterteEinsichtDropDown, erweiterteEinsichtButton, erweiterteEinsichtbestaetigen);
            IWebElement EinsichtWiderrufenButton = webDriver.FindElement(By.Name(erweiterteEinsichtWiderrufenButton));

            Assert.True(EinsichtWiderrufenButton.Enabled && EinsichtWiderrufenButton.Displayed);
            webDriver.Quit();
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine Einfache Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, lehnt die Anfrage ab. 
        /// Validierung: Einsicht Knopf hat Text: "Keine Einsicht"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void einfacheEinsichtnahme_EinsichtnahmeAblehnnen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(einfacheEinsichtDropDown, einfacheEinsichtButton, einfacheEinsichtAblehnen);

            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();

            IWebDriver webDriver2 = LoginSetUp(consultantUsername, consultantPassword);

            IWebElement ValidateButtonText = webDriver2.FindElement(By.Name(einfacheEinsichtButton));

            Assert.Equal("keine Einsicht", ValidateButtonText.Text);
            webDriver2.Quit();
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine Erweiterte Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, lehnt die Anfrage ab. 
        /// Validierung: Einsicht Knopf hat Text: "Keine Einsicht"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void erweiterteEinsichtnahme_EinsichtnahmeAblehnnen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(erweiterteEinsichtDropDown, erweiterteEinsichtButton, erweiterteEinsichtAblehnen);

            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();

            IWebDriver webDriver2 = LoginSetUp(consultantUsername, consultantPassword);

            IWebElement ValidateButtonText = webDriver2.FindElement(By.Name(erweiterteEinsichtButton));

            Assert.Equal("keine Einsicht", ValidateButtonText.Text);
            webDriver2.Quit();
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine einfache Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, bestaetigt die Einsichtnahme, um dann direkt die Einsichtnahme zu Widerrufen
        /// Validierung: Einsicht Knopf hat Text: "Keine Einsicht"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void einfacheEinsichtnahme_EinsichtnahmeWiderufen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(einfacheEinsichtDropDown, einfacheEinsichtButton, einfacheEinsichtbestaetigen);
            
            IWebElement EinsichtWiderrufenButton = webDriver.FindElement(By.Name(einfacheEinsichtWiderrufenButton));
            EinsichtWiderrufenButton.Click();

            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();

            IWebDriver webDriver2 = LoginSetUp(consultantUsername, consultantPassword);

            IWebElement ValidateButtonText = webDriver2.FindElement(By.Name(einfacheEinsichtButton));

            Assert.Equal("keine Einsicht", ValidateButtonText.Text);
            webDriver2.Quit();
        }

        /// <summary>
        /// Beginn: Zuweisung zwischen Kunden und Steuerberater entfernen. 
        /// Als n�chstes: Der Steuerberater legt zun�chst die Zuweisung des Kunden und sich selbst fest. Im n�chsten Schritt beantragt der Steuerberater eine Erweiterte Einsichtnahme. Log sich im Anschluss aus.
        /// Kunde: Kunde Meldet sich an, springt zu der Einsicht Verwalten View, bestaetigt die Einsichtnahme, um dann direkt die Einsichtnahme zu Widerrufen
        /// Validierung: Einsicht Knopf hat Text: "Keine Einsicht"
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void erweiterteEinsichtnahme_EinsichtnahmeWiderufen_ReturnsTrue()
        {
            deleteRelation();
            IWebDriver webDriver = einfacheEinsichtnahmeBeantragen(erweiterteEinsichtDropDown, erweiterteEinsichtButton, erweiterteEinsichtbestaetigen);

            IWebElement EinsichtWiderrufenButton = webDriver.FindElement(By.Name(erweiterteEinsichtWiderrufenButton));
            EinsichtWiderrufenButton.Click();

            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();

            IWebDriver webDriver2 = LoginSetUp(consultantUsername, consultantPassword);

            IWebElement ValidateButtonText = webDriver2.FindElement(By.Name(erweiterteEinsichtButton));

            Assert.Equal("keine Einsicht", ValidateButtonText.Text);
            webDriver2.Quit();
        }
    }
}