using Microsoft.AspNetCore.Mvc.Testing;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Chrome;
using System.Reflection.Metadata;

namespace E2E
{
    /// <summary>
    /// E2E Testklasse um die Login Interaktionen zu testen.
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class Login
    {
        //Login Daten f�r die E2E Tests
        public static string consultantUsername = "testsb";
        public static string consultantPassword = "testsb";
        public static string customerUsername = "test";
        public static string customerPassword = "test";
        public static string adminUsername = "admin";
        public static string adminPassword = "Team08";
        public static string loginUrl = "http://sopro.isse.de:49861/Account/Login";
        public static string logoutUrl = "http://sopro.isse.de:49861/Account/Logout";

        /// <summary>
        /// Login Set Up Funktion, �ffnet einen Webdriver, gibt Login Daten ein. Wird von den TestFunktionen als SetUp verwendet
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="UserName">Username der in das Username Input Feld eingef�gt wird</param>
        /// <param name="PasswordString">Passwort das in das Passwort Input Feld eingef�gt wird</param>
        public static IWebDriver LoginSetUp(string UserName, String PasswordString)
        {
            IWebDriver webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl(loginUrl);

            IWebElement Username = webDriver.FindElement(By.Name("Username"));
            IWebElement Password = webDriver.FindElement(By.Id("form2Example2"));

            Username.SendKeys(UserName);
            Password.SendKeys(PasswordString);

            IWebElement button = webDriver.FindElement(By.ClassName("btn"));
            button.Click();
            return webDriver;
        }


        /// <summary>
        /// E2E Test f�r den Consultant Login
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LoginConsultant()
        {
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            Assert.EndsWith("/Consultant/OwnCustomers", webDriver.Url);
            webDriver.Quit();
        }


        /// <summary>
        /// E2E Test f�r den Customer Login
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LoginCustomer()
        {
            IWebDriver webDriver = LoginSetUp(customerUsername, customerPassword);
            Assert.EndsWith("/Customer/AssetOverview", webDriver.Url);
            webDriver.Quit();
        }


        /// <summary>
        /// E2E Test f�r den Admin Login
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LoginAdmin()
        {
            IWebDriver webDriver = LoginSetUp(adminUsername, adminPassword);
            Assert.EndsWith("/Admin/RegisteredAccounts", webDriver.Url);
            webDriver.Quit();
        }

        /// <summary>
        /// E2E Test f�r Login mit falschen Daten. �berpr�ft die Data Validation Attribute
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void WrongLogin()
        {
            IWebDriver webDriver = LoginSetUp("WrongUsername", "WrongPassword");
            string errorMessage = webDriver.FindElement(By.ClassName("validation")).Text;
            Assert.True(errorMessage == "Ung�ltige Zugangsdaten");
            webDriver.Quit();
        }



        /// <summary>
        /// E2E Test f�r den Consultant Log out
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LogoutConsultant()
        {
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            Assert.EndsWith("/Consultant/OwnCustomers", webDriver.Url);
            webDriver.Navigate().GoToUrl(logoutUrl);
            Assert.EndsWith("/Account/Login", webDriver.Url);
            webDriver.Quit();
        }


        /// <summary>
        /// E2E Test f�r den Customer LogOut
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LogoutCustomer()
        {
            IWebDriver webDriver = LoginSetUp(customerUsername, customerPassword);
            Assert.EndsWith("/Customer/AssetOverview", webDriver.Url);
            webDriver.Navigate().GoToUrl(logoutUrl);
            Assert.EndsWith("/Account/Login", webDriver.Url);
            webDriver.Quit();
        }

        /// <summary>
        /// E2E Test f�r den Admin LogOut
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void LogoutAdmin()
        {
            IWebDriver webDriver = LoginSetUp(adminUsername, adminPassword);
            Assert.EndsWith("/Admin/RegisteredAccounts", webDriver.Url);
            webDriver.Navigate().GoToUrl(logoutUrl);
            Assert.EndsWith("/Account/Login", webDriver.Url);
            webDriver.Quit();
        }
    }
}



