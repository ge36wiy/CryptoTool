namespace NutzerdatenTest
{
    /// <summary>
    /// E2E Testklasse um die Interaktionen mit Nutzerdaten zu testen
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class NutzerdatenTest
    {


        //Login Daten f�r die E2E Tests
        public static string consultantUsername = "testsb";
        public static string consultantPassword = "testsb";
        public static string customerUsername = "test";
        public static string customerPassword = "test";
        public static string adminUsername = "admin";
        public static string adminPassword = "Team08";
        public static string loginUrl = "http://sopro.isse.de:49861/Account/Login";
        public static string logoutUrl = "http://sopro.isse.de:49861/Account/Logout";
        public static string customerProfile = "http://sopro.isse.de:49861/Customer/Profile";
        public static string customerFirstName = "Test";
        public static string customerLastName = "Kunde";
        public static string customerEmail = "Dieter.Gerald@gmail.com";
        public static string customerAddress = "Feldweg 134";

        public static string consultantProfile = "http://sopro.isse.de:49861/Consultant/Profile";
        public static string consultantFirstName = "Test";
        public static string consultantLastName = "Steuerberater";
        public static string consultantEmail = "Hans.Peter@hotmail.com";
        public static string consultantAddress = "Rockenfeldweg 12";


        /// <summary>
        /// Login Set Up Funktion, �ffnet einen Webdriver, gibt Login Daten ein. Wird von den TestFunktionen als SetUp verwendet
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="UserName">Username der in das Username Input Feld eingef�gt wird</param>
        /// <param name="PasswordString">Passwort das in das Passwort Input Feld eingef�gt wird</param>
        public static IWebDriver LoginSetUp(string UserName, String PasswordString)
        {
            IWebDriver webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl(loginUrl);

            IWebElement Username = webDriver.FindElement(By.Name("Username"));
            IWebElement Password = webDriver.FindElement(By.Id("form2Example2"));

            Username.SendKeys(UserName);
            Password.SendKeys(PasswordString);

            IWebElement button = webDriver.FindElement(By.ClassName("btn"));
            button.Click();
            return webDriver;
        }

        /// <summary>
        /// Setzt die Nutzerdaten des Testusers auf die Korrekten Werte zurueck
        /// CodeOwner: Johannes Bulun
        /// </summary>
        public static void CorrectCustomer()
        {
            IWebDriver webDriver = LoginSetUp(customerUsername, customerPassword);
            webDriver.Navigate().GoToUrl(customerProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement saveButton = webDriver.FindElement(By.Id("save-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys(customerFirstName);
            inputLastName.SendKeys(customerLastName);
            inputEmailAddress.SendKeys(customerEmail);
            inputAddress.SendKeys(customerAddress);
            saveButton.Click();
            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();
        }

        /// <summary>
        /// Setzt die Nutzerdaten des Testusers auf die Korrekten Werte zurueck
        /// CodeOwner: Johannes Bulun
        /// </summary>
        public static void CorrectConsultant()
        {
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            webDriver.Navigate().GoToUrl(consultantProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement saveButton = webDriver.FindElement(By.Id("save-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys(consultantFirstName);
            inputLastName.SendKeys(consultantLastName);
            inputEmailAddress.SendKeys(consultantEmail);
            inputAddress.SendKeys(consultantAddress);

            saveButton.Click();
            webDriver.Navigate().GoToUrl(logoutUrl);
            webDriver.Quit();
        }

        /// <summary>
        /// Testet das aendern von Nutzerdaten fuer den Kunden. Aendert dafuer die Nutzerdaten und speichert sie ab. Anschliessend werden die Inputfelder auf ihren Inhalt validiert
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void KundenDatenEditieren()
        {
            CorrectCustomer();
            IWebDriver webDriver = LoginSetUp(customerUsername, customerPassword);
            webDriver.Navigate().GoToUrl(customerProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement saveButton = webDriver.FindElement(By.Id("save-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys("OtherName");
            inputLastName.SendKeys("OtherLastName");
            inputEmailAddress.SendKeys("Other.Name@gmail.com");
            inputAddress.SendKeys("Otherstreet 123");
            saveButton.Click();

            IWebElement validateInputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement validateInputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement validateInputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement validateInputAddress = webDriver.FindElement(By.Id("inputAddress"));

            Thread.Sleep(5000);
            Assert.Equal("OtherName", validateInputFirstName.GetAttribute("value"));
            Assert.Equal("OtherLastName", validateInputLastName.GetAttribute("value"));
            Assert.Equal("Other.Name@gmail.com", validateInputEmailAddress.GetAttribute("value"));
            Assert.Equal("Otherstreet 123", validateInputAddress.GetAttribute("value"));

            webDriver.Navigate().GoToUrl(logoutUrl);
            CorrectCustomer();
            webDriver.Quit();
        }

        /// <summary>
        /// Testet das aendern von Nutzerdaten fuer den Kunden. Aendert dafuer die Nutzerdaten und drueckt auf abbrechen um das Aendern zuruckzusetzen. Anschliessend werden die Inputfelder auf ihren Inhalt validiert
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void KundenDatenEditierenCancel()
        {
            CorrectCustomer();
            IWebDriver webDriver = LoginSetUp(customerUsername, customerPassword);
            webDriver.Navigate().GoToUrl(customerProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement cancelButton = webDriver.FindElement(By.Id("cancel-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys("OtherName");
            inputLastName.SendKeys("OtherLastName");
            inputEmailAddress.SendKeys("Other.Name@gmail.com");
            inputAddress.SendKeys("Otherstreet 123");
            cancelButton.Click();

            IWebElement validateInputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement validateInputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement validateInputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement validateInputAddress = webDriver.FindElement(By.Id("inputAddress"));

            Assert.Equal(customerFirstName, validateInputFirstName.GetAttribute("value"));
            Assert.Equal(customerLastName, validateInputLastName.GetAttribute("value"));
            Assert.Equal(customerEmail, validateInputEmailAddress.GetAttribute("value"));
            Assert.Equal(customerAddress, validateInputAddress.GetAttribute("value"));


            webDriver.Navigate().GoToUrl(logoutUrl);
            CorrectCustomer();
            webDriver.Quit();
        }


        /// <summary>
        /// Testet das aendern von Nutzerdaten fuer den Steuerberater. Aendert dafuer die Nutzerdaten und speichert sie ab. Anschliessend werden die Inputfelder auf ihren Inhalt validiert
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void SteuerberaterDatenEditieren()
        {
            CorrectConsultant();
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            webDriver.Navigate().GoToUrl(consultantProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement saveButton = webDriver.FindElement(By.Id("save-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys("ConsultantOtherName");
            inputLastName.SendKeys("ConsultantOtherLastName");
            inputEmailAddress.SendKeys("ConsultantOther.Name@gmail.com");
            inputAddress.SendKeys("ConsultantOtherstreet 123");
            saveButton.Click();

            IWebElement validateInputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement validateInputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement validateInputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement validateInputAddress = webDriver.FindElement(By.Id("inputAddress"));

            Thread.Sleep(5000);
            Assert.Equal("ConsultantOtherName", validateInputFirstName.GetAttribute("value"));
            Assert.Equal("ConsultantOtherLastName", validateInputLastName.GetAttribute("value"));
            Assert.Equal("ConsultantOther.Name@gmail.com", validateInputEmailAddress.GetAttribute("value"));
            Assert.Equal("ConsultantOtherstreet 123", validateInputAddress.GetAttribute("value"));

            webDriver.Navigate().GoToUrl(logoutUrl);
            CorrectConsultant();
            webDriver.Quit();
        }


        /// <summary>
        /// Testet das aendern von Nutzerdaten fuer den Steuerberater. Aendert dafuer die Nutzerdaten und drueckt auf abbrechen um das Aendern zuruckzusetzen. Anschliessend werden die Inputfelder auf ihren Inhalt validiert
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void SteuerberaterDatenEditierenCancel()
        {
            CorrectConsultant();
            IWebDriver webDriver = LoginSetUp(consultantUsername, consultantPassword);
            webDriver.Navigate().GoToUrl(consultantProfile);

            IWebElement editButton = webDriver.FindElement(By.Id("edit-btn"));
            IWebElement cancelButton = webDriver.FindElement(By.Id("cancel-btn"));
            IWebElement inputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement inputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement inputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement inputAddress = webDriver.FindElement(By.Id("inputAddress"));

            Thread.Sleep(5000);

            editButton.Click();
            inputFirstName.Clear();
            inputLastName.Clear();
            inputEmailAddress.Clear();
            inputAddress.Clear();

            inputFirstName.SendKeys("ConsultantOtherName");
            inputLastName.SendKeys("ConsultantOtherLastName");
            inputEmailAddress.SendKeys("ConsultantOther.Name@gmail.com");
            inputAddress.SendKeys("ConsultantOtherstreet 123");
            cancelButton.Click();

            IWebElement validateInputFirstName = webDriver.FindElement(By.Id("inputFirstName"));
            IWebElement validateInputLastName = webDriver.FindElement(By.Id("inputLastName"));
            IWebElement validateInputEmailAddress = webDriver.FindElement(By.Id("inputEmailAddress"));
            IWebElement validateInputAddress = webDriver.FindElement(By.Id("inputAddress"));

            Assert.Equal(consultantFirstName, validateInputFirstName.GetAttribute("value"));
            Assert.Equal(consultantLastName, validateInputLastName.GetAttribute("value"));
            Assert.Equal(consultantEmail, validateInputEmailAddress.GetAttribute("value"));
            Assert.Equal(consultantAddress, validateInputAddress.GetAttribute("value"));

            webDriver.Navigate().GoToUrl(logoutUrl);
            CorrectConsultant();
            webDriver.Quit();
        }
    }
}