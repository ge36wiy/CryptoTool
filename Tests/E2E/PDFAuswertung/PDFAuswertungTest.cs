namespace PDFAuswertung
{
    /// <summary>
    /// E2E Testklasse um die Interaktion fuer den PDF Download zu testen.
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class PDFAuswertungTest
    {
        //Login Daten f�r die E2E Tests
        public static string consultantUsername = "testsb";
        public static string consultantPassword = "testsb";
        public static string customerUsername = "test";
        public static string customerPassword = "test";
        public static string adminUsername = "admin";
        public static string adminPassword = "Team08";
        public static string loginUrl = "http://sopro.isse.de:49861/Account/Login";
        public static string logoutUrl = "http://sopro.isse.de:49861/Account/Logout";

        /// <summary>
        /// Login Set Up Funktion, �ffnet einen Webdriver, gibt Login Daten ein. Wird von den TestFunktionen als SetUp verwendet
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="UserName">Username der in das Username Input Feld eingef�gt wird</param>
        /// <param name="PasswordString">Passwort das in das Passwort Input Feld eingef�gt wird</param>
        public static IWebDriver LoginSetUp(string UserName, String PasswordString)
        {
            IWebDriver webDriver = new ChromeDriver();

            webDriver.Navigate().GoToUrl(loginUrl);

            IWebElement Username = webDriver.FindElement(By.Name("Username"));
            IWebElement Password = webDriver.FindElement(By.Id("form2Example2"));

            Username.SendKeys(UserName);
            Password.SendKeys(PasswordString);

            IWebElement button = webDriver.FindElement(By.ClassName("btn"));
            button.Click();
            return webDriver;
        }


        /// <summary>
        /// E2E Test um das Herunterladen des Steuerberichts zu testen
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void CustomerPDFExport_ReturnsTrue()
        {
            
            IWebDriver driver = LoginSetUp(customerUsername, customerPassword);
            driver.Navigate().GoToUrl("http://sopro.isse.de:49861/Customer/TaxReport");
            Thread.Sleep(1000);

            IWebElement button = driver.FindElement(By.ClassName("btn"));
            button.Click();
            Thread.Sleep(1000);

            IWebElement printButton = driver.FindElement(By.Id("print-btn"));

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("setTimeout(function() { window.print(); }, 0);");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(3000);
            string JSPath = "document.querySelector('body>print-preview-app').shadowRoot.querySelector('#sidebar').shadowRoot.querySelector('print-preview-button-strip').shadowRoot.querySelector('cr-button.action-button')";
            Thread.Sleep(5000);
            IWebElement cancelBtn = (IWebElement)js.ExecuteScript($"return {JSPath}");
            Thread.Sleep(2000);

            //Assert 
            Assert.True(cancelBtn.Displayed && cancelBtn.Enabled);
            cancelBtn.Click();
            driver.Quit();
        }

        /// <summary>
        /// E2E Test testet die Cancel Taste des Chrome Download Drivers
        /// CodeOwner: Johannes Bulun
        /// </summary>
        [Fact]
        public void CancelCustomerPDFExport_ReturnsTrue()
        {

            IWebDriver driver = LoginSetUp(customerUsername, customerPassword);
            driver.Navigate().GoToUrl("http://sopro.isse.de:49861/Customer/TaxReport");
            Thread.Sleep(1000);

            IWebElement button = driver.FindElement(By.ClassName("btn"));
            button.Click();
            Thread.Sleep(1000);

            IWebElement printButton = driver.FindElement(By.Id("print-btn"));

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("setTimeout(function() { window.print(); }, 0);");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(3000);
            string JSPath = "document.querySelector('body>print-preview-app').shadowRoot.querySelector('#sidebar').shadowRoot.querySelector('print-preview-button-strip').shadowRoot.querySelector('cr-button.cancel-button')";
            Thread.Sleep(5000);
            IWebElement cancelBtn = (IWebElement)js.ExecuteScript($"return {JSPath}");
            Thread.Sleep(2000);

            //Assert 
            Assert.True(cancelBtn.Displayed && cancelBtn.Enabled);
            cancelBtn.Click();
            driver.Quit();
        }
    }
}