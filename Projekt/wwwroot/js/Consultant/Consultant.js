//codeowner: Alexander Rioux
var idbool = [];
var consultantList = [];
var consultanttocustomer = [];
class IdToBool {
    constructor(id, bool, consultantId) {
        this.id = id;
        this.bool = bool;
        this.consultantId = consultantId;
    }
}
// setzt die consultantList
function setConsultantList(conList) {
    consultantList = conList;
}
// setzt die consultanttocustomer 
function setConsultanttocustomer(contocusList) {
    consultanttocustomer = contocusList;
}
// holt sich den Input von dem jeweiligen Element (eindeutigkeit durch id parameter)
function checkInput(inputElement, id) {
    var exists = false;
    var idcons = -1
    var selectedValue = "";
    selectedValue = inputElement.value;
    // Überprüfe, ob der ausgewählte Consultant in der Consultant-Liste existiert
    exists = consultantList.some(function (consultant) {
        if (consultant.id.toString() + ' ' + consultant.firstname + ' ' + consultant.lastname === selectedValue) {
            idcons = consultant.id;
            return true;
        } else return false;
    });
    var helper = idbool.find(item => item.id === id);
    if (helper === undefined) {
        idbool.push(new IdToBool(id, exists, idcons));
    } else {
        helper.bool = exists;
        helper.consultantId = idcons;
    }
}
//Überprüft ob Eingabe gültig ist und lässt beim Fehlerfall das Popup erscheinen
function validateForm(event, id, customerId) {
    var helper = idbool.find(item => item.id === id);
    if (helper === undefined) {
        event.preventDefault(); // Formular-Submit abbrechen da keine Eingabe
        var myModal = new bootstrap.Modal(document.getElementById("WrongConsultantPopup"));
        myModal.show();
        return false;
    }
    var bool = helper.bool;
    if (!bool) {
        event.preventDefault(); // Formular-Submit abbrechen da Eingabe Fehlerhaft
        var myModal = new bootstrap.Modal(document.getElementById("WrongConsultantPopup"));
        myModal.show();
        return false;
    }
    var helper2 = consultanttocustomer.find(item => item.id === idbool.find(item => item.id === id).consultantId);
    if (helper2 === undefined) {
        return true;
    }
    var alreadyHas = helper2.listCustomerId.find(item => item.id === customerId)
    if (alreadyHas === undefined) {
        return true;
    }
    event.preventDefault(); // Formular-Submit abbrechen da Relation bereits existiert
    var myModal = new bootstrap.Modal(document.getElementById("WrongConsultantPopup"));
    myModal.show();
    return false;
}
