  /*CodeOwnder: Thomas Gerum*/


//Überprüft ob das eingegebene Datum im letzten Kalenderjahr liegt
function LastCalendarYear(dateTimeString) {
  var aktuellesDatum = new Date();
  var aktuellesJahr = aktuellesDatum.getFullYear();
  var dateTime = new Date(dateTimeString);
  var jahrDesDateTime = dateTime.getFullYear();
  var myModal = new bootstrap.Modal(document.getElementById("LastYearPopup"));
  if (jahrDesDateTime < aktuellesJahr) {
    myModal.show();
  }
}

//Überprüft ob das eingegebene Datum im letzten Kalenderjahr liegt
function LastCalendarYearCreate(input) {
  dateTimeString = input.value.toString();
  var aktuellesDatum = new Date();
  var aktuellesJahr = aktuellesDatum.getFullYear();
  var dateTime = new Date(dateTimeString);
  var jahrDesDateTime = dateTime.getFullYear();

  var myModal = new bootstrap.Modal(document.getElementById("LastYearPopup"));
  if (jahrDesDateTime < aktuellesJahr) {
    myModal.show();
  }
}