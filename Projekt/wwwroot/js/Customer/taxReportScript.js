﻿    /// <summary>
    /// Frontend Funktionalitäten für Druckfunktion
    /// CodeOwner: Vladyslav
    /// </summary>

document.getElementById('overview_hideBtn').onclick = function () {
    document.getElementById('reportOverview').hidden = true;
    document.getElementById('reportDetails').hidden = false;
    document.getElementById('overview_hideBtn_link').classList.add('active')
    document.getElementById('details_hideBtn_link').classList.remove('active')
}


document.getElementById('details_hideBtn').onclick = function () {
    document.getElementById('reportDetails').hidden = true;
    document.getElementById('reportOverview').hidden = false;
    document.getElementById('details_hideBtn_link').classList.add('active')
    document.getElementById('overview_hideBtn_link').classList.remove('active')
}

document.getElementById('print-btn').onclick = function () {
    window.print();
}  
