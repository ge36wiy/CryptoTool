﻿    /// <summary>
    /// Frontend Funktionalitäten für Profil
    /// CodeOwner: Vladyslav
    /// </summary>

let switchToEditMode = function () {
    var edit_btn = document.getElementById("edit-btn");
    var save_btn = document.getElementById("save-btn");
    var cancel_btn = document.getElementById("cancel-btn");
    edit_btn.style.display = "none";
    save_btn.style.display = "inline-block";
    cancel_btn.style.display = "inline-block";
    var inputFirstName_field = document.getElementById("inputFirstName");
    inputFirstName_field.removeAttribute('readonly');
    document.getElementById("inputLastName").removeAttribute('readonly')
    document.getElementById("inputEmailAddress").removeAttribute('readonly')
    document.getElementById("inputAddress").removeAttribute('readonly')


    /*document.getElementById("img-name-block").style.display = "none";
    document.getElementById("img-edit-block").style.display = "block";*/

}
let switchToViewMode = function () {
    var edit_btn = document.getElementById("edit-btn");
    var save_btn = document.getElementById("save-btn");
    var cancel_btn = document.getElementById("cancel-btn");
    edit_btn.style.display = "inline-block";
    save_btn.style.display = "none";
    cancel_btn.style.display = "none";

    /* document.getElementById("inputFirstName").value = prev_name;
     document.getElementById("inputLastName").value = prev_surname;
     document.getElementById("inputEmailAddress").value = prev_email;
     document.getElementById("inputAddress").value = prev_address;*/

    var inputFirstName_field = document.getElementById("inputFirstName");
    inputFirstName_field.setAttribute('readonly', 'readonly')
    document.getElementById("inputLastName").setAttribute('readonly', 'readonly')
    document.getElementById("inputEmailAddress").setAttribute('readonly', 'readonly')
    document.getElementById("inputAddress").setAttribute('readonly', 'readonly')

    /*document.getElementById("img-name-block").style.display = "block";
    document.getElementById("img-edit-block").style.display = "none";*/

}
/*if (document.getElementById("error-msg").textContent.trim() !== "") {
    switchToEditMode()
}*/

var spans = document.querySelectorAll("span");
var isError = false;

spans.forEach(function (span) {
    if (span.textContent.trim() !== "" && span.classList.contains("text-danger")) {
        isError = true;
        return;
    }
});

if (isError) {
    switchToEditMode()
}

let edit_btn = document.getElementById("edit-btn");
let save_btn = document.getElementById("save-btn");
let cancel_btn = document.getElementById("cancel-btn");
edit_btn.addEventListener('click', switchToEditMode);
//save_btn.addEventListener('click', switchToViewMode);
cancel_btn.addEventListener('click', switchToViewMode);