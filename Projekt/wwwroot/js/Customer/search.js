//Alle Methoden auf dieser Seite sind von Sebastian Klee

//Hilfsmethode: gibt Index aller Kinder zurück
function getChildren(parent, rows) {
  var i;
  var listOfChildrenIDs = [];
  var target = parent.getAttribute("data-bs-target");
  for (i = 0; i < rows.length; i++) {
    var id = rows[i].getAttribute("id");
    if (target.indexOf(id) > -1) {
      listOfChildrenIDs.push(id);
    }
  }
  return listOfChildrenIDs;
}

//alterniert Pfeil nach oben und nach unten bei klick und ruft dann Sortiermethode auf
function changeSymbol() {
  var div = document.getElementById("order");
  if (div.innerText == "↑") {
    div.innerHTML = "&darr;";
  } else {
    div.innerHTML = "&uarr;";
  }
  sortSetup();
}

//Hilfsmethode: setzt Werte um richtig zu sortieren und ruft dann Sortiermethode entsprechend auf
function sortSetup() {
  let orderDictionary = {
    "↓": 1,
    "↑": 2,
  };
  var order = document.getElementById("order").innerText;
  var sortColumn = document.getElementById("sort").value;
  if (sortColumn == "Name") {
    sortByColumn(orderDictionary[order], 1);
  }
  if (sortColumn == "Menge") {
    sortByColumn(orderDictionary[order], 2);
  }
  if (sortColumn == "Gesamtwert") {
    sortByColumn(orderDictionary[order], 4);
  }
  if (sortColumn == "Wert über Haltefrist") {
    sortByColumn(orderDictionary[order], 6);
  }
}

//sortiert Liste nach ausgewähltem DropdownElement und Pfeilrichtung (Teilassets werden passend positioniert)
function sortByColumn(order, row) {
  var table, rows, switching, i, j, x, y, xText, yText, shouldSwitch;
  table = document.getElementById("myTable");
  rows = table.rows;

  //trennt Coin- und Teilasset Zeilen
  var coins = [];
  var partial = [];
  for (i = 0; i < rows.length; i++) {
    if (rows[i].getAttribute("data-bs-parent") == ".clickable") {
      partial.push(rows[i]);
    } else {
      coins.push(rows[i]);
    }
  }

  //sortiert Coins
  switching = true;
  while (switching) {
    switching = false;
    for (i = 0; i < coins.length - 1; i++) {
      shouldSwitch = false;
      x = coins[i]
        .getElementsByTagName("TD")
        [row].getElementsByTagName("div")[2];
      y = coins[i + 1]
        .getElementsByTagName("TD")
        [row].getElementsByTagName("div")[2];
      xText = x.innerText;
      yText = y.innerText;
      //nach Name sortiert
      if (row == 1) {
        //aufsteigend
        if (order == 1) {
          if (xText.toLowerCase() > yText.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
          //absteigend
        } else {
          if (xText.toLowerCase() < yText.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      //alle numerischen Sortierungen
      if (row == 2 || row == 4 || row == 6) {
        if (xText.slice(-1) == "€") {
          xText = xText.substring(0, xText.length - 1);
          yText = yText.substring(0, yText.length - 1);
        }
        //absteigend
        if (order == 1) {
          if (
            Number(xText.replace(/\./g, "").replace(/\,/g, ".")) <
            Number(yText.replace(/\./g, "").replace(/\,/g, "."))
          ) {
            shouldSwitch = true;
            break;
          }
          //aufsteigend
        } else {
          if (
            Number(xText.replace(/\./g, "").replace(/\,/g, ".")) >
            Number(yText.replace(/\./g, "").replace(/\,/g, "."))
          ) {
            shouldSwitch = true;
            break;
          }
        }
      }
    }
    if (shouldSwitch) {
      [coins[i], coins[i + 1]] = [coins[i + 1], coins[i]];
      switching = true;
    }
  }
  //erzeugt neue Tabelle, mit richtiger Sortierung
  document.getElementById("myTable").innerHTML = "";
  for (i = 0; i < coins.length; i++) {
    var newRow = table.insertRow(-1);
    newRow.outerHTML = coins[i].outerHTML;
  }

  //fügt Teilassets hinzu
  for (i = partial.length - 1; i >= 0; i--) {
    for (j = 0; j < table.rows.length; j++) {
      if (
        table.rows[j].getAttribute("data-bs-target") != null &&
        table.rows[j]
          .getAttribute("data-bs-target")
          .includes(partial[i].getAttribute("id"))
      ) {
        insertAfter(partial[i], table.rows[j]);
      }
    }
  }
}

//Hilfsmethode: Node.insertBefore, aber after
function insertAfter(newNode, existingNode) {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}

//realisiert Filter und Suche auf AssetOverview
//macht alle Zeilen unsichtbar, die im Dropdown nicht ausgewählt sind oder deren Name nicht den Suchbegriff enthält, und deren Teilassets
function filterList() {
  var options = document.getElementById("fil").selectedOptions;
  var selectedFilter = Array.from(options).map(({ value }) => value);
  var input, filter, table, tr, td, i, j, txtValue, div, target, id;
  var listOfVisibleChildren = [];
  input = document.getElementById("myInput").value.toLowerCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  //wenn Eingabe leer, mache alles unsichtbar
  if (selectedFilter.length == 0) {
    for (i = 0; i < tr.length; i++) {
      tr[i].style.display = "none";
    }
    return;
  }
  //nur parents die Eingabe matchen sichtbar
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td");
    div = td[1].getElementsByTagName("div")[2];
    if (div) {
      tr[i].style.display = "none";
      txtValue = div.innerText.toLowerCase();
      if (txtValue.includes(input)) {
        for (j = 0; j < selectedFilter.length; j++) {
          if (txtValue.trim() == selectedFilter[j].toLowerCase()) {
            listOfVisibleChildren = listOfVisibleChildren.concat(
              getChildren(tr[i], tr)
            );
            tr[i].style.display = "";
          }
        }
      }
    }
  }
  //alle Kinder deren Parents sichtbar sind werden sichtbar
  for (i = 0; i < tr.length; i++) {
    if (listOfVisibleChildren.includes(tr[i].getAttribute("id"))) {
      tr[i].style.display = "";
    }
  }
}
