using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Models
{
    /// <summary>
    /// Die Klasse <c>Messages</c> erstellt das Modell für die Datenbank welches Nachrichten abspeichert
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class Messages
    {
        /// <summary>
        /// MessageId ist die eindeutige Id um Nachrichten voneinander unterscheiden zu können
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Key]
        public int MessageId { get; set; }


        /// <summary>
        /// Reciever gibt an wer die Nachricht erhalten soll
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public int Reciever { get; set; }


        /// <summary>
        /// Message ist die zu übergebene Nachricht
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public String Message { get; set; }


        /// <summary>
        /// Konstruktor
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public Messages(int reciever, String message)
        {
            this.Message = message;
            this.Reciever = reciever;
        }
    }
}