﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Models
{

    /// <summary>
    /// Customer User Model mit jeweiligen Validation Constraints, erbt von Account
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class Customer : Account
    {
        /// <summary>
        /// Vorname des Kunden
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "Vorname darf nicht leer sein")]
        [StringLength(50, MinimumLength = 1,
                 ErrorMessage = "Ungültige Länge")]
        public string Firstname { get; set; }

        /// <summary>
        /// Nachname des Kunden
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "Nachname darf nicht leer sein")]
        [StringLength(50, MinimumLength = 1,
            ErrorMessage = "Ungültige Länge")]
        public string Lastname { get; set; }


        /// <summary>
        /// Adresse des Kunden
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "Adresse darf nicht leer sein")]
        [StringLength(200, MinimumLength = 1,
           ErrorMessage = "Ungültige Länge")]
        public string Address { get; set; }

        /// <summary>
        /// E-Mail Adresse des Kunden
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "E-mail darf nicht leer sein")]
        [EmailAddress(ErrorMessage = "Inkorrekte E-mail")]
        public string Email { get; set; }

        /// <summary>
        /// Pfad zum Profilbild des Kunden
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public string Pathtoimage { get; set; }
    }
}