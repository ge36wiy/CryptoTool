using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Projekt.Utilities;
using System.Text.Json.Serialization;

namespace Projekt.Models
{
    /// <summary>
    /// TransactionModel mit jeweiligen Validation Constraints
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// ID der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// Name des Coins
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [ValidCoins]
        [Required(ErrorMessage = "Es muss eine Kryptowährung spezifiziert werden")]
        [JsonRequired]
        public String Coin { get; set; }

        /// <summary>
        /// Typ der Transaktion(Kauf, Verkauf oder Mining)
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Typ")]
        [JsonRequired]
        public TransactionType Type { get; set; }

        /// <summary>
        /// Gebühr der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Gebühr in Euro")]
        [JsonRequired]
        public decimal Fee { get; set; }

        /// <summary>
        /// Kurs der Coins zum Kaufdatum
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [Required(ErrorMessage = "Es muss eine Einkaufsrate spezifiziert werden")]
        [DisplayName("Rate zum Zeitpunkt des Kaufs")]
        [JsonRequired]
        public double Rate { get; set; }

        /// <summary>
        /// Menge der Coins die Transferiert wurden
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [AmountValidator]
        [Required(ErrorMessage = "Es muss eine Menge an Coins spezifiziert werden")]
        [DisplayName("Menge")]
        [JsonRequired]
        public double Amount { get; set; }

        /// <summary>
        /// Gesamtwert der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Gesamtwert")]
        public double Value { get; set; }

        /// <summary>
        /// Restwert der Transaktion, für Steuerberechnung FIFO und AssetOverview
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [JsonIgnore]
        [DisplayName("Restwert")]
        public double Remainder { get; set; }

        /// <summary>
        /// BesitzerID der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [JsonIgnore]
        [DisplayName("Besitzer")]
        public int OwnerId { get; set; }

        /// <summary>
        /// Datum der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DateValidator]
        [Required(ErrorMessage = "Es muss eine Datum ausgewählt werden")]
        [DataType(DataType.Date)]
        [DisplayName("Kaufdatum")]
        [JsonRequired]
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        /// Datum an dem die Transaktion über der Haltefrist ist
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [JsonIgnore]
        [DataType(DataType.Date)]
        [DisplayName("Handelsdatum")]
        public DateTime Tradeable { get; set; }

        /// <summary>
        /// Flag für Soft Deletion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [JsonIgnore]
        public Boolean Deleted { get; set; }

    }
}