namespace Projekt.Models;

/// <summary>
/// Automatisch erstelltes ErrorViewModel
/// CodeOwner: automatisch erstellt
/// </summary>
public class ErrorViewModel
{
    /// <summary>
    /// Id des Errors
    /// CodeOwner: automatisch erstellt
    /// </summary>
    public string RequestId { get; set; }
    /// <summary>
    /// Gibt zurück ob RequestId nicht leer war
    /// CodeOwner: automatisch erstellt
    /// </summary>
    /// <returns> boolean: false wenn RequestId null, true sonst </returns>
    public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
}
