using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Models
{
    /// <summary>
    /// Admin User Model, erbt Username Passwort und Id, ist mit eventuell weiteren Attributen zu befüllen, je nach Wunsch
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class Admin : Account
    {
    }
}