﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Models
{
    /// <summary>
    /// Consultant User Model mit jeweiligen Validation Constraints, erbt von Account
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class Consultant : Account
    {

        /// <summary>
        /// Vorname des Steuerberaters
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>

        [Required(ErrorMessage = "Vorname darf nicht leer sein")]
        [StringLength(50, MinimumLength = 1,
            ErrorMessage = "Ungültige Länge")]
        public string Firstname { get; set; }

        /// <summary>
        /// Nachname des Steuerberaters
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "Nachname darf nicht leer sein")]
        [StringLength(50, MinimumLength = 1,
            ErrorMessage = "Ungültige Länge")]
        public string Lastname { get; set; }

        /// <summary>
        /// Adresse des Steuerberaters
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "Adresse darf nicht leer sein")]
        [StringLength(200, MinimumLength = 1,
           ErrorMessage = "Ungültige Länge")]
        public string Address { get; set; }

        /// <summary>
        /// E-Mail des Steuerberaters
        /// CodeOwner: Thomas Gerum, Vlad Kovganko
        /// </summary>
        [Required(ErrorMessage = "E-mail darf nicht leer sein")]
        [EmailAddress(ErrorMessage = "Inkorrekte E-Mail")]
        public string Email { get; set; }

        /// <summary>
        /// Link zum Profilbild des Steuerberaters
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public string Pathtoimage { get; set; }
    }
}