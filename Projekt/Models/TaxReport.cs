﻿using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Projekt.Utilities;
using System.Data;

namespace Projekt.Models
{
#pragma warning disable CS1591

    /// <summary>
    /// Ein TaxReport Objekt repräsentiert eine steuerliche Auswertung und enthält alle dafür notwendigen Informationen 
    /// CodeOwner: Vladyslav Kovganko
    /// </summary>
    public class TaxReport
    {
        public int year;
        /// <summary>
        /// profit - steuerlich irrelevanter Gewinn!
        /// </summary>
        public double profit, losses, tax_relevant_losses, tax_relevant_profit, total_profit;

        public decimal fees;
        /// <summary>
        /// Verkäufe im Jahr year
        /// </summary>
        public List<Sell> sells;
        /// <summary>
        /// Tabellen für die View
        /// </summary>
        public Dictionary<string, List<Sell>> tables;
        /// <summary>
        /// Gewinn vom Mining
        /// </summary>
        public double mining_profit;
        /// <summary>
        /// Mining Tabelle für die View
        /// </summary>
        public Dictionary<string, double> mining_coins;


        public TaxReport(int year, List<Transaction> transactions)
        {
            mining_coins = initMiningCoins(year, transactions);

            initTaxReport(year, transactions);
        }

        /// <summary>
        /// Hilfsmethode: Erstellt ein Dictionary das die Paare von Typ {Coin, Gesamtwert} enthält
        /// => Alle zur steuerlichen Auswertung gehörenden Coins und deren Gesamtwert, die durch Mining erwirtschaftet wurden.
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="year">Das relevante Jahr</param>
        /// <param name="transactions">Die Transktionen des Kunden, anhand von denen die Mining Coins ermittelt werden</param>
        /// <returns>Dictionary(Coin, Gesamtwert)</returns>
        private Dictionary<string, double> initMiningCoins(int year, List<Transaction> transactions)
        {
            Dictionary<string, double> mining_coins_pairs = new Dictionary<string, double>();

            //transactions.RemoveAll(t => (t.PurchaseDate.Year != year || t.Type != TransactionType.Mining));
            foreach (Transaction t in transactions)
            {
                string current_coin = t.Coin;

                if (t.PurchaseDate.Year != year || t.Type != TransactionType.Mining) continue;

                if (mining_coins_pairs.ContainsKey(current_coin))
                {
                    mining_coins_pairs[current_coin] += t.Value;
                }
                else
                {
                    mining_coins_pairs.Add(current_coin, 0);
                    mining_coins_pairs[current_coin] += t.Value;
                }
            }

            return mining_coins_pairs;
        }

        /// <summary>
        /// Hilfsmethode: Gibt den Gesamtwert aller Mining Coins des aktuellen Steuerberichts zurück
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param></param>
        /// <returns>Gesamtwert aller Mining Coins im Jahr X</returns>
        public double getMiningProfit()
        {
            double profit = 0;
            foreach (var coin_name in mining_coins.Keys)
            {
                profit += mining_coins[coin_name];
            }

            return profit;
        }


        /// <summary>
        /// Initialisiert das TaxReport Object
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="year">Das relevante Jahr</param>
        /// <param name="transactions">Die Transktionen des Kunden</param>
        /// <returns>steuerliche Auswertung vom Jahr year </returns>
        private void initTaxReport(int year, List<Transaction> transactions)
        {
            // Werte die am Ende berechnet werden. 
            double _profit = 0, _losses = 0, _tax_relevant_losses = 0, _tax_relevant_profit = 0;
            decimal _fees = 0;
            bool TaxRelevant = false;

            // Repräsentiert alle im Jar <year> getätigten Verkäufe
            List<Sell> _sells = new List<Sell>();

            // Alle Transaktionen nach dem geg. Jahr liegen entfernen und anschließend die Liste chronologisch sortieren.
            transactions.RemoveAll(t => t.PurchaseDate.Year > year);
            transactions.Sort((t1, t2) => t1.PurchaseDate.CompareTo(t2.PurchaseDate));

            for (int i = 0; i < transactions.Count; i++)
            {
                // Den nächsten Verkauf finden
                if (transactions[i].Type != TransactionType.Verkauf) continue;
                Transaction sell_transaction = transactions[i];


                // Sell-Objekt wird anschließend initialisiert
                Sell sell = new Sell();
                sell.sell_transaction = sell_transaction;

                string current_coin = sell_transaction.Coin;

                //amount_to_process ist die Menge an Coins die verabeitet werden soll (Korrekt auf Kauftranasktionen verteilt). 
                double amount_to_process = sell_transaction.Remainder;

                //Solange amount_to_process != wurde nocht nicht alles verkauft
                while (amount_to_process != 0)
                {
                    Transaction buy_transaction = FindNextBuyTransaction(transactions.GetRange(0, i + 1), current_coin);

                    double temp_profit = subtractTransactions(sell_transaction, buy_transaction);

                    double sold_amount = amount_to_process - sell_transaction.Remainder;


                    double sellT_partial_fee = calculatePartialFee(sold_amount, sell_transaction.Amount, Decimal.ToDouble(sell_transaction.Fee));
                    double buyT_partial_fee = calculatePartialFee(sold_amount, buy_transaction.Amount, Decimal.ToDouble(buy_transaction.Fee));

                    double partial_fee = sellT_partial_fee + buyT_partial_fee;

                    temp_profit -= partial_fee;

                    amount_to_process = sell_transaction.Remainder;



                    if (sell_transaction.PurchaseDate.Year == year)
                    {
                        _fees += (decimal)partial_fee;
                        TaxRelevant = IsTaxRelevant(sell_transaction, buy_transaction);

                        if (TaxRelevant)
                        {
                            if (temp_profit < 0)
                                _tax_relevant_losses += temp_profit;

                            _tax_relevant_profit += temp_profit;
                        }
                        else
                        {
                            if (temp_profit < 0)
                                _losses += temp_profit;
                            _profit += temp_profit;
                        }
                        BuyProfitPair buyProfitPair = InitBuyProfitPair(sell_transaction, buy_transaction, sold_amount, buyT_partial_fee, sellT_partial_fee, temp_profit, TaxRelevant);

                        sell.buyProfits.Add(buyProfitPair);
                    }
                }




                if (sell_transaction.PurchaseDate.Year == year)
                {
                    _sells.Add(sell);
                }
            }

            if (Math.Floor(_tax_relevant_profit) <= 600 && _tax_relevant_profit >= 0)
            {
                _profit += _tax_relevant_profit;
                _tax_relevant_profit = 0;
            }


            Dictionary<string, List<Sell>> tables = getTables(_sells);
            this.year = year;
            this.fees = _fees;
            this.profit = _profit;
            this.losses = _losses;
            this.tax_relevant_losses = _tax_relevant_losses;
            this.tax_relevant_profit = _tax_relevant_profit;
            this.total_profit = _profit + _tax_relevant_profit;
            this.sells = _sells;
            this.tables = tables;
        }


        private BuyProfitPair InitBuyProfitPair(Transaction sell, Transaction buy, double sold_amount,
            double buyT_partial_fee,
            double sellT_partial_fee,
            double profit,
            bool taxRelevant)
        {

            BuyProfitPair buyProfitPair = new BuyProfitPair();
            buyProfitPair.buy_transaction = buy;
            buyProfitPair.buy_price = sold_amount * buy.Rate;
            buyProfitPair.soldAmount = sold_amount;
            buyProfitPair.buy_partial_fee = buyT_partial_fee;
            buyProfitPair.buy_date = buy.PurchaseDate;
            buyProfitPair.buy_id = buy.Id;
            buyProfitPair.profit = profit;
            buyProfitPair.taxRelevant = taxRelevant;

            buyProfitPair.sell_price = sell.Rate * sold_amount;
            buyProfitPair.sell_partial_fee = sellT_partial_fee;
            buyProfitPair.sell_date = sell.PurchaseDate;
            buyProfitPair.sell_id = sell.Id;

            return buyProfitPair;
        }


        /// <summary>
        /// Hilfsmethode: Überprüft, ob der Verkauf steuerlich relevant ist
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="sell_t">Verkauf</param>
        /// <param name="buy_t">Entprechende Kauftransaktion</param>
        /// <returns>Boolean</returns>
        private static bool IsTaxRelevant(Transaction sell_t, Transaction buy_t)
        {
            return sell_t.PurchaseDate > buy_t.PurchaseDate.AddDays(365) ? false : true;
        }


        /// <summary>
        /// Hilfsmethode: Gibt die älteste Kauftransaktion zurück, deren Coins noch zum Verkauf zur Verfügung stehen. 
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="transactions">Verkauf</param>
        /// <param name="coin">Coinname</param>
        /// <returns>Der Kauf, der am längsten in der Vergangeheit liegt</returns>
        private static Transaction FindNextBuyTransaction(List<Transaction> transactions, string coin)
        {
            foreach (Transaction t in transactions)
            {
                if ((t.Type == TransactionType.Kauf || t.Type == TransactionType.Mining) && t.Coin == coin && t.Remainder != 0)
                {
                    return t;
                }
            }
            throw new GhostCoinException(coin, "Inkonsistente Transaktionsliste");
        }


        /// <summary>
        /// Hilfsmethode: Berechnet den Gewinn vom Verkauf  
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="sell_t">Verkauf</param>
        /// <param name="buy_t">entsprechender Kauf</param>
        /// <param name="amount_to_process">verkaufte Menge</param>
        /// <returns>Gewinn</returns>
        private static double calculateProfit(Transaction sell_t, Transaction buy_t, double amount_to_process)
        {
            return sell_t.Rate * amount_to_process - buy_t.Rate * amount_to_process;
        }


        /// <summary>
        /// Hilfsmethode: Verringert den Coin Bestand um die max. Mögliche Menge (verkaufte Menge) und gibt den Gewinn zurück
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="sell_t">Verkauf</param>
        /// <param name="buy_t">entsprechender Kauf</param>
        /// <returns>Gewinn</returns>
        private static double subtractTransactions(Transaction sell_t, Transaction buy_t)
        {
            double temp_profit = 0;
            if (buy_t.Remainder >= sell_t.Remainder)
            {
                temp_profit = calculateProfit(sell_t, buy_t, sell_t.Remainder);
                buy_t.Remainder -= sell_t.Remainder;
                sell_t.Remainder = 0;
            }
            else
            {
                temp_profit = calculateProfit(sell_t, buy_t, buy_t.Remainder);
                sell_t.Remainder -= buy_t.Remainder;
                buy_t.Remainder = 0;
            }

            return temp_profit;
        }

        /// <summary>
        /// Hilfsmethode: Initialisiert Tabellen die in der View bei "Details" angezeigt werden.
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="sells">Verkäufe</param>
        /// <returns>Zu jedem Coin eine Tabelle in Form eines Dictionary</returns>
        private static Dictionary<string, List<Sell>> getTables(List<Sell> sells)
        {
            Dictionary<string, List<Sell>> tables = new Dictionary<string, List<Sell>>();

            foreach (Sell sell in sells)
            {
                string coin_name = sell.sell_transaction.Coin;
                if (tables.ContainsKey(coin_name))
                {
                    tables[coin_name].Add(sell);
                }
                else
                {
                    tables.Add(coin_name, new List<Sell>());
                    tables[coin_name].Add(sell);
                }
            }

            return tables;
        }

        /// Berechnet die Gebühr abhängig von der verkaufeten Menge 
        /// CodeOwner: Vladyslav Kovganko
        private static double calculatePartialFee(double sold_amount, double amount, double fee)
        {
            return sold_amount * fee / amount;
        }

        /// <summary>
        ///  Summiert den Gewinn der jeweiligen Tabelle. 
        ///  CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="table">Tabelle</param>
        /// <param name="taxRelevant">Ob der Gewinn steuerlicht relevant sein soll oder nicht</param>
        /// <returns>Gewinn</returns>
        public static double getProfit(List<Sell> table, bool taxRelevant)
        {
            double profit = 0;
            foreach (Sell sell in table)
            {
                foreach (BuyProfitPair buyProfitPair in sell.buyProfits)
                {
                    if (taxRelevant && buyProfitPair.taxRelevant)
                    {
                        profit += buyProfitPair.profit;
                    }
                    else if (!taxRelevant && !buyProfitPair.taxRelevant)
                    {
                        profit += buyProfitPair.profit;
                    }
                }
            }

            return profit;
        }


        /// <summary>
        /// </summary>
        public double getTaxRelevantProfit()
        {
            return tax_relevant_profit;
        }

        /// <summary>
        /// </summary>
        public double getTaxIrrelevantProfit()
        {
            return profit;
        }

        /// <summary>
        /// </summary>
        public double getOverallProfit()
        {
            return getTaxIrrelevantProfit() + getTaxRelevantProfit();
        }


    }

    /// <summary>
    /// Eine "Datenstruktur" zur Represäntation eines Verkaufs. (Er besteht aus einer Verkaufstransaktion und einer Menge der Kauftransaktionen, deren Coins verkauft wurden.)
    /// </summary>
    public struct Sell
    {
        /// <summary>
        /// Entsprechender Verkauf
        /// </summary>
        public Transaction sell_transaction;
        /// <summary>
        /// Coins die Verkauf wurden
        /// </summary>
        public List<BuyProfitPair> buyProfits;


        public Sell()
        {
            sell_transaction = null;
            buyProfits = new List<BuyProfitPair>();
        }
    }

    /// <summary>
    /// Eine weitere Datenstruktur, die alle relevanten Informationen bei einem Verkauf enthält.
    /// </summary>
    public struct BuyProfitPair
    {
        public Transaction buy_transaction;
        public double profit, soldAmount, buy_partial_fee, buy_price, sell_partial_fee, sell_price;
        public bool taxRelevant;

        public DateTime buy_date, sell_date;

        public int buy_id, sell_id;
    }
}




