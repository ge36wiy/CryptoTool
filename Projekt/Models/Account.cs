using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Models
{

    /// <summary>
    /// Account Login Model
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Id des Userobjekts, wird an die jeweiligen Klassen vererbt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Username für den Login, wird an die Userklassen vererbt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Username")]
        public string Username { get; set; }

        /// <summary>
        /// Passwort für den Login, wird an die Userklassen vererbt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Passwort")]
        public string Password { get; set; }

    }
}