﻿using Projekt.Utilities;
namespace Projekt.Models
{
    /// <summary>
    /// Verknüoft Consultant und seine Einsichtberechtigungen, um diese dem Kunden in der AccessGrantedView anzuzeigen
    /// CodeOwner: Christopher Glöckel
    /// </summary>
    public class AssignedConsultant : Consultant
    {
        /// <summary>
        /// Einsichtstatus der einfachen Einsicht
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public InsightStatus StatusSimpleInsight { get; set; }
        /// <summary>
        /// Einsichtstatus der erweiterten Einsicht
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public InsightStatus StatusExtendedInsight { get; set; }

        /// <summary>
        /// Konstruktor, der die anzuzeigenden Properties aus einem Consultant übernimmt und zudem die Einsichtstatus speichert
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public AssignedConsultant(Consultant c, InsightStatus statusSimpleInsight, InsightStatus statusExtendedInsight)
        {
            Id = c.Id;
            Firstname = c.Firstname;
            Lastname = c.Lastname;
            Address = c.Address;
            Email = c.Email;
            Pathtoimage = c.Pathtoimage;
            StatusExtendedInsight = statusExtendedInsight;
            StatusSimpleInsight = statusSimpleInsight;
        }
    }
}
