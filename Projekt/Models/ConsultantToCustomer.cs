using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Projekt.Utilities;

namespace Projekt.Models
{
    /// <summary>
    /// Die Klasse <c>ConsultantToCustomer</c> erstellt das Modell für die Datenbank welches Kunden zu Steuerberater und ihre Einsichtstufen beschreibt
    /// Nutzer: Steuerberater / Admin
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class ConsultantToCustomer
    {
        /// <summary>
        /// Id des Steuerberaters
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Required]
        public int IdConsultant { get; set; }
        /// <summary>
        /// Id des Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Required]
        public int IdCustomer { get; set; }
        /// <summary>
        /// Art der Einsicht bei der einfachen Einsicht
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Required]
        public InsightStatus InsightStatusSimple { get; set; }
        /// <summary>
        /// Art der Einsicht bei der erweiterten Einsicht
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Required]
        public InsightStatus InsightStatusExtended { get; set; }
    }
}