using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.Cookies;



var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<MvcAccountContext>(options =>
    {
        options.UseSqlite(builder.Configuration.GetConnectionString("MvcAccountContext") ?? throw new InvalidOperationException("Connection string 'MvcAccountContext' not found."));
        options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
    });

// Add services to the container.
builder.Services.AddControllersWithViews();


builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
{
    option.LoginPath = "/Account/Login";
    option.ExpireTimeSpan = TimeSpan.FromMinutes(10);
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}



app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();

app.UseAuthorization();



//pattern: "{controller=Home}/{action=Index}/{id?}");
//app.MapRazorPages();
app.MapDefaultControllerRoute();
app.Run();
