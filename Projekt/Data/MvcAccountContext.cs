using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;


///<summary>
/// Datenbankkontext mit den jeweiligen Tabellen
///</summary>
public class MvcAccountContext : DbContext
{
    ///<summary>
    /// Datenbankkontext 
    ///</summary>
    public static DbContextOptions<MvcAccountContext> lastOptions { get; set; }


    ///<summary>
    /// Datenbankkontext  Konstruktor
    ///</summary>
    public MvcAccountContext(DbContextOptions<MvcAccountContext> options)
        : base(options)
    {
        lastOptions = options;
    }

    /// <summary>
    /// Konstruktor für den Datenbankkontext
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public MvcAccountContext() { }

    /// <summary>
    /// Tabelle für Login daten
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public DbSet<Account> Account { get; set; } = default!;

    /// <summary>
    /// Tabelle für Customerobjekte
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public virtual DbSet<Customer> Customer { get; set; }

    /// <summary>
    /// Tabelle für Consultantobjekte
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public virtual DbSet<Consultant> Consultant { get; set; }

    /// <summary>
    /// Tabelle für Adminuserobjekte
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public DbSet<Admin> Admin { get; set; }

    /// <summary>
    /// Tabelle für Transaktionsobjekte
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public virtual DbSet<Transaction> Transaction { get; set; }


    /// <summary>
    /// Datenbank ConsultantToCustomer mit get und set
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public virtual DbSet<ConsultantToCustomer> ConsultantToCustomer { get; set; }


    /// <summary>
    /// Datenbank Messages mit get und set
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public DbSet<Messages> Messsages { get; set; }


    /// <summary>
    /// Initalisierung der Datenbank ConsultantToCustomer mit zwei Primärschlüssel
    /// CodeOwner: Alexander Rioux
    /// </summary>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ConsultantToCustomer>()
              .HasKey(m => new { m.IdConsultant, m.IdCustomer });

    }

    public void updateCustomerData(Customer user)
    {
        var userToUpdate = this.Customer.Find(user.Id);
        if (userToUpdate != null)
        {
            userToUpdate.Firstname = user.Firstname;
            userToUpdate.Lastname = user.Lastname;
            userToUpdate.Email = user.Email;
            userToUpdate.Address = user.Address;

            this.Update(userToUpdate);
            this.SaveChanges();
        }
    }

    public void updateConsultantData(Consultant user)
    {
        var userToUpdate = this.Consultant.Find(user.Id);
        if (userToUpdate != null)
        {
            userToUpdate.Firstname = user.Firstname;
            userToUpdate.Lastname = user.Lastname;
            userToUpdate.Email = user.Email;
            userToUpdate.Address = user.Address;

            this.Update(userToUpdate);
            this.SaveChanges();
        }
    }
}
