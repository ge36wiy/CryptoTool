using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;
namespace Projekt.Interfaces
{
    ///<summary>
    /// Interface zum Organisieren von Import Funktionalitten
    /// Code Owner: Johannes Bulun
    ///</summary>
    public interface IImportFile
    {

        /// <summary>
        /// Methode die den Import ueber Json implementiert
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <returns> Liste aller Namen von supporteten Coins</returns>
        List<Transaction> parseData(IFormFile file);


    }
}