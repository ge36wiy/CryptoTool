using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekt.Interfaces
{
    ///<summary>
    /// Interface zum beziehen der Daten für unterschiedliche Coins
    /// Code Owner: Thomas Gerum
    ///</summary>
    public interface ICoininfo
    {

        /// <summary>
        /// Methode die alle Supporteten Coins zurückgibt, die in der Autocompletion für die Coineingabe auftauchen sollen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Liste aller Namen von supporteten Coins</returns>
        public List<String> getAllCoinsList();

        /// <summary>
        /// Methode die alle Supporteten Coins un ihre ID zurückgibt, die in der Autocompletion für die Coineingabe auftauchen sollen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Liste aller Namen von supporteten Coins und ihre Id als Dictionary</returns>
        public Dictionary<string, string> getAllCoins();

        /// <summary>
        /// Berechnet den momentanen Kurs aller übergebenen Assetnamen.
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="names"> Liste von Assetnamen.</param>
        /// <returns>Dictionary, Assetname zu momentaner Kurs dieses Assets.</returns>
        public Dictionary<string, double> getRates(List<string> names);

        /// <summary>
        /// Bestimmt den Link zu Icons aller übergebenen Assetnamen.
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="names"> Liste von Assetnamen.</param>
        /// <returns>Dictionary, Assetname zu Link von Icon dieses Assets.</returns>
        public Dictionary<string, string> getPictures(List<string> names);

    }
}