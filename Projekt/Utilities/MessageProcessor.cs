using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;

namespace Projekt.Utilities
{
    /// <summary>
    /// Abwicklung der Aufgaben zu Nachrichten in der Datenbank 
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class MessageProcessor
    {
        /// <summary>
        /// Die funktion RemoveMessage entfernt eine Nachricht in der Datenbank 
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="_context"> Übergibt den Context für die Datenbank</param>
        /// <param name="messageId"> Spezifiziert die Nachricht</param>
        public async Task RemoveMessage(MvcAccountContext _context, int messageId)
        {
            if (!(_context.Messsages == null))
            {
                var message = await _context.Messsages.FindAsync(messageId);
                if (message != null)
                {
                    _context.Messsages.Remove(message);
                    await _context.SaveChangesAsync();
                }
            }
        }


        /// <summary>
        /// Die funktion AddMessage fügt eine Message in der Datenbank hinzu
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="_context"> Übergibt den Context für die Datenbank</param>
        /// <param name="reciever"> Spezifiziert den Empfänger</param>
        /// <param name="message"> Ist die zu übergebene Nachricht</param>
        public async Task AddMessage(MvcAccountContext _context, int reciever, String message)
        {
            var newMessage = new Messages(reciever, message);

            if (_context.Consultant.Any(x => x.Id == reciever))
            {
                _context.Messsages.Add(newMessage);
                await _context.SaveChangesAsync();
            }
        }
    }
}