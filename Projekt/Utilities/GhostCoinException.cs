﻿namespace Projekt.Utilities
{
    /// <summary>
    /// Wird geworfen wenn die Liste der Transaktionen inkonsistent ist. Also wenn mehr verkauft wird als gekauft wurde. 
    /// => Daraufhin folgt die entprechende Behandlung dieser Exception
    /// CodeOwner: Vladyslav Kovganko
    /// </summary>
    public class GhostCoinException : Exception
    {
        public string ghostCoin { get; }

        public GhostCoinException(string ghostCoin, string message) : base(message)
        {
            this.ghostCoin = ghostCoin;
        }
    }
}
