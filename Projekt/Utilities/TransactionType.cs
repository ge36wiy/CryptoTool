using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;
namespace Projekt.Utilities
{
    /// <summary>
    /// Stellt die unterschiedlichen an Transaktionen dar
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum TransactionType
    {
        /// <summary>
        /// Kauf Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [EnumMember(Value = "Kauf")]
        Kauf,

        /// <summary>
        /// Verkauf Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [EnumMember(Value = "Verkauf")]
        Verkauf,

        /// <summary>
        /// Mining Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [EnumMember(Value = "Mining")]
        Mining

    }
}