using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.Utilities
{

    /// <summary>
    /// Custom Date Validator um sicherzu stellen, dass Transaktionsdatum nicht in der Zukunft liegt
    /// CodeOwner: Thomas Gerum
    /// </summary>

    public class DateValidator : ValidationAttribute
    {
        /// <summary>
        /// Funktion für die Fehlermessage, sollte die Validation Fehlschlagen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public override string FormatErrorMessage(string name)
        {
            return "Datum kann nicht in der Zukunft liegen";
        }

        /// <summary>
        /// Funktion zur Überpüfung ob Datum nicht in der Zukunft liegt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        protected override ValidationResult IsValid(object objValue,
                                                    ValidationContext validationContext)
        {
            var dateValue = objValue as DateTime? ?? new DateTime();

            if (dateValue.Date > DateTime.Now.Date)
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
            return ValidationResult.Success;
        }
    }

    /// <summary>
    /// Custom Coin Validator um sicherzustellen, dass der Eingegebene COin beim Erstellen oder Bearbeiten einer Transaktion der Liste aller supporteten Coins entpsricht (Im Nachhinein hätte diese Validierung vermutlich im Controller stattfinden sollen)
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class ValidCoinsAttribute : ValidationAttribute
    {

        List<string> _args;

        /// <summary>
        /// Konstrukto für den Validator, der die Vergleichswerte Liefert
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public ValidCoinsAttribute()
        {
            CoinGecko cg = new CoinGecko();
            _args = cg.getAllCoinsList();
        }

        /// <summary>
        /// Überprüfung ob der Eingegebene Coin der Liste der Erlaubten Coins entspricht
        /// CodeOwner: Thomas Gerum
        /// </summary>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (_args.Contains((string)value))
                return ValidationResult.Success;
            return new ValidationResult("Kein Erlaubter Coin");
        }
    }

    /// <summary>
    /// Coinmengenvalidator, dass keine negative Menge oder 0 an coins spezifiziert werden kann
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class AmountValidator : ValidationAttribute
    {
        /// <summary>
        /// Funktion für die Fehlermessage, sollte die Validation Fehlschlagen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public override string FormatErrorMessage(string name)
        {
            return "Menge darf nicht kleiner gleich 0 sein";
        }

        /// <summary>
        /// Überprüfung, ob die Menge der Coins größer 0 ist
        /// CodeOwner: Thomas Gerum
        /// </summary>
        protected override ValidationResult IsValid(object objValue,
                                                    ValidationContext validationContext)
        {
            double inputValue = (double)objValue;

            if (inputValue <= 0)
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
            return ValidationResult.Success;
        }
    }
}