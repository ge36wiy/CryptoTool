using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;


namespace Projekt.Utilities
{
    /// <summary>
    /// Die Klasse Asset fasst Transaktionen eines Kunden und Coins zusammen und verrechnet sie
    /// CodeOwner: Sebastian Klee
    /// </summary>
    public class Asset
    {
        /// <summary>
        /// Name des Coins
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// momentane Anzahl des Coins
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double Quantity { get; set; }
        /// <summary>
        /// momentane Anzahl des Coins über Haltefrist
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double QuantityOverHoldingPeriod { get; set; }
        /// <summary>
        /// momentaner Preis pro Coin
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double Rate { get; set; }
        /// <summary>
        /// momentaner Gesamtwert des Coins
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double TotalValue { get; set; }
        /// <summary>
        ///momentaner Gesamtwert des Coins über Haltefrist
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double TotalValueOverHoldingPeriod { get; set; }
        /// <summary>
        ///Link zu Icon des Coins
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public string LinkToImage { get; set; }
        /// <summary>
        ///List der Teilassets
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public List<Transaction> Transactions { get; set; }
    }
}