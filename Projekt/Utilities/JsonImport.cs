using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.ViewModels;
using Projekt.Container;
using Projekt.Utilities;
using System.Text.Json;
using System.IO;
using Projekt.Interfaces;
using Projekt.Controllers;

namespace Projekt.Utilities
{
    /// <summary>
    /// Diese Klasse ermoeglicht den Import von Transaktionen ber Json Dateien. Dafuer wird die Schnittstelle IImportFile verwendet, 
    /// um sicher zustellen, dass die Quelldatei richtig geparst wird.
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class JsonImport : IImportFile
    {
        /// <summary>
        /// Liest Transaktionen aus einer Datei aus einer Json Datei aus.
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="file"> Ein Json File das durch die View bergeben wird, um die darin enthaltenden Transaktionen zu importieren</param>
        /// <returns> Liste mit Transaktionen</returns>
        public List<Transaction> parseData(IFormFile file)
        {
            List<Transaction> transactionList = new List<Transaction>();
            CoinGecko coinGecko = new CoinGecko();
            List<string> coinList = coinGecko.getAllCoinsList();
            try
            {
                // Create an instance of StreamReader to read from a file.
                StreamReader sr = new StreamReader(file.OpenReadStream());
                string transactionsAsText = sr.ReadToEnd();
                Transaction[] transactionsList = JsonSerializer.Deserialize<Transaction[]>(transactionsAsText);
                foreach (Transaction transaction in transactionsList)
                {
                    if (transaction != null && transaction.Value >= 0 && coinList.Contains(transaction.Coin) && Enum.IsDefined(typeof(TransactionType), transaction.Type) && transaction.Fee >= 0 && transaction.Rate >= 0 && transaction.Amount >= 0 && transaction.Value >= 0 && transaction.PurchaseDate <= DateTime.Now)
                    {
                        transaction.Remainder = transaction.Amount;
                        transaction.Tradeable = transaction.PurchaseDate.AddYears(1);
                        transactionList.Add(transaction);
                    }
                    else
                    {
                        return new List<Transaction>();
                    }
                }
                //Ueberprfe die Liste, ob alle Verkauften Coins vorher im Bestand waren
                transactionList = ValidateSellAmount(transactionList);
                sr.Close();
                return transactionList;
            }
            catch (JsonException e)
            {
                Console.WriteLine("The file is not correctly formatted");
                Console.WriteLine(e.Message);
                return new List<Transaction>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + e.InnerException);
                return new List<Transaction>();
            }
        }

        /// <summary>
        /// ueberprueft eine Liste von Transaktionen, ob die Verkauften Coins vorher im auch im Bestand waren
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="transactionList"> Liste der Transaktionen die berprft werden soll</param>
        /// <returns> Liste mit Transaktionen</returns>
        public List<Transaction> ValidateSellAmount(List<Transaction> transactionList)
        {
            Dictionary<string, Double> CheckPositivCoinAmount = new Dictionary<string, double>();
            List<Transaction> sortedTransactionList = transactionList.OrderBy(transaction => transaction.PurchaseDate).ToList();
            //Ueberprfe ob die Verkauften Coins vorher auch im Bestand waren
            foreach (Transaction transactionToAggregate in sortedTransactionList)
            {
                string coin = transactionToAggregate.Coin;
                double amount = transactionToAggregate.Amount;
                TransactionType type = transactionToAggregate.Type;
                if (!CheckPositivCoinAmount.ContainsKey(coin) && type == TransactionType.Kauf)
                {
                    CheckPositivCoinAmount.Add(coin, amount);
                }
                else if (!CheckPositivCoinAmount.ContainsKey(coin) && type == TransactionType.Verkauf)
                {
                    return new List<Transaction>();
                }
                else if (CheckPositivCoinAmount.ContainsKey(coin) && type == TransactionType.Verkauf)
                {
                    CheckPositivCoinAmount[coin] -= amount;
                }
                else if (CheckPositivCoinAmount.ContainsKey(coin) && type == TransactionType.Kauf)
                {
                    CheckPositivCoinAmount[coin] += amount;
                }
            }
            foreach (string key in CheckPositivCoinAmount.Keys)
            {
                if (CheckPositivCoinAmount[key] < 0)
                {
                    Console.WriteLine("Es knnen keine Coins verkauft werden, die nicht im Bestand waren");
                    return new List<Transaction>();
                }
            }
            return sortedTransactionList;
        }
    }
}