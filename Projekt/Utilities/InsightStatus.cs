﻿namespace Projekt.Utilities
{
    /// <summary>
    /// Enumeration für die Art von Einsichten welcher der Steuerberater besitzt
    /// CodeOwner: Alexander Rioux, Christopher Glöckel
    /// </summary>
    public enum InsightStatus
    {
        /// <summary>
        /// Einsicht
        /// CodeOwner: Alexander Rioux, Christopher Glöckel
        /// </summary>
        Insight,


        /// <summary>
        /// Beantragt
        /// CodeOwner: Alexander Rioux, Christopher Glöckel
        /// </summary>
        ApplicationInsight,


        /// <summary>
        /// Keine Einsicht
        /// CodeOwner: Alexander Rioux, Christopher Glöckel
        /// </summary>
        None
    }
}