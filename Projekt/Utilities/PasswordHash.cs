using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;


namespace Projekt.Utilities
{

    /// <summary>
    /// Stellt das Hashing für Passwörter zur Verfügung
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public static class PasswordHash
    {

        /// <summary>
        /// Hasht den eingegebenen String mit SHA256
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="text"> eingegebener String (Passwort)</param>
        /// <returns>gehashter string des eingegebenen Passworts</returns>
        public static string CreateHash(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return String.Empty;
            }

            using (SHA256 sha = SHA256.Create())
            {
                byte[] textBytes = System.Text.Encoding.UTF8.GetBytes(text);
                byte[] hashBytes = sha.ComputeHash(textBytes);

                string hash = BitConverter
                    .ToString(hashBytes)
                    .Replace("-", String.Empty);

                return hash;
            }
        }
    }
}