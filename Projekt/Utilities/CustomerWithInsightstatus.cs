using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;

namespace Projekt.Utilities
{
    /// <summary>
    /// Die Klasse <c>CustomerWithInsightStatus</c> bildet ein Model um Einsichten und Kunden zuzuordnen
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class CustomerWithInsightstatus
    {
        /// <summary>
        /// der Betrachtete Kunde
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public Customer customer { get; set; }
        /// <summary>
        /// seine einfache Einsicht
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public InsightStatus insightSimple { get; set; }
        /// <summary>
        /// seine erweiterte Einsicht
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public InsightStatus insightExtended { get; set; }
        /// <summary>
        /// seine einfache Einsicht als String
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public String insightSimpleString { get; set; }
        /// <summary>
        /// seine erweiterete Einsicht als String
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public String insightExtendedString { get; set; }


    }
}