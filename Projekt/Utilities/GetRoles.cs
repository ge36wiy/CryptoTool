using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Projekt.Utilities
{
    /// <summary>
    /// Auslagerung der Funktion die aktuellen Rollen des Users zurückzugeben
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public static class GetRoles
    {

        /// <summary>
        /// Gibt eine Liste an geclaimten Rollen eines Users Zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="principal"> Claim Principal aus dem die ROllen ausgelesen werden sollen</param>
        /// <returns>Liste der Rollen des Eingabeparameters</returns>
        public static List<string> Roles(this ClaimsPrincipal principal)
        {
            return new List<string>(principal.Identities.SelectMany(i =>
                {
                    return i.Claims
                        .Where(c => c.Type == i.RoleClaimType)
                        .Select(c => c.Value)
                        .ToList();
                }));
        }
    }
}