using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;

namespace Projekt.Utilities
{
    /// <summary>
    /// Die Klasse <c>ConsultantWithCustomerList</c> erstellt ein Model welches einen Steuerberater zu einer Liste von Kunden Zuordnet
    /// Nutzer: Steuerberater
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class ConsultantWithCustomerList
    {
        /// <summary>
        /// Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public Consultant Consultant { get; set; }
        /// <summary>
        /// Liste von Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<Customer> Customers { get; set; }
    }
}