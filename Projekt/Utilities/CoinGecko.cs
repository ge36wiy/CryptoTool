using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Interfaces;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Projekt.Utilities
{
    /// <summary>
    /// Diese Klasse liefert jegliche Daten die wir von externen Schnittstellen für Coins erhalten.
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class CoinGecko : ICoininfo
    {

        Dictionary<string, string> dict = new Dictionary<string, string>();

        /// <summary>
        /// Konstruktor, der das Dictionary befüllt, sodass bei Ableitungen von diesem Dictionary kein neuer API Call gesendet werden Muss
        /// es wird versucht einen API Call für eine komplette Coinliste durchzuführen, welche dann in dem KlassenAttribut Dictionary geparst und abgespeichert wird. Außerdem wird die JSON Datei als Backup abgelegt
        /// Schlägt das aufgrund eines API Fehlers fehl, wird die Backup datei eingelesen und geparst und diese zurückgegeben. Außerdem wird in das DIctionary ein Wert geaddet, der signalisiert dass die Daten nicht aktuell sind. Dies wird in der jeweiligen View mit einer IF Abfrage gehandelt.
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public CoinGecko()
        {
            try
            {
                WebRequest request = WebRequest.Create("https://api.coingecko.com/api/v3/coins/list");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                File.WriteAllText(@"./Utilities/backupCoinList.json", responseFromServer);
                dynamic stuff = JsonConvert.DeserializeObject(responseFromServer);
                foreach (var item in stuff)
                {
                    if (!dict.ContainsKey((string)item.name))
                        dict.Add((string)item.name, (string)item.id);
                }
                Debug.WriteLine("Success");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                dict.Add("backup", "true");
                using (StreamReader reader = new StreamReader(@"./Utilities/backupCoinList.json"))
                {
                    string backupfile = reader.ReadToEnd();
                    dynamic stuff = JsonConvert.DeserializeObject(backupfile);
                    foreach (var item in stuff)
                    {
                        if (!dict.ContainsKey((string)item.name))
                            dict.Add((string)item.name, (string)item.id);
                    }

                }
            }
        }

        /// <summary>
        /// Gibt das Dicitonary das im Konstruktor erstellt wird zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Dictionary das im Kontruktor erstellt wird</returns>
        public Dictionary<string, string> getAllCoins()
        {
            return dict;
        }


        /// <summary>
        /// Beschneidet das DIctionary aus dem Konstruktor und gibt nur eine Liste der jeweiligen Keys zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Liste der Keys aus dem Dicitonary aller Supporteten Coins</returns>
        public List<string> getAllCoinsList()
        {

            try
            {
                List<string> list = new List<string>(dict.Keys);
                return list;
            }
            catch (Exception e)
            {
                List<string> res = new List<string>();
                Debug.WriteLine(e);
                res.Add("Bitcoin");
                res.Add("Ethereum");
                res.Add("Bitcoin Cash");
                res.Add("Ripple");
                res.Add("EOS");
                res.Add("tezos");
                return res;
            }

        }

        /// <summary>
        /// Berechnet mithilfe von Coingecko API den momentanen Kurs aller übergebenen Assetnamen.
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="names"> Liste von Assetnamen.</param>
        /// <returns>Dictionary, Assetname zu momentaner Kurs dieses Assets.</returns>
        public Dictionary<string, double> getRates(List<string> names)
        {
            Dictionary<string, double> nameToRate = new Dictionary<string, double>();
            Dictionary<string, string> nameToID = new Dictionary<string, string>();
            Dictionary<string, double> idToRate = new Dictionary<string, double>();
            List<string> coins = new List<string>();
            //Dictionary Assetname zu assetID und Liste von IDs für Rate Abfrage
            foreach (String name in names)
            {
                if (dict.ContainsKey(name))
                {
                    nameToID.Add(name, dict[name]);
                    coins.Add(dict[name]);
                }
            }
            //erstellt Abfrage, um alle gefragten Raten mit einer Abfrage zu finden
            string allCoins = string.Join("%2C", coins);
            List<string> queryParts = new List<string>() { "https://api.coingecko.com/api/v3/simple/price?ids=", allCoins, "&vs_currencies=eur" };
            string query = string.Join("", queryParts);

            try
            {
                WebRequest request = WebRequest.Create(query);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                dynamic stuff = JsonConvert.DeserializeObject(responseFromServer);

                //extrahiert CoinId und Rate aus Json um Dictionary AssetId zu Rate zu erstellen
                foreach (dynamic coin in stuff)
                {
                    string coinId = coin.Name;
                    double coinRate = 0;
                    foreach (dynamic item in coin)
                    {
                        coinRate = item.eur;
                    }
                    idToRate.Add(coinId, coinRate);

                }

                //updates Backup
                Dictionary<string, double> backup = new Dictionary<string, double>();
                using (StreamReader sreader = new StreamReader(@"Utilities/backupRates.json"))
                {
                    string backupfile = sreader.ReadToEnd();
                    if (backupfile.Length > 0)
                    {
                        backup = JsonConvert.DeserializeObject<Dictionary<string, double>>(backupfile);
                    }
                }
                foreach (KeyValuePair<string, double> entry in idToRate)
                {
                    if (backup.ContainsKey(entry.Key))
                    {
                        backup[entry.Key] = entry.Value;
                    }
                    else
                    {
                        backup.Add(entry.Key, entry.Value);
                    }
                }
                File.WriteAllText(@"Utilities/backupRates.json", JsonConvert.SerializeObject(backup));
            }
            catch (Exception)
            {
                //verwendet Backup wenn Api überlastet
                using (StreamReader sreader = new StreamReader(@"Utilities/backupRates.json"))
                {
                    string backupfile = sreader.ReadToEnd();
                    idToRate = JsonConvert.DeserializeObject<Dictionary<string, double>>(backupfile);
                }
            }

            //kombiniert Dictionaries Assetname zu Id und AssetId zu Rate zu Dictionary Assetname zu Rate 
            foreach (KeyValuePair<string, string> entry in nameToID)
            {
                if (idToRate.ContainsKey(entry.Value))
                {
                    nameToRate.Add(entry.Key, idToRate[entry.Value]);
                }
            }

            return nameToRate;
        }

        /// <summary>
        /// Bestimmt mithilfe von Coingecko API den Link zu Icons aller übergebenen Assetnamen.
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="names"> Liste von Assetnamen.</param>
        /// <returns>Dictionary, Assetname zu Link von Icon dieses Assets.</returns>
        public Dictionary<string, string> getPictures(List<string> names)
        {
            Dictionary<string, string> nameToPic = new Dictionary<string, string>();
            Dictionary<string, string> nameToID = new Dictionary<string, string>();
            Dictionary<string, string> idToPic = new Dictionary<string, string>();
            List<string> coins = new List<string>();
            //Dictionary Assetname zu assetID und Liste mit IDs für Icon Link Abfrage
            foreach (String name in names)
            {
                if (dict.ContainsKey(name))
                {
                    nameToID.Add(name, dict[name]);
                    coins.Add(dict[name]);
                }
            }

            //erstellt Abfrage, um alle gefragten Links mit einer Abfrage zu finden
            string allCoins = string.Join("%2C", coins);
            List<string> queryParts = new List<string>() { "https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur&ids=", allCoins, "&order=market_cap_desc&per_page=100&page=1&sparkline=false&locale=en" };
            string query = string.Join("", queryParts);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(query);
                request.UserAgent = ".NET Framework Test Client";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                dynamic stuff = JsonConvert.DeserializeObject(responseFromServer);

                //extrahiert CoinId und Link aus Json um Dictionary AssetId zu Link zu erstellen
                foreach (var coin in stuff)
                {
                    idToPic.Add((string)coin.id, (string)coin.image);

                }

                //updates Backup
                Dictionary<string, string> backup = new Dictionary<string, string>();
                using (StreamReader sreader = new StreamReader(@"Utilities/backupImageLinks.json"))
                {
                    string backupfile = sreader.ReadToEnd();
                    if (backupfile.Length > 0)
                    {
                        backup = JsonConvert.DeserializeObject<Dictionary<string, string>>(backupfile);
                    }
                }
                foreach (KeyValuePair<string, string> entry in idToPic)
                {
                    if (backup.ContainsKey(entry.Key))
                    {
                        backup[entry.Key] = entry.Value;
                    }
                    else
                    {
                        backup.Add(entry.Key, entry.Value);
                    }
                }
                File.WriteAllText(@"Utilities/backupImageLinks.json", JsonConvert.SerializeObject(backup));
            }
            catch (Exception)
            {
                //verwendet Backup wenn Api überlastet
                using (StreamReader sreader = new StreamReader(@"Utilities/backupImageLinks.json"))
                {
                    string backupfile = sreader.ReadToEnd();
                    idToPic = JsonConvert.DeserializeObject<Dictionary<string, string>>(backupfile);
                }
            }

            //kombiniert Dictionaries Assetname zu Id und AssetId zu Link zu Dictionary Assetname zu Link 
            foreach (KeyValuePair<string, string> entry in nameToID)
            {
                if (idToPic.ContainsKey(entry.Value))
                {
                    nameToPic.Add(entry.Key, idToPic[entry.Value]);
                }
            }
            return nameToPic;
        }
    }
}