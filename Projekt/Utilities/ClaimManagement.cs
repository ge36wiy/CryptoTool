using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Projekt.Models;
using System.Runtime.Serialization;

namespace Projekt.Utilities
{
    /// <summary>
    /// Klasse die das Login Claim Management auslagert
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class ClaimManagement
    {

        /// <summary>
        /// ClaimIdentity
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public ClaimsIdentity identity;

        /// <summary>
        /// Claim Eigenschaften
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public AuthenticationProperties props;

        /// <summary>
        /// Konstruktor zum erstellen des ClaimMgmt Objekts
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="account"> Claimt den speziellen Account als eigeloggt</param>
        /// <param name="role"> Claimt die jeweilige Rolle für den einzuloggenden User</param>
        public ClaimManagement(Account account, String role)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, account.Username),
                    new Claim(ClaimTypes.Role, role),
                };

            identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            props = new AuthenticationProperties
            {

            };
        }

        /// <summary>
        /// Konstruktor zum erstellen des ClaimMgmt Objekts
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="account"> Claimt den speziellen Account als eigeloggt</param>
        /// <param name="role"> Claimt die jeweilige Rolle für den einzuloggenden User</param>
        /// <param name="role2"> Claimt die jeweilige Rolle für den einzuloggenden User</param>
        /// <param name="role3"> Claimt die jeweilige Rolle für den einzuloggenden User</param>
        public ClaimManagement(Account account, string role, string role2, string role3)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, account.Username),
                    new Claim(ClaimTypes.Role, role),
                    new Claim(ClaimTypes.Role, role2),
                    new Claim(ClaimTypes.Role, role3),
                };

            identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            props = new AuthenticationProperties
            {

            };
        }


    }
}