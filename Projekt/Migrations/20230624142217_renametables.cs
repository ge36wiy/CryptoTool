﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Projekt.Migrations
{
    /// <inheritdoc />
    public partial class renametables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Steuerberater_Pathtoimage",
                table: "Account",
                newName: "Customer_Pathtoimage");

            migrationBuilder.RenameColumn(
                name: "Steuerberater_Email",
                table: "Account",
                newName: "Customer_Email");

            migrationBuilder.RenameColumn(
                name: "Vorname",
                table: "Account",
                newName: "Lastname");

            migrationBuilder.RenameColumn(
                name: "Steuerberater_Vorname",
                table: "Account",
                newName: "Firstname");

            migrationBuilder.RenameColumn(
                name: "Steuerberater_Nachname",
                table: "Account",
                newName: "Customer_Lastname");

            migrationBuilder.RenameColumn(
                name: "Steuerberater_Adresse",
                table: "Account",
                newName: "Customer_Firstname");

            migrationBuilder.RenameColumn(
                name: "Nachname",
                table: "Account",
                newName: "Customer_Address");

            migrationBuilder.RenameColumn(
                name: "Adresse",
                table: "Account",
                newName: "Address");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Customer_Pathtoimage",
                table: "Account",
                newName: "Steuerberater_Pathtoimage");

            migrationBuilder.RenameColumn(
                name: "Customer_Email",
                table: "Account",
                newName: "Steuerberater_Email");

            migrationBuilder.RenameColumn(
                name: "Lastname",
                table: "Account",
                newName: "Vorname");

            migrationBuilder.RenameColumn(
                name: "Firstname",
                table: "Account",
                newName: "Steuerberater_Vorname");

            migrationBuilder.RenameColumn(
                name: "Customer_Lastname",
                table: "Account",
                newName: "Steuerberater_Nachname");

            migrationBuilder.RenameColumn(
                name: "Customer_Firstname",
                table: "Account",
                newName: "Steuerberater_Adresse");

            migrationBuilder.RenameColumn(
                name: "Customer_Address",
                table: "Account",
                newName: "Nachname");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Account",
                newName: "Adresse");
        }
    }
}
