﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Projekt.Migrations
{
    /// <inheritdoc />
    public partial class consultanttocustomer : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Fee",
                table: "Transaction",
                type: "TEXT",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL");

            migrationBuilder.CreateTable(
                name: "ConsultantToCustomer",
                columns: table => new
                {
                    IdConsultant = table.Column<int>(type: "INTEGER", nullable: false),
                    IdCustomer = table.Column<int>(type: "INTEGER", nullable: false),
                    InsightStatusSimple = table.Column<int>(type: "INTEGER", nullable: false),
                    InsightStatusExtended = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConsultantToCustomer", x => new { x.IdConsultant, x.IdCustomer });
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConsultantToCustomer");

            migrationBuilder.AlterColumn<double>(
                name: "Fee",
                table: "Transaction",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "TEXT");
        }
    }
}
