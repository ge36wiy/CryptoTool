﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Projekt.Migrations
{
    /// <inheritdoc />
    public partial class CustomerBoolean : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adresse",
                table: "Account");

            migrationBuilder.AddColumn<bool>(
                name: "Customer",
                table: "Account",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Customer",
                table: "Account");

            migrationBuilder.AddColumn<string>(
                name: "Adresse",
                table: "Account",
                type: "TEXT",
                nullable: true);
        }
    }
}
