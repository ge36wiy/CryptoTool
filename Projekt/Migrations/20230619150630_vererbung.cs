﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Projekt.Migrations
{
    /// <inheritdoc />
    public partial class vererbung : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Kunde");

            migrationBuilder.DropTable(
                name: "Steuerberater");

            migrationBuilder.DropColumn(
                name: "Customer",
                table: "Account");

            migrationBuilder.AddColumn<string>(
                name: "Adresse",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Account",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nachname",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Pathtoimage",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Steuerberater_Adresse",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Steuerberater_Email",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Steuerberater_Nachname",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Steuerberater_Pathtoimage",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Steuerberater_Vorname",
                table: "Account",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Vorname",
                table: "Account",
                type: "TEXT",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adresse",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Nachname",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Pathtoimage",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Steuerberater_Adresse",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Steuerberater_Email",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Steuerberater_Nachname",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Steuerberater_Pathtoimage",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Steuerberater_Vorname",
                table: "Account");

            migrationBuilder.DropColumn(
                name: "Vorname",
                table: "Account");

            migrationBuilder.AddColumn<bool>(
                name: "Customer",
                table: "Account",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Kunde",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Adresse = table.Column<string>(type: "TEXT", nullable: true),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Nachname = table.Column<string>(type: "TEXT", nullable: true),
                    Pathtoimage = table.Column<string>(type: "TEXT", nullable: true),
                    Vorname = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kunde", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Steuerberater",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Adresse = table.Column<string>(type: "TEXT", nullable: true),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Nachname = table.Column<string>(type: "TEXT", nullable: true),
                    Pathtoimage = table.Column<string>(type: "TEXT", nullable: true),
                    Vorname = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Steuerberater", x => x.Id);
                });
        }
    }
}
