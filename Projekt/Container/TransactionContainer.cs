using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;
using Projekt.Utilities;
using System.Diagnostics;

namespace Projekt.Container
{
    /// <summary>
    /// Verwaltung für Datenbankoperationen mit Transaktionen
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class TransactionContainer
    {
        private readonly MvcAccountContext _context;

        /// <summary>
        /// Konstruktor zum erstellen des Containers
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public TransactionContainer(MvcAccountContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gibt Alle Transaktionen als Liste zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Liste von ALLEN Transaktionen</returns>
        public List<Transaction> GetAllTransactions()
        {
            List<Transaction> list = new List<Transaction>();
            list = _context.Transaction.ToList();
            return list;
        }

        /// <summary>
        /// Legt eine neue Transaktion in der Datenbank an
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="trans">Transaktion, die vom User eingegeben wurde und durch den Controller hier verarbeitet wird</param>
        public void CreateTransaction(Transaction trans)
        {
            _context.Transaction.Add(trans);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gibt Alle Transaktionen eines bestimmen Users als Liste zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id">KundenId für den Die Transaktionen abgerufen werden sollen</param>
        /// <returns> Liste von ALLEN Transaktionen</returns>
        public List<Transaction> GetAllTransactionsCustomer(int id)
        {
            List<Transaction> list = new List<Transaction>();
            list = _context.Transaction.Where(e => e.OwnerId == id).ToList();
            list.RemoveAll(e => e.Deleted == true);
            return list;
        }

        /// <summary>
        /// Aktualisiert eine Transaktion bzw. bearbeitet sie
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="transaction">Transaktion, die vom User eingegeben wurde und durch den Controller hier verarbeitet wird</param>
        public void EditTransaction(Transaction transaction)
        {
            _context.Update(transaction);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gibt eine gesuchte Transaktion zurück
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id">Id der gesuchten Transaktion</param>
        /// <returns> die gesuchte Transaktion </returns>
        public Transaction FindTransaction(int id)
        {
            return _context.Transaction.Find(id);
        }

        /// <summary>
        /// Setzt eine Flag für deleted bei einer eingegebenen Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="transaction"> zu löschende Transaktion</param>
        /// <returns> void </returns>
        public void DeleteTransaction(Transaction transaction)
        {
            transaction.Deleted = true;
            _context.Update(transaction);
            _context.SaveChanges();
        }

        /// <summary>
        /// kreiert aus allen Transaktionen der übergebenen Kunden-ID die zugehörigen Assets
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="ID"> ID des Kunden für den Assets berechnet werden sollen</param>
        /// <returns> Liste von fertigen Assets für AssetOverview von ID </returns>
        public List<Asset> CreateAssets(int ID)
        {
            var Transactionlist = GetAllTransactionsCustomer(ID);
            var orderedByCoin = OrderByCoin(Transactionlist);
            var augmentedAssets = AugmentTransactions(orderedByCoin);
            if (augmentedAssets.Count > 0 && augmentedAssets[0].Quantity == -1)
            {
                //Coins wurden verkauft aber nicht gekauft
                return augmentedAssets;
            }
            var quantityOverHoldingPeriodSet = SetQuantityOverHoldingPeriod(augmentedAssets);
            return SetPricesAndIcons(quantityOverHoldingPeriodSet);
        }

        /// <summary>
        /// kreiert aus allen Transaktionen der übergebenen Kunden-ID die zugehörigen Assets für die ExtendedInsight
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="ID"> ID des Kunden für den Assets berechnet werden sollen</param>
        /// <returns> Liste von fertigen Assets für AssetOverview von ID </returns>
        public List<Asset> CreateAssetsConsultant(int ID)
        {
            var Transactionlist = GetAllTransactionsCustomer(ID);
            var orderedByCoin = OrderByCoin(Transactionlist);
            var augmentedAssets = calculateTotal(orderedByCoin);
            return SetPricesAndIcons(augmentedAssets);
        }

        /// <summary>
        /// berechnet für jedes Asset die Gesamtzahl Coins
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="assets"> Liste von Assets, die nur Name und alle nach Datum sortierten Transaktionen enthält.</param>
        /// <returns> Liste von Assets mit Titel, Total und allen Transaktionen </returns>
        public List<Asset> calculateTotal(List<Asset> assets)
        {
            foreach (Asset asset in assets)
            {
                double total = 0;
                foreach (Transaction transaction in asset.Transactions)
                {
                    if (transaction.Type == TransactionType.Verkauf)
                    {
                        total -= transaction.Amount;
                    }
                    else
                    {
                        total += transaction.Amount;
                    }
                }
                asset.Quantity = total;
                //Nur Fehlerverhinderung
                asset.QuantityOverHoldingPeriod = 0;
            }
            return assets;
        }

        /// <summary>
        /// Gruppiert Transaktionen nach Coin, und erzeugt daraus Assets, die aber nur Coin-Namen und alle nach Datum sortierten Transaktionen mit diesem Coin enthalten 
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="transactions"> Liste der unverarbeiteten Transaktionen des angemeldeten Kunden</param>
        /// <returns> Liste von Assets, die aber nur Coin-Namen und alle nach Datum sortierten Transaktionen mit diesem Coin enthalten </returns>
        private List<Asset> OrderByCoin(List<Transaction> transactions)
        {
            List<Asset> TransactionByCoin = new List<Asset>();
            List<string> Coins = new List<string>();
            //fügt Transaktion zu bestehendem Asset hinzu, oder erzeugt neues Asset
            foreach (Transaction CurrentTransaction in transactions)
            {
                if (Coins.Contains(CurrentTransaction.Coin))
                {
                    Asset FoundAsset = TransactionByCoin.Find(a => a.Name == CurrentTransaction.Coin);
                    FoundAsset.Transactions.Add(CurrentTransaction);
                }
                else
                {
                    Asset asset = new Asset { Name = CurrentTransaction.Coin, Transactions = new List<Transaction> { CurrentTransaction } };
                    TransactionByCoin.Add(asset);
                    Coins.Add(CurrentTransaction.Coin);
                }
            }

            //sortiert Transaktionen aufsteigend nach Datum
            foreach (Asset currentAsset in TransactionByCoin)
            {
                currentAsset.Transactions.Sort((x, y) => x.PurchaseDate.CompareTo(y.PurchaseDate));
            }

            //sortiert Transaktionen aufsteigend nach Coinname
            TransactionByCoin.Sort((x, y) => x.Name.CompareTo(y.Name));

            return TransactionByCoin;
        }

        /// <summary>
        /// verrechnet in jedem Asset Objekt die Verkäufe mit den Käufen/ Mining nach FiFo-Prinzip um Bestand zu berechnen.  
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="orderedByCoin"> Liste von Assets, die nur Name und alle nach Datum sortierten Transaktionen enthält.</param>
        /// <returns>Liste von Assets mit Titel, Gesamtbestand und Teilassets.</returns>
        private List<Asset> AugmentTransactions(List<Asset> orderedByCoin)
        {
            List<Asset> faultyAssets = new List<Asset>();
            List<Asset> augmentedAssets = new List<Asset>();
            foreach (Asset currentAsset in orderedByCoin)
            {
                Asset newAsset = new Asset { Name = currentAsset.Name, Transactions = new List<Transaction>() };
                foreach (Transaction currentTransaction in currentAsset.Transactions)
                {
                    if (currentTransaction.Type == TransactionType.Verkauf)
                    {
                        //zieht Verkauf von Käufen nach FiFo Prinzip ab
                        int i = 0;
                        while (currentTransaction.Remainder > 0)
                        {
                            //Käufe wurden nicht eingetragen
                            if (i >= newAsset.Transactions.Count)
                            {
                                newAsset.Quantity = -1;
                                faultyAssets.Add(newAsset);
                                break;
                            }
                            double diff = Math.Min(currentTransaction.Remainder, newAsset.Transactions[i].Remainder);
                            currentTransaction.Remainder -= diff;
                            newAsset.Transactions[i].Remainder -= diff;
                            i++;
                        }
                        newAsset.Transactions.RemoveAll(x => x.Remainder == 0);
                    }
                    else
                    {
                        //fügt neues Teilasset hinzu
                        newAsset.Transactions.Add(currentTransaction);
                    }
                }
                double amount = 0;
                //berechnet Gesamtanzahl von Coins pro Asset
                for (int i = 0; i < newAsset.Transactions.Count; i++)
                {
                    amount += newAsset.Transactions[i].Remainder;
                }
                if (amount > 0)
                {
                    newAsset.Quantity = amount;
                    augmentedAssets.Add(newAsset);
                }
            }
            if (faultyAssets.Count == 0)
            {
                //Wenn es keine fehlenden Transaktionen gab, wird Liste mit Assets zurckgegeben, die Name, Gesamtbestand und modifizierte Transaktionen beinhalten 
                return augmentedAssets;
            }
            //Wenn es fehlende Transaktionen gab, wird Liste mit fehlerhaften Coins zurückgegeben
            return faultyAssets;
        }

        /// <summary>
        /// Berechnet Anzahl Coins über Haltefrist für jedes übergebene Asset.  
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="augmentedAssets"> Liste von Assets, die nur Name und Teilassets enthält.</param>
        /// <returns>Liste von Assets mit Titel, Gesamtbestand, Teilassets und Anzahl Coins über Haltefrist.</returns>
        public List<Asset> SetQuantityOverHoldingPeriod(List<Asset> augmentedAssets)
        {
            foreach (Asset asset in augmentedAssets)
            {
                double amount = 0;
                // summiert Anzahl Coins über Haltefrist pro Asset
                foreach (Transaction transaction in asset.Transactions)
                {
                    if ((DateTime.Today - transaction.PurchaseDate).Days > 365)
                    {
                        amount += transaction.Remainder;
                    }
                }
                asset.QuantityOverHoldingPeriod = amount;
            }
            return augmentedAssets;
        }

        /// <summary>
        /// Setzt für jedes Asset den momentanen Kurs und Logo. Berechnet aus Kurs Gesamtwert und Gesamtwert über Haltefrist für jedes Asset.
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <param name="augmentedAssets"> Liste von Assets mit Titel, Gesamtbestand, Teilassets und Anzahl Coins über Haltefrist.</param>
        /// <returns>Fertige Liste von Assets die an View übergeben werden kann.</returns>
        public List<Asset> SetPricesAndIcons(List<Asset> augmentedAssets)
        {
            List<string> names = new List<string>();
            //Liste mit allen Coinnamen
            foreach (Asset asset in augmentedAssets)
            {
                names.Add(asset.Name);
            }
            CoinGecko cg = new CoinGecko();
            Dictionary<string, double> nameToRates = new Dictionary<string, double>();
            Dictionary<string, string> nameToPic = new Dictionary<string, string>();
            try
            {
                //bestimmt momentanen Kurs für jedes Asset
                nameToRates = cg.getRates(names);
                //bestimmt Icon für jedes Asset
                nameToPic = cg.getPictures(names);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            foreach (Asset asset in augmentedAssets)
            {
                //fügt Rate aus, falls gefunden
                if (nameToRates.ContainsKey(asset.Name))
                {
                    asset.Rate = nameToRates[asset.Name];
                }
                else
                {
                    asset.Rate = 0;
                }
                //fügt Icon Link ein, falls gefunden
                if (nameToPic.ContainsKey(asset.Name))
                {
                    asset.LinkToImage = nameToPic[asset.Name];
                }
                else
                {
                    asset.LinkToImage = "";
                }
                //berechnet Gesamtwerte aus Rate
                asset.TotalValue = asset.Rate * asset.Quantity;
                asset.TotalValueOverHoldingPeriod = asset.Rate * asset.QuantityOverHoldingPeriod;
            }


            return augmentedAssets;
        }


        /// <summary>
        /// Überprüft, ob ein Verkauf zulässig ist. Falls der Besitz eines Coins zu irgendeinenem Zeitpunkt unter 0 fällt, wird false zurück gegeben.
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="trans"> Transaction die validiert wird</param>
        /// <returns> Boolean mit true für zulässig, false für unzulässig</returns>
        public Boolean IsValidTransaction(Transaction trans)
        {
            if (trans.Type == TransactionType.Verkauf)
            {
                double coins = 0;
                List<Transaction> list = GetAllTransactionsCustomer(trans.OwnerId);
                list.RemoveAll(e => e.Coin != trans.Coin);
                var removeTrans = list.SingleOrDefault(x => x.Id == trans.Id);
                if (removeTrans != null)
                    list.Remove(removeTrans);
                List<Transaction> listafter = new List<Transaction>(list);
                list.RemoveAll(e => e.PurchaseDate > trans.PurchaseDate);
                listafter.RemoveAll(e => e.PurchaseDate <= trans.PurchaseDate);
                foreach (Transaction item in list)
                {
                    if (item.Type == TransactionType.Verkauf)
                    {
                        coins -= item.Amount;
                    }
                    else
                        coins += item.Amount;
                }
                if (trans.Amount > coins)
                    return false;
                coins -= trans.Amount;
                foreach (Transaction item in listafter)
                {
                    if (item.Type == TransactionType.Verkauf)
                    {
                        coins -= item.Amount;
                        if (coins < 0)
                        {
                            return false;
                        }
                    }
                    else
                        coins += item.Amount;
                }

            }

            return true;
        }

        /// <summary>
        /// Überprüft, ob ein Verkauf zulässig ist. Falls der Besitz eines Coins zu irgendeinenem Zeitpunkt unter 0 fällt, wird false zurück gegeben.
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="trans"> Transaction die validiert wird</param>
        /// <returns> Boolean mit true für zulässig, false für unzulässig</returns>
        public Boolean IsValidTransactionDelete(Transaction trans)
        {
            double coins = 0;
            List<Transaction> list = GetAllTransactionsCustomer(trans.OwnerId);
            list.RemoveAll(e => e.Coin != trans.Coin);
            var removeTrans = list.SingleOrDefault(x => x.Id == trans.Id);
            if (removeTrans != null)
                list.Remove(removeTrans);
            foreach (Transaction item in list)
            {
                if (item.Type == TransactionType.Verkauf)
                {
                    coins -= item.Amount;
                    if (coins < 0)
                    {
                        return false;
                    }
                }
                else
                    coins += item.Amount;
            }


            return true;
        }

    }
}