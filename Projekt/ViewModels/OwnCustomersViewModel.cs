using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;
using Projekt.Controllers;

using Projekt.Utilities;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Die Klasse <c>OwnCustomerViewModel</c> bildet ein Model für die OwnCustomer View
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class OwnCustomersViewModel : BasicViewModel
    {
        /// <summary>
        /// Liste der Kunden mit Einsichten
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<CustomerWithInsightstatus> CustomerList { get; set; }
    }
}