using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Utilities;
using Projekt.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekt.ViewModels
{

    /// <summary>
    /// Viewmodel für jegliche Interaktion mit Transaktionen
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class TransactionViewModel : BasicViewModel
    {
        /// <summary>
        /// Id einer Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("ID")]
        public int Id { get; set; }

        /// <summary>
        /// Name der Kryptowährung
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Kryptowährung")]
        public String Coin { get; set; }

        /// <summary>
        /// Art der Transaktion (Kauf, Verkauf oder Mining)
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Transaktionstyp")]
        public TransactionType Type { get; set; }

        /// <summary>
        /// Kurs des Coins zum Zeitpunkt des Kaufs
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [Required(ErrorMessage = "Es muss eine Einkaufsrate spezifiziert werden")]
        [DisplayName("Kurs")]
        public double Rate { get; set; }

        /// <summary>
        /// Gebühr der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Gebühr")]
        public decimal Fee { get; set; }

        /// <summary>
        /// Menge eines Coins bei einer Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [Required(ErrorMessage = "Es muss eine Menge an Coins spezifiziert werden")]
        [DisplayName("Menge an Coins")]
        public double Amount { get; set; }

        /// <summary>
        /// Gesamtwert der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Gesamtwert der Transaktion")]
        public double Value { get; set; }

        /// <summary>
        /// Restwert der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public double Remainder { get; set; }

        /// <summary>
        /// Id des Besitzers der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public int OwnerId { get; set; }

        /// <summary>
        /// Datum der Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DateValidator]
        [Required(ErrorMessage = "Es muss eine Datum ausgewählt werden")]
        [DisplayName("Handelsdatum")]
        public DateOnly PurchaseDate { get; set; }

        /// <summary>
        /// Datum als String zur Überprüfung ob das eingegebene Datum in letztem Kalenderjahr liegt, siehe wwwroot\js\Transaction\TransactionInPast.js
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public String JavaScriptDate { get; set; }

        /// <summary>
        /// Datum, ab wann die Transaktion über der Haltefrist liegt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [DisplayName("Handelsdatum")]
        public DateOnly Tradeable { get; set; }

        /// <summary>
        /// Flag um zu überprüfen ob eine Transaktion vom Besitzer gelöscht wurde
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public Boolean Deleted { get; set; }

        /// <summary>
        /// Liste zur AUflistung aller Transaktionen eines Users
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public List<Transaction> Transactionlist { get; set; }

        /// <summary>
        /// Transaktionsobjekt zum Vorbefüllen der Felder bei einer Edit, Details oder Deleteoperation
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public Transaction Transaction { get; set; }

        /// <summary>
        /// Liste aller Validen Coins, die über die Autovervollständigung akzeptiert werden sollen, siehe wwwroot\js\Transaction\autocomplete.js
        /// CodeOwner: Thomas Gerum
        /// </summary>
        [NotMapped]
        public List<string> Coinlist { get; set; }

        /// <summary>
        /// Dictionary um die Coinnamen den Coinids zuzuweisen, um den Kurs eines Coins dynamisch über Javascript abrufen zu können
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public Dictionary<string, string> Coindict { get; set; }

        /// <summary>
        /// Flag um dem User zu signalisieren falls die Coinliste bei diesem Versuch nicht aktualisert werden konnte
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public bool isBackup { get; set; }

        /// <summary>
        /// Flag um zu überprüfen ob ein Verkauf zulässig ist, bzw dass Genug Copins gekauft wurden die Verkauft werden können
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public bool isValid { get; set; }
    }
}