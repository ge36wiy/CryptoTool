using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Projekt.Utilities;

namespace Projekt.ViewModels
{

    /// <summary>
    /// Standardviewmodel für die möglichkeit immer den eingeloggten User mit zu geben
    /// CodeOwner: Thomas Gerum
    /// </summary>
    /// 
    public class BasicViewModel
    {

        /// <summary>
        /// Privates Parameter für eingeloggten Kunden
        /// CodeOwner: Thomas Gerum
        /// </summary>
        private Customer customer;

        /// <summary>
        /// Parameter für eingeloggten Steuerberater
        /// CodeOwner: Thomas Gerum
        /// </summary>
        private Consultant consultant;

        /// <summary>
        /// Zweites Parameter für den eingeloggten Kunden um Set-Methode konfigurieren zu können
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public Customer _Customer
        {
            get { return customer; }
            /// <summary>
            /// Set-Methode, die bei setzen des Customers die Anzahl ausstehender Anfragen ermittelt und in MessageCount speichert
            /// CodeOwner: Christopher Glöckel
            /// </summary>
            set
            {
                customer = value;
                MessageCount = 0;
                if (MvcAccountContext.lastOptions != null)
                {
                    using (var context = new MvcAccountContext(MvcAccountContext.lastOptions))
                    {
                        var query = from con in context.Consultant
                                    join rel in context.ConsultantToCustomer
                                    on con.Id equals rel.IdConsultant
                                    where rel.IdCustomer == value.Id
                                    select rel;
                        foreach (var item in query)
                        {
                            if (item.InsightStatusExtended == InsightStatus.ApplicationInsight)
                            {
                                MessageCount++;
                            }
                            if (item.InsightStatusSimple == InsightStatus.ApplicationInsight)
                            {
                                MessageCount++;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parameter für eingeloggten Admin
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public Admin _Admin { get; set; }


        /// <summary>
        /// Steuerberater bekommt seine Nachrichten zugewiesen
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public Consultant _Consultant
        {
            get { return consultant; }
            set
            {
                consultant = value;
                MessageListConsultant = new List<Messages>() { };
                if (MvcAccountContext.lastOptions != null)
                {
                    using (var context = new MvcAccountContext(MvcAccountContext.lastOptions))
                    {
                        var messages = context.Messsages.Where(x => x.Reciever == consultant.Id);
                        foreach (var item in messages)
                        {
                            MessageListConsultant.Add(item);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Anzahl der ausstehenden Einsichtanfragen des Customers
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public int MessageCount { get; set; }


        /// <summary>
        /// Liste aller Nachrichten des consultants
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<Messages> MessageListConsultant { get; set; }
    }
}