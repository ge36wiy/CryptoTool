using Projekt.Models;
using Projekt.Container;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Die Klasse <c>AllAssignedViewModel</c> bildet das Model für die Overview des Admins über alle Relationen zwischen Kunden und Steuerberater
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class AllAssignedViewModel : BasicViewModel
    {
        /// <summary>
        /// Liste aller Relationen zwischen Kunden und Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<ConsultantToCustomer> ConsultantToCustomers { get; set; }
    }
}