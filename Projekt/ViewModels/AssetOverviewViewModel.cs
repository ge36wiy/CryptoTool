using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;
using Projekt.Utilities;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Die Klasse AssetOverviewViewModel bildet ein Model für die AssetOverview View
    /// CodeOwner: Sebastian Klee
    /// </summary>
    public class AssetOverviewViewModel : BasicViewModel
    {
        private List<Asset> _assetList = new List<Asset>();
        /// <summary>
        ///Gesamtwert aller Assets des Kunden
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public double Total { get; set; }
        /// <summary>
        ///Liste aller Assets des Kunden
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public List<Asset> AssetList
        {
            get => _assetList;
            set
            {
                //berechnet Total on the fly
                foreach (Asset asset in value)
                {
                    Total += asset.TotalValue;
                }
                _assetList = value;
            }
        }

        /// <summary>
        ///Konstruktor, setzt Total auf 0
        /// CodeOwner: Sebastian Klee
        /// </summary>
        public AssetOverviewViewModel()
        {
            double Total = 0;
            foreach (Asset asset in AssetList)
            {
                Total += asset.TotalValue;
            }
        }

    }
}
