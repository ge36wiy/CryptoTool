﻿using Projekt.Models;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Wird verwendet um ein TaxReport Objekt in die View zu "schicken"
    /// Code owner: Vladyslav Kovganko
    /// </summary>
    public class TaxReportViewModel : BasicViewModel
    {
        public TaxReport _taxReport { get; set; }
    }
}
