using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Viewmodel für Userlogin
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class LoginViewModel
    {

        /// <summary>
        /// Attribut für den Username für Login
        /// CodeOwner: Thomas Gerum
        /// </summary>

        public string Username { get; set; }

        /// <summary>
        /// Attribut für das Passwort für Login
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public string Password { get; set; }
    }
}