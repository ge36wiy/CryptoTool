using Projekt.Models;
using Projekt.Container;
using Projekt.Utilities;
namespace Projekt.ViewModels
{
    /// <summary>
    /// Die Klasse <c>InsightViewModel</c> bildet ein Model für die Insight und ExtendedInsight View
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class InsightViewModel : BasicViewModel
    {
        /// <summary>
        /// alle Assets des betrachteten Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<Asset> AssetList { get; set; }
    }
}