using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

using Projekt.Models;
using Projekt.Utilities;


namespace Projekt.ViewModels
{
    /// <summary>
    /// Viewmodel fuer die AddCustomerRelationView, welche die Beziehung zwischen Kunden und Steuerberater verwaltet
    /// Nutzer: Steuerberater
    /// CodeOwner: Johannes Bulun
    /// </summary>
    public class AddCustomerRelationViewModel : BasicViewModel
    {
        /// <summary>
        /// List{Consultant} ist eine Liste aller im System hinterlegte Steuerberrater
        /// CodeOwner: Johannes Bulun
        /// </summary>
        public List<Consultant> consultantList { get; set; }
        /// <summary>
        /// List{Customer} ist eine Liste aller im System hinterlegte Kunden
        /// CodeOwner: Johannes Bulun
        /// </summary>
        public List<Customer> customerList { get; set; }
    }
}