﻿using Projekt.Models;
using Projekt.Utilities;

namespace Projekt.ViewModels
{
    /// <summary>
    /// Viewmodel für AccessGrantedView
    /// CodeOwner: Christopher Glöckel
    /// </summary>
    public class AccessViewModel : BasicViewModel
    {
        /// <summary>
        /// Anzuzeigende Consultants mit ihren Einsichtsberechtigungen
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public List<AssignedConsultant> assigned;

        /// <summary>
        /// Konstruktor, der die Liste initialisiert
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        public AccessViewModel()
        {
            assigned = new List<AssignedConsultant>();
        }
    }
}
