using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

using Projekt.Models;
using Projekt.Utilities;
using Projekt.Controllers;


namespace Projekt.ViewModels
{
    /// <summary>
    /// Die Klasse <c>AllCustomerViewModel</c> bildet ein Model für die AllCustomer View
    /// CodeOwner: Alexander Rioux
    /// </summary>
    public class AllCustomersViewModel : BasicViewModel
    {
        /// <summary>
        /// DB Steuerberater zu Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public ConsultantToCustomer consultantToCustomer { get; set; }
        /// <summary>
        /// Liste an Steuerberatern
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<Consultant> ConsultantList { get; set; }
        /// <summary>
        /// Steuerberater zu seinen Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<ConsultantWithCustomerList> ConsultantWithCustomerLists { get; set; }
        /// <summary>
        /// Eigene Kunden des derzeit betrachteten Steuerberaters mit Einsichten
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<CustomerWithInsightstatus> MyCustomerList { get; set; }
        /// <summary>
        /// nicht zugeordnete Kunden
        /// CodeOwner: Alexander Rioux
        /// </summary>
        public List<Customer> NoConsultant { get; set; }
    }
}