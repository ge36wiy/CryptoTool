using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Models;

namespace Projekt.ViewModels
{

    /// <summary>
    /// Alle existierenenden User für Adminübersicht Thomas Gerum
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public class AllAccountViewModel : BasicViewModel
    {

        /// <summary>
        /// Liste für alle existierenden Steuerberater
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public List<Consultant> Consultantlist { get; set; }

        /// <summary>
        /// Liste für alle existierenden Kunden
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public List<Customer> Customerlist { get; set; }

        /// <summary>
        /// Liste für alle existierenden Admins
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public List<Admin> Adminlist { get; set; }
    }
}