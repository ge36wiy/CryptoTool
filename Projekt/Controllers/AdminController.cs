using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.Utilities;
using Projekt.ViewModels;

namespace Projekt.Controllers
{

    /// <summary>
    /// Controller für AdminViews
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly MvcAccountContext _context;

        /// <summary>
        /// Konstruktor für den Admincontroller
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public AdminController(MvcAccountContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Zeigt die Details eines bestimmten Admin Users an
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den Jeweiligen User von dem die Details angezeigt werden sollen</param>
        /// <returns> Details view des spezifizierten Users</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Admin == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admin == null)
            {
                return NotFound();
            }

            return View(admin);
        }


        /// <summary>
        /// Get Methode um einen AdminUser zu erstellen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> View zum erstellen eines Admins</returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post Methode um einen Admin User zu erstellen, Hasht das Passwort mit SHA256, ändert den eingegeben Usernamen in Kleinbuchstaben(um Groß-leinschreibung beim Usernamen irrelevant zu machen) und legt die Daten in der AdminTabelle ab. 
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="admin"> es wird ein Adminobjekt eingegeben mit Username und Passwort das vom User auf der Create Seite eingegeben wurde</param>
        /// <returns> Entweder die Create seite bei Fehler der Validierung oder Die Admin Startseite</returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Password")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                //Password wird gehasht
                admin.Password = PasswordHash.CreateHash(admin.Password);
                //username wird in kleinbuchstaben umgewandelt, um die groß kleinschreibung im username hinfällig zu machen
                admin.Username = admin.Username.ToLower();
                _context.Add(admin);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Account");
            }
            return View(admin);
        }

        /// <summary>
        /// Get Methode zum bearbeiten eines Admin Users
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID zum spezifizieren welcher User bearbeitet werden Soll</param>
        /// <returns> Edit Page des spezifizierten Users</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Admin == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }
            return View(admin);
        }

        /// <summary>
        /// Post Methode um die bearbeiteten Daten in der Datenbank zu speichern. Falls sich das Passwort geändert hat, wird es neu gehasht, falls sich der Username geändert hat, wird dieser in LowerCase geändert und in der Datenbank gespeichert bzw. aktualisiert
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Id des zu bearbeitenden Adminusers</param>
        /// <param name="admin"> es wird ein Adminobjekt eingegeben mit Username und Passwort das vom User auf der Bearbeiten Seite eingegeben wurde</param>
        /// <returns> Bei fehlgeshclagener Validierung GET: Edit bei erfolgreicher Validierung die Admin Startseite</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password")] Admin admin)
        {
            if (id != admin.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Falls sich Passwort geändert hat wird neu gehasht
                    Admin adm = await _context.Admin.FirstOrDefaultAsync(x => x.Id == admin.Id);
                    if (admin.Password != adm.Password)
                    {
                        admin.Password = PasswordHash.CreateHash(admin.Password);
                    }
                    admin.Username = admin.Username.ToLower();
                    _context.Update(admin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdminExists(admin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Account");
            }
            return View(admin);
        }

        /// <summary>
        /// Get Methode für das Löschen eines AdminUsers
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den zu löschenden Adminuser</param>
        /// <returns> Löschview des jeweiligen Users</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Admin == null)
            {
                return NotFound();
            }

            var admin = await _context.Admin
                .FirstOrDefaultAsync(m => m.Id == id);
            if (admin == null)
            {
                return NotFound();
            }

            return View(admin);
        }

        /// <summary>
        /// PostMethode zum Löschen eines Users nach Bestätigung durch den Benutzer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert den zu Löschenden User</param>
        /// <returns> Startseite des eingeloggten Users</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Admin == null)
            {
                return Problem("Entity set 'MvcAccountContext.Admin'  is null.");
            }
            var admin = await _context.Admin.FindAsync(id);
            if (admin != null)
            {
                _context.Admin.Remove(admin);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Account");
        }

        /// <summary>
        /// Adminübersicht über alle existierenden User mit der Möglichkeit neue User anzulegen und zu löschen
        /// Die Daten werden aus der Datenbank abgerufen und an das Viewmodel übergeben
        /// Außerdem wird der aktuell eingeloggte User übergeben
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Viewmodel für die Adminübersicht</returns>
        public async Task<IActionResult> RegisteredAccounts()
        {
            string name = HttpContext.User.Identity.Name;
            AllAccountViewModel data = new AllAccountViewModel();
            data.Consultantlist = await _context.Consultant.ToListAsync();
            data.Customerlist = await _context.Customer.ToListAsync();
            data.Adminlist = await _context.Admin.ToListAsync();
            data._Admin = await _context.Admin.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            return View(data);
        }

        private bool AdminExists(int id)
        {
            return _context.Admin.Any(e => e.Id == id);
        }
    }
}
