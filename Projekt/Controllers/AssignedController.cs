using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.Utilities;
using Projekt.ViewModels;
using Newtonsoft.Json;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.View;
using NuGet.Packaging.Signing;
using Projekt.Controllers;

namespace Projekt.Controllers
{
    /// <summary>
    /// Die Klasse <c>AssignedController</c> regelt die Backend Aufgaben der Steuerberater zu Kunde Relation 
    /// Nutzer: Admin, Steuerberater
    /// CodeOwner: Alexander Rioux
    /// </summary>
    [Authorize(Roles = "Admin,ConsultantRole")]
    public class AssignedController : Controller
    {
        private readonly MvcAccountContext _context;


        /// <summary>
        /// Construktor des Controllers
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="context">Der derzeitige MvcAccountContext</param>
        public AssignedController(MvcAccountContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Leitet zur Index Seite weiter mit den nötigen Informationen des Nutzers
        /// GET: Assigned/Index 
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            AllAssignedViewModel data = new AllAssignedViewModel();
            data.ConsultantToCustomers = _context.ConsultantToCustomer.ToList();
            return View(data);
        }


        /// <summary>
        /// Leitet zur Create Seite weiter
        /// GET: Assigned/Create
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// </summary>
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }


        /// <summary>
        /// Leitet zur Index Seite weiter wenn eine richtige Relation zwischen Nutzern eingegeben wurde
        /// POST: Assigned/Create
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// Spezifische Eigneschaften werden gebindet um vor Overposting-Attacks zu schützen
        /// </summary>
        /// <param name="consultantToCustomer">Die in der Create-View eingegebenen Informationen</param>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdConsultant,IdCustomer,InsightStatusSimple,InsightStatusExtended")] ConsultantToCustomer consultantToCustomer)
        {
            if (!ConsultantAndCustomerExists(consultantToCustomer.IdConsultant, consultantToCustomer.IdCustomer)) return NotFound();
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(consultantToCustomer);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (System.Exception)
            {

                return Problem("Entity already exists");
            }
            return View(consultantToCustomer);
        }


        /// <summary>
        /// Leitet zur AllCustomer Seite weiter wenn eine richtige Relation zwischen Nutzern eingegeben wurde
        /// POST: Assigned/CreateConsultant
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// Spezifische Eigneschaften werden gebindet um vor Overposting-Attacks zu schützen
        /// </summary>
        /// <param name="consultant">Name des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        [HttpPost]
        [Authorize(Roles = "ConsultantRole")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateConsultant(int customerId, String consultant)
        {
            ConsultantToCustomer consultantToCustomer = new ConsultantToCustomer() { };
            int consultantId = -1;
            if (!string.IsNullOrEmpty(consultant))
            {
                string[] consultantParts = consultant.Split(' ');
                int consultantIdTest = int.Parse(consultantParts[0]);
                if (_context.Consultant.ToList().Any(c => c.Id == consultantIdTest))
                {
                    consultantId = int.Parse(consultantParts[0]);
                }
            }
            else
            {
                return NotFound();
            }
            if (!ConsultantAndCustomerExistsGeneral(consultantId, customerId)) return NotFound();
            consultantToCustomer = new ConsultantToCustomer() { IdConsultant = consultantId, IdCustomer = customerId, InsightStatusExtended = InsightStatus.None, InsightStatusSimple = InsightStatus.None };
            try
            {
                _context.Add(consultantToCustomer);
                await _context.SaveChangesAsync();
                var customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);
                var mp = new MessageProcessor() { };
                await mp.AddMessage(_context, consultantId, "Sie wurden als Steuerberater zum Kunden " + customer.Firstname + " " + customer.Lastname + " zugeordnet.");
                return RedirectToAction("AllCustomers", "consultant");
            }
            catch (System.Exception)
            {
                return Problem("Entity already exists");
            }
        }


        /// <summary>
        /// Leitet zur Edit Seite weiter
        /// GET: Assigned/Edit
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? consultantId, int? customerId)
        {
            if (!ConsultantAndCustomerExists(consultantId, customerId)) return NotFound();
            if (consultantId == null || customerId == null || _context.ConsultantToCustomer == null)
            {
                return NotFound();
            }
            var assigned = await _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefaultAsync(m => m.IdCustomer == customerId);
            if (assigned == null)
            {
                return NotFound();
            }
            return View(assigned);
        }


        /// <summary>
        /// Leitet zur Index Seite weiter wenn eine richtige Relation zwischen Nutzern eingegeben wurde
        /// POST: Assigned/Edit
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// Spezifische Eigneschaften werden gebindet um vor Overposting-Attacks zu schützen
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        /// <param name="consultantToCustomer">Die in der Edit-View aktualisierten Informationen</param>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int consultantId, int customerId, [Bind("IdConsultant,IdCustomer,InsightStatusSimple,InsightStatusExtended")] ConsultantToCustomer consultantToCustomer)
        {
            if (!ConsultantAndCustomerExists(consultantId, customerId)) return NotFound();
            if (consultantId != consultantToCustomer.IdConsultant || customerId != consultantToCustomer.IdCustomer)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(consultantToCustomer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Index");
            }
            return View(consultantToCustomer);
        }


        /// <summary>
        /// Leitet zur Delete Seite weiter
        /// GET: Assigned/Delete
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? consultantId, int? customerId)
        {
            if (consultantId == null || customerId == null || _context.Customer == null)
            {
                return NotFound();
            }

            var assigned = await _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefaultAsync(m => m.IdCustomer == customerId);
            if (assigned == null)
            {
                return NotFound();
            }
            return View(assigned);
        }


        /// <summary>
        /// Leitet zur Index Seite weiter wenn der Delete Knopf bestätigt wurde und löscht die Relation
        /// POST: Assigned/Delete
        /// Nutzer: Admin
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int consultantId, int customerId)
        {
            if (_context.ConsultantToCustomer == null)
            {
                return Problem("Entity set 'MvcAccountContext.ConsultantToCustomer'  is null.");
            }
            var assigned = await _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefaultAsync(m => m.IdCustomer == customerId);
            if (assigned == null)
            {
                return NotFound();
            }
            _context.ConsultantToCustomer.Remove(assigned);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        /// <summary>
        /// Leitet zur selectierten Seite weiter wenn der Delete Knopf bestätigt wurde und löscht die Relation
        /// POST: Assigned/DeleteConfirmedConsultant
        /// Nutzer: Admin, Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        /// <param name="page">Ursprungsseite für redirect</param>
        [HttpPost]
        [Authorize(Roles = "ConsultantRole")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmedConsultant(int consultantId, int customerId, string page)
        {
            if (!ConsultantAndCustomerExists(consultantId, customerId)) return NotFound();
            if (_context.ConsultantToCustomer == null)
            {
                return Problem("Entity set 'MvcAccountContext.ConsultantToCustomer'  is null.");
            }
            var assigned = await _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefaultAsync(m => m.IdCustomer == customerId);
            if (assigned == null)
            {
                return NotFound();
            }
            _context.ConsultantToCustomer.Remove(assigned);
            await _context.SaveChangesAsync();
            var customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);
            var mp = new MessageProcessor() { };
            await mp.AddMessage(_context, consultantId, "Sie wurden als Steuerberater vom Kunden " + customer.Firstname + " " + customer.Lastname + " komplett ausgetragen.");
            return RedirectToAction(page, "consultant");
        }


        /// <summary>
        /// Makiert Anfrage als angefragt in Datenbank
        /// POST: consultant/Request
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        /// <param name="insight">Die zu betrachtende Einsicht: 0 für simple, 1 für erweiterte</param>
        /// <param name="page">Ursprungsseite für redirect</param>
        [HttpPost]
        [Authorize(Roles = "ConsultantRole")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RequestInsight(int consultantId, int customerId, int insight, string page)
        {
            if (!ConsultantAndCustomerExists(consultantId, customerId)) return NotFound();
            ConsultantToCustomer ctc = _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId);
            if (insight == 0) // überprüft ob die simple Einsichtnahme vorliegt
            {
                ctc.InsightStatusSimple = InsightStatus.ApplicationInsight;
            }
            else if (insight == 1) // überprüft ob die erweiterte Einsichtnahme vorliegt
            {
                if (ctc.InsightStatusSimple == InsightStatus.None) ctc.InsightStatusSimple = InsightStatus.ApplicationInsight;
                ctc.InsightStatusExtended = InsightStatus.ApplicationInsight;
            }
            else
            {
                return RedirectToAction(page, "consultant");
            }
            _context.Update(ctc);
            await _context.SaveChangesAsync();
            var customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);
            var mp = new MessageProcessor() { };
            await mp.AddMessage(_context, consultantId, "Sie haben als Steuerberater beim Kunden " + customer.Firstname + " " + customer.Lastname + " die " + (insight == 0 ? "einfache" : "erweiterte") + " Einsicht beantragt.");
            return RedirectToAction(page, "consultant");
        }


        /// <summary>
        /// Leitet zur Seite zurück wenn der Remove Knopf bestätigt wurde und entfernt die Einsicht
        /// POST: Assigned/Remove
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="consultantId">Id des Steuerberaters</param>
        /// <param name="customerId">Id des Kunden</param>
        /// <param name="insight">Die zu betrachtende Einsicht: 0 für simple, 1 für erweiterte</param>
        /// <param name="page">Ursprungsseite für redirect</param>
        [HttpPost]
        [Authorize(Roles = "ConsultantRole")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveInsight(int consultantId, int customerId, int insight, string page)
        {
            if (!ConsultantAndCustomerExists(consultantId, customerId)) return NotFound();
            ConsultantToCustomer ctc = _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId);
            if (insight == 0) // überprüft ob die simple Einsichtnahme vorliegt
            {
                if (ctc.InsightStatusExtended != InsightStatus.None) ctc.InsightStatusExtended = InsightStatus.None;
                ctc.InsightStatusSimple = InsightStatus.None;
            }
            else if (insight == 1) // überprüft ob die erweiterte Einsichtnahme vorliegt
            {
                ctc.InsightStatusExtended = InsightStatus.None;
            }
            else
            {
                return Problem("Art der Einsicht existiert nicht");
            }
            _context.Update(ctc);
            await _context.SaveChangesAsync();
            var customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);
            var mp = new MessageProcessor() { };
            await mp.AddMessage(_context, consultantId, "Sie haben als Steuerberater beim Kunden " + customer.Firstname + " " + customer.Lastname + " die " + (insight == 0 ? "einfache und eventuell auch somit die erweiterte" : "erweiterte") + " Einsicht zurückgezogen.");
            return RedirectToAction(page, "consultant");
        }

        /// <summary>
        /// Get Methode um Kunde-Steuerberater Zuweisungen zu verwalten
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <returns> Returned die entsprechend erstellte View AddCustomerRelation </returns>
        [HttpGet]
        public async Task<IActionResult> AddCustomerRelation()
        {
            string name = HttpContext.User.Identity.Name;
            AddCustomerRelationViewModel relationViewModel = new AddCustomerRelationViewModel();
            //Eingeloggter User übergeben
            relationViewModel._Consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            relationViewModel.customerList = new List<Customer>();

            //Listen für das View Model initialisieren
            relationViewModel.customerList = _context.Customer.ToList();
            relationViewModel.consultantList = _context.Consultant.ToList();

            return View(relationViewModel);
        }

        /// <summary>
        /// Post Methode um Kunde-Steuerberater Zuweisungen zu Erstellen/Löschen. Beinhaltet ebenfalls die dazu passende Validierung der Dateien. 
        /// Es wird also überprüft, ob die eingegeben Datensätze sich zu Kunden/Steuerberater zuordnen lassen. 
        /// Es wird ebenfalls geprüft, ob die Beziehung bereits existiert, vor dem Erstellen bzw. löschen, um so Exceptions abzufangen.
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="customer"> String aus CustomerID + CustomerFirstName + CustomerLastName</param>
        /// <param name="consultant"> String aus consultantID + consultantFirstName + consultantLastName</param>
        /// <param name="buttonInfo"> Entweder Delete oder Create, je nachdem welcher Button in der Get geklickt wurde. Create bedeutet Zuweisung hinzufügen - Delete Zuweisung löschen</param>
        /// <returns> die eigenen Kunden View</returns>
        [HttpPost]
        public async Task<IActionResult> AddCustomerRelation(string customer, string consultant, string buttonInfo)
        {
            // Init Variablen
            int customerId = 0;
            int consultantId = 0;
            int customerIdTest = 0;
            int consultantIdTest = 0;
            // Init Customer, Consultant, AssignedConsultant Liste aus der Datenbank
            List<Customer> customerListTest = _context.Customer.ToList();
            List<Consultant> consultantListTest = _context.Consultant.ToList();
            List<ConsultantToCustomer> RelationList = _context.ConsultantToCustomer.ToList();
            //Ids aus Inputs auslesen und prüfen ob KundeId/BeraterId überhaupt existiert
            if (!string.IsNullOrEmpty(customer))
            {
                string[] customerParts = customer.Split(' ');
                //prüfen ob eine Integer als ID übergeben wurde
                try
                {
                    customerIdTest = int.Parse(customerParts[0]);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message + "Invalid String Input for Id Field");
                    return RedirectToAction("OwnCustomers", "Consultant");
                }
                if (customerListTest.Any(c => c.Id == customerIdTest))
                {
                    customerId = int.Parse(customerParts[0]);
                }
            }
            if (!string.IsNullOrEmpty(consultant))
            {
                string[] consultantParts = consultant.Split(' ');
                //prüfen ob eine Integer als ID übergeben 
                try
                {
                    consultantIdTest = int.Parse(consultantParts[0]);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message + "Invalid String Input for Id Field");
                    return RedirectToAction("OwnCustomers", "Consultant");
                }
                if (consultantListTest.Any(c => c.Id == consultantIdTest))
                {
                    consultantId = int.Parse(consultantParts[0]);
                }
            }
            //Zuweisung erstellen
            if (buttonInfo == "Create" && customerId != 0 && consultantId != 0)
            {
                ConsultantToCustomer relation = new ConsultantToCustomer();
                relation.IdCustomer = customerId;
                relation.IdConsultant = consultantId;
                relation.InsightStatusSimple = InsightStatus.None;
                relation.InsightStatusExtended = InsightStatus.None;
                Console.WriteLine("Test" + relation.IdCustomer + relation.InsightStatusSimple);
                bool RelationExists = false;
                foreach (ConsultantToCustomer relationToCheck in RelationList)
                {
                    if (relationToCheck.IdCustomer == customerId && relationToCheck.IdConsultant == consultantId)
                    {
                        RelationExists = true;
                    }
                }
                if (!RelationExists)
                {
                    _context.Add(relation);
                    await _context.SaveChangesAsync();
                    var customer1 = _context.Customer.FirstOrDefault(x => x.Id == customerId);
                    var mp = new MessageProcessor() { };
                    await mp.AddMessage(_context, consultantId, "Sie wurden dem Kunden " + customer1.Firstname + " " + customer1.Lastname + " zugeteilt");
                }
                return RedirectToAction("OwnCustomers", "Consultant");
            }
            //Zuweisung entfernen
            else if (buttonInfo == "Delete" && customerId != 0 && consultantId != 0)
            {
                ConsultantToCustomer relation = new ConsultantToCustomer();
                relation.IdCustomer = customerId;
                relation.IdConsultant = consultantId;
                foreach (ConsultantToCustomer relationToCheck in RelationList)
                {
                    if (relationToCheck.IdCustomer == customerId && relationToCheck.IdConsultant == consultantId)
                    {
                        _context.Remove(relationToCheck);
                        await _context.SaveChangesAsync();
                    }
                }
                var customer2 = _context.Customer.FirstOrDefault(x => x.Id == customerId);
                var mp = new MessageProcessor() { };
                await mp.AddMessage(_context, consultantId, "Ihre Einsicht beim Kunden " + customer2.Firstname + " " + customer2.Lastname + " wurde entfernt");
            }

            return RedirectToAction("OwnCustomers", "Consultant");
        }


        /// <summary>
        /// Überprüft ob die Ids generell existieren
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>InsightStatus</returns>
        private bool ConsultantAndCustomerExistsGeneral(int? consultantId, int? customerId)
        {
            return _context.Customer.Any(e => e.Id == customerId) && _context.Consultant.Any(e => e.Id == consultantId);
        }
        /// <summary>
        /// Überprüft ob die Ids mit einer Relation in ConsultantToCustomer existieren
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>InsightStatus</returns>
        private bool ConsultantAndCustomerExists(int? consultantId, int? customerId)
        {
            return _context.ConsultantToCustomer.Any(e => e.IdCustomer == customerId && e.IdConsultant == consultantId);

        }
    }
}
