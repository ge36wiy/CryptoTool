
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.Utilities;
using Projekt.ViewModels;

using Projekt.Container;
using System.Xml.Linq;
using System.Diagnostics;

using System.IO;


namespace Projekt.Controllers
{

    /// <summary>
    /// Handle Usereingaben des Customerusers
    /// Nutzer: Customer
    /// CodeOwner: siehe Methoden
    /// </summary>
    [Authorize(Roles = "CustomerRole")]
    public class CustomerController : Controller
    {
        private readonly MvcAccountContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        TransactionContainer cont;

        /// <summary>
        /// Konstruktor für den Controller
        /// Nutzer: Customer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public CustomerController(MvcAccountContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
            cont = new TransactionContainer(context);
        }

        /// <summary>
        /// erstellt AssetOverview View
        /// CodeOwner: Sebastian Klee
        /// </summary>
        /// <returns> AssetOverview View, mit Assets des angemeldeten Kunden </returns>
        public IActionResult AssetOverview()
        {
            string name = HttpContext.User.Identity.Name;
            AssetOverviewViewModel data = new AssetOverviewViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.AssetList = cont.CreateAssets(data._Customer.Id);
            return View(data);
        }

        /// <summary>
        /// erstellt AccessGrantedOverview View
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        /// <returns> AccessGrantedOverview View, die alle dem Kunden zugeordneten Steuerberater und ihre Einsichtberechtigungen anzeigt </returns>
        public async Task<IActionResult> AccessGrantedOverview()
        {
            string name = HttpContext.User.Identity.Name;
            List<AssignedConsultant> allConsultants = new List<AssignedConsultant>();
            int selfId = (await _context.Customer.FirstOrDefaultAsync(x => x.Username.ToLower() == name)).Id;
            var query = from con in _context.Consultant
                        join rel in _context.ConsultantToCustomer
                        on con.Id equals rel.IdConsultant
                        where rel.IdCustomer == selfId
                        select new { rel, con };
            foreach (var item in query)
            {
                allConsultants.Add(new AssignedConsultant(item.con, item.rel.InsightStatusSimple, item.rel.InsightStatusExtended));
            }
            AccessViewModel data = new();
            data.assigned = allConsultants;
            data._Customer = await _context.Customer.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            return View(data);
        }
        /// <summary>
        /// Passt nach Beantwortung einer Einsichtanfrage den Einsichtstatus an
        /// CodeOwner: Christopher Glöckel
        /// </summary>
        /// <param name="consultantID">Im Button spezifiezierte ID des betroffenen Steuerberaters</param>
        /// <param name="isExtended">Spezifiziert auf welche Einsicht zugegriffen wird</param>
        /// <param name="insightStatus">Neuer gewünschter Zustand des Einsichtstatus</param>
        /// <returns> Redirect zu AccessGrantedOverview </returns>
        public async Task<IActionResult> AccessChange(int consultantID, bool isExtended, InsightStatus insightStatus)
        {
            string name = HttpContext.User.Identity.Name;
            int selfId = (await _context.Customer.FirstOrDefaultAsync(x => x.Username.ToLower() == name)).Id;
            var rel = await _context.ConsultantToCustomer.FirstOrDefaultAsync(r => r.IdConsultant == consultantID && r.IdCustomer == selfId);
            if (isExtended)
            {
                rel.InsightStatusExtended = insightStatus;
                if (insightStatus == InsightStatus.Insight)
                {
                    rel.InsightStatusSimple = InsightStatus.Insight;
                }
            }
            else
            {
                rel.InsightStatusSimple = insightStatus;
                if (insightStatus == InsightStatus.None)
                {
                    rel.InsightStatusExtended = InsightStatus.None;
                }
            }
            _context.Update(rel);
            await _context.SaveChangesAsync();
            //von Alexander Rioux ergänzt-----------
            var customer = _context.Customer.FirstOrDefault(x => x.Id == selfId);
            var mp = new MessageProcessor() { };
            await mp.AddMessage(_context, consultantID, "Die " + (isExtended ? "erweiterte " : "einfache ") + "Einsichtnahme bei " + customer.Firstname + " " + customer.Lastname + " wurde " + (insightStatus == InsightStatus.Insight ? "genehmigt." : "wiederrufen, oder abgelehnt."));
            //--------------------------------------
            return RedirectToAction("AccessGrantedOverview");
        }


        /// <summary>
        /// Zeigt die Profilseite des jeweiligen Kunden an
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <returns> Profile View</returns>
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            BasicViewModel data = await getCurrentCustomer();
            return View(data);
        }


        /// <summary>
        /// Validiert und setzt ggf. die Profildaten auf neue Werte. 
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="form_data">Die vom User übergebene neuen Profildaten </param>
        /// <returns> Profil View mit neuen Werten falls die Validierung erfolgreich war, sonst alte Werte</returns>
        [HttpPost]
        public async Task<IActionResult> Profile(BasicViewModel form_data)
        {
            if (ModelState.IsValid)
            {
                _context.updateCustomerData(form_data._Customer);
            }
            else
            {
                // Um in der Navbar den korrekten Namen anzuzeigen.
                string current_username = User.Identity.Name;
                Customer? correct_user = await _context.Customer.FirstOrDefaultAsync(u => u.Username == current_username);
                ViewData["error-case-name"] = correct_user.Firstname;
                ViewData["error-case-surname"] = correct_user.Lastname;
                ViewData["error-case-pathToProfileImg"] = correct_user.Pathtoimage;
                return View(form_data);
            }
            return RedirectToAction("Profile");
        }


        /// <summary>
        /// Aktualisiert das Profilbild des Nutzers
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="imageFile">Die vom User übergebene Bilddatei</param>
        /// <returns> Profil View mit neuem Profilbild falls die Validierung erfolgreich war, sonst eine Fehlermeldung</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadProfileImg(IFormFile imageFile)
        {
            if (imageFile != null && isValidImage(imageFile))
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "img/profile_img");
                string uniqueFileName = Guid.NewGuid().ToString() + "_" + imageFile.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);

                string relativeFilePath = "~" + filePath.Replace(_webHostEnvironment.WebRootPath, string.Empty)
                                                      .Replace("\\", "/");
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    imageFile.CopyTo(stream);
                    string current_username = User.Identity.Name;
                    Customer? current_user = await _context.Customer.FirstOrDefaultAsync(u => u.Username == current_username);
                    if (current_user != null)
                    {
                        String PathToPrevImage = current_user.Pathtoimage;
                        current_user.Pathtoimage = relativeFilePath;
                        _context.Update(current_user);
                        _context.SaveChanges();
                        //Delete prev profile img
                        DeletePrevProfileImg(PathToPrevImage);
                    }
                }
            }

            BasicViewModel data = await getCurrentCustomer();
            return View("Profile", data);

        }

        /// <summary>
        /// Hilfsmethode: Löscht das aktuelle Profilbild des Nutzers. 
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="PathToPrevImage">Pfad zum aktuellen Profilbild des Nutzers</param>
        /// <returns></returns>
        private void DeletePrevProfileImg(string PathToPrevImage)
        {
            string fileName = Path.GetFileName(PathToPrevImage);
            string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "img/profile_img", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }


        /// <summary>
        /// Hilfsmethode: Überprüft ob die vom User übergebene Datei tatsächlich eine Bilddatei ist, ob diese eine gültige Extension und Größe(5 MB) hat.
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="ImageFile">Die vom User übergebene Datei</param>
        /// <returns>Boolean</returns>
        private bool isValidImage(IFormFile ImageFile)
        {
            long fileSizeLimit = 5 * 1024 * 1024;
            string[] allowedExtensions = { ".jpg", ".jpeg", ".png" };
            string fileExtension = Path.GetExtension(ImageFile.FileName).ToLowerInvariant();
            if (!allowedExtensions.Contains(fileExtension))
            {
                ViewData["ErrorMessage"] = "Falsche Extension";
                return false;

            }
            else if (ImageFile.Length > fileSizeLimit)
            {
                ViewData["ErrorMessage"] = "Die Datei ist zu groß";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hilfsmethode: Lädt das aktuelle Kundenobjekt aus der Datenbank und erstellt eine BasicViewModel
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param></param>
        /// <returns>BasicViewModel für die Navbar, die auf den aktuellen Kunden referenziert.</returns>
        private async Task<BasicViewModel> getCurrentCustomer()
        {
            string name = HttpContext.User.Identity.Name;
            BasicViewModel data = new BasicViewModel();
            data._Customer = await _context.Customer.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            return data;
        }


        /// <summary>
        /// Zeigt die View zur Jahrauswahl für den Steuerbericht an.
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param></param>
        /// <returns>TaxReportYear View</returns>
        [HttpGet]
        public async Task<IActionResult> TaxReport()
        {
            BasicViewModel data = await getCurrentCustomer();
            return View("TaxReportYear", data);
        }


        /// <summary>
        /// Erstellt das TaxReport-Objekt und schickt es in die TaxReportView
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="year">Das für den Steuerbericht relevante Jahr</param>
        /// <returns>TaxReportView</returns>
        [HttpPost]
        public async Task<IActionResult> TaxReport(int year)
        {
            /*WICHTIG

            Eingeloggter User für Navbar Layout eingeloggte User mitgegeben werden!!!!!
            Bei Fragen an Thomas wenden

            WICHTIG*/

            if (year < 0)
            {
                return RedirectToAction("TaxReport");
            }

            string name = HttpContext.User.Identity.Name;
            Customer current_customer = await _context.Customer.FirstOrDefaultAsync(x => x.Username.ToLower() == name);



            List<Transaction> transactions = cont.GetAllTransactionsCustomer(current_customer.Id);
            TaxReportViewModel taxReportViewModel = new TaxReportViewModel();
            taxReportViewModel._Customer = current_customer;
            try
            {
                TaxReport taxReport = new TaxReport(year, transactions);
                taxReportViewModel._taxReport = taxReport;

            }
            catch (GhostCoinException ex)
            {
                ViewData["Error-msg"] = "Fehler: Sie haben mehr " + ex.ghostCoin + " verkauft als gekauft.";
                return View(taxReportViewModel);
            }


            return View(taxReportViewModel);
        }


        /// <summary>
        /// Zeigt die Details eines bestimmten Customer Users an
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den Jeweiligen User von dem die Details angezeigt werden sollen</param>
        /// <returns> Details view des spezifizierten Users</returns>

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Customer == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }


        /// <summary>
        /// Get Methode um einen CustomerUser zu erstellen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> View zum erstellen eines Customers</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post Methode um einen Customer User zu erstellen, Hasht das Passwort mit SHA256, ändert den eingegeben Usernamen in Kleinbuchstaben(um Groß-leinschreibung beim Usernamen irrelevant zu machen) und legt die Daten in der customerTabelle ab. Außerdem wird der Pfad für das Standarduserbild festgelegt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="customer"> es wird ein Customerobjekt eingegeben mit Username und Passwort und allen weiteren Attributen das vom User auf der Create Seite eingegeben wurde</param>
        /// <returns> Entweder die Create seite bei einem Fehler bei der Validierung oder Die Admin Startseite</returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Firstname,Lastname,Username,Password,Address,Email,Pathtoimage")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                //Standardbild wird hinterlegt
                customer.Pathtoimage = "~/img/profile_img/customer_profile_img.png";
                //Passwort wird mit sha 256 gehasht
                customer.Password = (string)PasswordHash.CreateHash(customer.Password);
                //username wird in kleinbuchstaben umgewandelt, um die groß kleinschreibung im username hinfällig zu machen
                customer.Username = customer.Username.ToLower();
                _context.Add(customer);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Account");
            }
            return View(customer);
        }

        /// <summary>
        /// Get Methode zum bearbeiten eines Customer Users
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID zum spezifizieren welcher User bearbeitet werden Soll</param>
        /// <returns> Edit Page des spezifizierten Users</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Customer == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        /// <summary>
        /// Post Methode um die bearbeiteten Daten in der Datenbank zu speichern. Falls sich das Passwort geändert hat, wird es neu gehasht und das Objekt wird in der Datenbank gespeichert
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID des zu bearbeitenden Customer Objekts</param>
        /// <param name="customer"> es wird ein Customerobjekt eingegeben mit allen Attributen das vom User auf der Bearbeiten Seite eingegeben wurde</param>
        /// <returns> Bei fehlgeshclagener Validierung GET: Edit bei erfolgreicher Validierung die Admin Startseite</returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Firstname,Lastname,Username,Password,Address,Email,Pathtoimage")] Customer customer)
        {
            if (id != customer.Id)
            {
                return NotFound();
            }
            customer.Username = customer.Username.ToLower();
            Customer knd = await _context.Customer.FirstOrDefaultAsync(x => x.Id == customer.Id);
            if (customer.Password != knd.Password)
            {
                customer.Password = PasswordHash.CreateHash(customer.Password);
            }
            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(customer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Account");
            }
            return View(customer);
        }

        /// <summary>
        /// Get Methode für das Löschen eines Customerusers
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den zu löschenden Customeruser</param>
        /// <returns> Löschview des jeweiligen Users</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Customer == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        /// <summary>
        /// PostMethode zum Löschen eines Users nach Bestätigung durch den Benutzer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert den zu Löschenden User</param>
        /// <returns> Startseite des eingeloggten Users</returns>
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Customer == null)
            {
                return Problem("Entity set 'MvcAccountContext.Customer'  is null.");
            }
            var customer = await _context.Customer.FindAsync(id);
            if (customer != null)
            {
                _context.Customer.Remove(customer);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Account");
        }

        private bool CustomerExists(int id)
        {
            return _context.Customer.Any(e => e.Id == id);
        }
    }
}
