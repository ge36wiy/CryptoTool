using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Projekt.ViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Projekt.Utilities;


namespace Projekt.Controllers
{

    /// <summary>
    /// Controller zum Verwalten der Authentisierung und AUthentifizierung
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private readonly MvcAccountContext _context;

        /// <summary>
        /// Kosntruktor für den Controller
        /// CodeOwner: Thomas Gerum
        /// </summary>
        public AccountController(MvcAccountContext context)
        {
            _context = context;

        }



        /// <summary>
        /// Weiterleitung and die Admin Übersicht über alle User
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Controller Action von Registered Accounts</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            return RedirectToAction("RegisteredAccounts", "Admin");
        }




        /// <summary>
        /// Leitet den eventuell schon eingeloggten User auf die jeweiligen Startseite des Users weiter
        /// Falls der User noch nicht eingeloggt ist wird er auf die Login seite geleitet
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Controller Action der jeweiligen Startseite des Users</returns>
        [AllowAnonymous]
        public IActionResult Login()
        {
            ClaimsPrincipal claimUser = HttpContext.User;
            if (claimUser.Identity.IsAuthenticated)
            {
                //Falls User eingeloggt ist wird er auf seine Startseite redirected
                var roles = claimUser.Identities.SelectMany(i =>
                {
                    return i.Claims
                        .Where(c => c.Type == i.RoleClaimType)
                        .Select(c => c.Value)
                        .ToList();
                });
                if (roles.Contains("Admin"))
                {
                    return RedirectToAction("Index", "Account");
                }
                else if (roles.Contains("CustomerRole"))
                {
                    return RedirectToAction("AssetOverview", "Customer");
                }
                else if (roles.Contains("ConsultantRole"))
                {
                    return RedirectToAction("OwnCustomers", "Consultant");
                }
            }
            //Falls er nicht eingeloggt ist, wird er auf login screen redirectted
            return View();

        }

        /// <summary>
        /// Überbrüft die eingegeben LOgin daten des Users mit den unterschiedlichen Userdatenbanken. Wird ein Match gefunden, wird der User mit seinen jeweilig benötigten Rollen eingeloggt und auf seine Startseite weitergeleitet
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="account"> es wird ein Accountobjekt eingegeben mit Username und Passwort das vom User auf der Login Seite eingegeben wurde</param>
        /// <returns> Startseite des jeweil eingeloggten Users</returns>

        [AllowAnonymous]
        [HttpPost, ActionName("Login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Id,Username,Password")] Account account)
        {
            account.Username = account.Username.ToLower();

            //Uservalidierung in der Account Datenbank
            bool IsValid = _context.Customer.Any(x => x.Username.ToLower() == account.Username && x.Password == PasswordHash.CreateHash(account.Password));
            if (IsValid)
            {
                //Claim Management
                ClaimManagement mgmt = new ClaimManagement(account, "CustomerRole");
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(mgmt.identity),
                    mgmt.props);
                //weiterleitung auf startseite
                return RedirectToAction("AssetOverview", "Customer");
            }
            IsValid = _context.Consultant.Any(x => x.Username.ToLower() == account.Username && x.Password == PasswordHash.CreateHash(account.Password));
            if (IsValid)
            {
                //Claim Management
                ClaimManagement mgmt = new ClaimManagement(account, "ConsultantRole");
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(mgmt.identity),
                    mgmt.props);
                //weiterleitung auf startseite
                return RedirectToAction("OwnCustomers", "Consultant");
            }
            IsValid = _context.Admin.Any(x => x.Username.ToLower() == account.Username && x.Password == PasswordHash.CreateHash(account.Password));
            if (IsValid)
            {
                //Claim Management
                ClaimManagement mgmt = new ClaimManagement(account, "Admin", "ConsultantRole", "CustomerRole");
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(mgmt.identity),
                    mgmt.props);
                //weiterleitung auf startseite
                return RedirectToAction("Index", "Account");
            }
            if (!IsValid)
            {
                //falls nicht richtige Zugangsdaten
                ModelState.AddModelError("", "Ungültige Zugangsdaten");
            }
            return View();
        }

        /// <summary>
        /// Logt den akteull eingeloggten User aus
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Startseite bzw. Login Seite</returns>
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Falls die Seite nicht angezeigt werden darf, wird man auf diese Access Denied Ansicht weitergeleitet
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Access Denied View</returns>
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();

        }

        private bool AccountExists(int id)
        {
            return (_context.Account?.Any(e => e.Id == id)).GetValueOrDefault();
        }

    }
}
