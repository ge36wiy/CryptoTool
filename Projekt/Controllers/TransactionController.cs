using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.ViewModels;
using Projekt.Container;
using Projekt.Utilities;

namespace Projekt.Controllers
{

    /// <summary>
    /// Controller zum Verwalten von Transaktionskationen
    /// CodeOwner: Thomas Gerum
    /// </summary>
    [Authorize(Roles = "CustomerRole")]
    public class TransactionController : Controller
    {
        private readonly MvcAccountContext _context;
        TransactionContainer cont;


        /// <summary>
        /// Konstruktor zum erstellen des Controllers und anlegen des Transactionscontainers für die Datenbankoperationen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="context"> Datenbankkontext</param>
        public TransactionController(MvcAccountContext context)
        {
            _context = context;
            cont = new TransactionContainer(context);
        }

        /// <summary>
        /// AdminOverview über alle existierenden Transaktionen, INfos werden an das Viewmodel übergeben, sowie der aktuell eingeloggte Benutzer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> Übersicht über alle User</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            TransactionViewModel data = new TransactionViewModel();
            data.Transactionlist = cont.GetAllTransactions();
            return View(data);
        }

        /// <summary>
        /// Übersicht über alle Transaktionen des aktuell eingeloggten Users. Daten werden in das Viewmodel übergeben und mit der COntainermethode befüllt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns>View mit den Daten für den jeweiligen User</returns>
        public IActionResult TransactionsOfCurrentUser()
        {

            //Operation um Eigene Transaktionen anzuzeigen
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transactionlist = cont.GetAllTransactionsCustomer(data._Customer.Id);
            return View(data);
        }

        /// <summary>
        /// Get Methode für die Details der spezifizierten Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert die zu bearbeitende Transaktion</param>
        /// <returns> Daten und View zu bearbeitender Transaktion</returns>
        public IActionResult Details(int id)
        {
            var transaction = cont.FindTransaction(id);

            //Übergeben der Transaktion und des aktuellen Users an das Viewmodel
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transaction = transaction;
            if (transaction.OwnerId != data._Customer.Id)
            {
                return RedirectToAction("TransactionsOfCurrentUser", "Transaction");
            }
            return View(data);

        }

        /// <summary>
        /// Get Methode für das Erstellen einer Transaktion. Es wird der aktuell eingeloggte User übergeben, sowie ein Dicitionary an Coinnamen und Ids und eine Liste mit Coinnamen für den Autocomplete per Javascript in der Create Transaction View
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> View zum erstellen einer Transaktion</returns>
        public IActionResult Create()
        {
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            CoinGecko cg = new CoinGecko();
            data.Coinlist = cg.getAllCoinsList();
            data.isBackup = false;
            try
            {
                if (data.Coinlist.Contains("backup"))
                    data.isBackup = true;
                data.Coinlist.Remove("backup");
            }
            catch (Exception)
            {

            }
            data.Coindict = cg.getAllCoins();
            data.isValid = true;
            return View(data);
        }

        /// <summary>
        /// PostMethode zum erstellen einer Transaktion. Alle eingegebenen Daten werden in ein Transaction Objekt gebindet. nicht eingegebene Daten werden automatisch befüllt und anschließend in der Transaktionstabelle abgelegt. Bei Misserfolg der Validierung wird die Get Methode für Create ausgeführt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="transaction"> Transaktionsobjekt aus den Daten die der User in der CreateTransaction Form eingegeben hat</param>
        /// <returns> Weiterleitung an Übersicht über alle Transaktionen eines Users oder die Get Methode für Create bei fehlgeschlagener Validierung</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Coin,Type,Fee,Rate,Amount,Value,Remainder,OwnerId,PurchaseDate,Tradeable,Deleted")] Transaction transaction)
        {
            string name = HttpContext.User.Identity.Name;
            transaction.OwnerId = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name).Id;
            bool valid = cont.IsValidTransaction(transaction);
            if (ModelState.IsValid && valid)
            {
                //Anlegen eine Transaktion und befüllen der Standardfelder
                transaction.Remainder = transaction.Amount;
                transaction.Tradeable = transaction.PurchaseDate.AddYears(1);
                transaction.Deleted = false;
                cont.CreateTransaction(transaction);
                return RedirectToAction("TransactionsOfCurrentUser", "Transaction");
            }

            TransactionViewModel data = new TransactionViewModel();
            CoinGecko cg = new CoinGecko();
            data.Coinlist = cg.getAllCoinsList();
            data.Coindict = cg.getAllCoins();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transaction = transaction;
            data.isValid = valid;
            return View(data);
        }

        /// <summary>
        /// Get Methode zum bearbeiten einer Transaktion, Analog zur Get Methode von Create
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert die zu bearbeitende Transaktion</param>
        /// <returns> View zum Bearbeiten der spezifizierten Transaktion</returns>

        public IActionResult Edit(int id)
        {
            //Containersucher nach Transaktion
            var transaction = cont.FindTransaction(id);
            if (transaction == null)
            {
                return NotFound();
            }
            //Übergeben der Transaktion und des aktuellen Users an das Viewmodel
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transaction = transaction;
            //für Modalpopup ein javascript datum zum überprüfen ob Transaktion im letzten Jahr liegt
            data.JavaScriptDate = transaction.PurchaseDate.ToString("yyyy-MM-ddTHH:mm:ss");
            //Übergeben Der liste an ID und coins für das automatische holen der Umrechnungsrate
            CoinGecko cg = new CoinGecko();
            data.Coinlist = cg.getAllCoinsList();
            data.Coindict = cg.getAllCoins();
            data.isValid = true;
            //Falls keine Transaktion des akutellen Users nicht zulassen sondern redirect
            if (transaction.OwnerId != data._Customer.Id)
            {
                return RedirectToAction("TransactionsOfCurrentUser", "Transaction");
            }
            return View(data);
        }

        /// <summary>
        /// PostMethode zum Bearbeiten einer Transaktion. Alle eingegebenen Daten werden in ein Transaction Objekt gebindet. nicht eingegebene Daten werden automatisch befüllt und anschließend in der Transaktionstabelle abgelegt. Bei Misserfolg der Validierung wird die Get Methode für Edit ausgeführt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID des zu bearbeitenden Transaktionsobjekts</param>
        /// <param name="transactionview"> Transactionviewmodel in dem ein Transaction Objekt liegt. Dieses wird in der Methode extrahiert</param>
        /// <returns> Weiterleitung an Übersicht über alle Transaktionen eines Users oder die Get Methode für Edit bei fehlgeschlagener Validierung</returns>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Transaction")] TransactionViewModel transactionview)
        {
            //Transaktion aus Viemodel entnehmen
            Transaction transaction = transactionview.Transaction;
            bool valid = cont.IsValidTransaction(transaction);
            if (id != transaction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid && valid)
            {
                try
                {
                    //Aktualsieren der Automatischen Felder:
                    //transaction.Value = transaction.Rate * transaction.Amount;
                    transaction.Remainder = transaction.Amount;
                    transaction.Tradeable = transaction.PurchaseDate.AddYears(1);
                    //Containeroperationen
                    cont.EditTransaction(transaction);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransactionExists(transaction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                //Zurück auf Übersicht nach Erfolg
                return RedirectToAction(nameof(TransactionsOfCurrentUser));
            }
            // Im fehlerfall der eingabe auf gleiche View nochmal
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            CoinGecko cg = new CoinGecko();
            data.Coinlist = cg.getAllCoinsList();
            data.Coindict = cg.getAllCoins();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transaction = transaction;
            data.isValid = false;

            return View(data);
        }

        /// <summary>
        /// Get Methode zum Löschen einer Transaktion
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Id zum spezifizieren der Transaktion zum Löschen</param>
        /// <returns> View zum Löschen einer Transaktion</returns>
        public IActionResult Delete(int id)
        {
            var transaction = cont.FindTransaction(id);
            bool valid = cont.IsValidTransactionDelete(transaction);
            //Übergeben der Transaktion und des aktuellen Users an das Viewmodel
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            data.Transaction = transaction;
            data.isValid = valid;
            //Pass Javascript String für überprüfung im letzten Jahr
            data.JavaScriptDate = transaction.PurchaseDate.ToString("yyyy-MM-ddTHH:mm:ss");
            if (transaction.OwnerId != data._Customer.Id)
            {
                return RedirectToAction("TransactionsOfCurrentUser", "Transaction");
            }
            return View(data);
        }

        /// <summary>
        /// PostMethode zum Löschen einer Transkation nach Bestätigung durch den Benutzer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert die zu löschende Transaktion</param>
        /// <returns> Übersicht über alle Transaktionen des Users</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {

            var transaction = cont.FindTransaction(id);
            if (transaction != null)
            {
                cont.DeleteTransaction(transaction);
            }

            return RedirectToAction("TransactionsOfCurrentUser", "Transaction");
        }


        /// <summary>
        /// Get Methode zum Löschen einer Transaktion durch den Administrator
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Id zum spezifizieren der Transaktion zum Löschen</param>
        /// <returns> View zum Löschen einer Transaktion</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteAdmin(int id)
        {
            var transaction = cont.FindTransaction(id);

            //Übergeben der Transaktion und des aktuellen Users an das Viewmodel
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data.Transaction = transaction;

            return View(data);
        }


        /// <summary>
        /// PostMethode zum Löschen einer Transkation nach Bestätigung durch den Administrator
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Id zum spezifizieren der Transaktion zum Löschen</param>
        /// <returns> View zum Löschen einer Transaktion</returns>
        [HttpPost, ActionName("DeleteAdmin")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteAdminConfirmed(int id)
        {

            var transaction = cont.FindTransaction(id);
            if (transaction != null)
            {
                _context.Transaction.Remove(transaction);
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Transaction");
        }



        private bool TransactionExists(int id)
        {
            return _context.Transaction.Any(e => e.Id == id);
        }

        /// <summary>
        /// Ermoeglicht den Import von Transaktionen über ein Json File
        /// CodeOwner: Johannes Bulun
        /// </summary>
        /// <param name="file"> Inputfile mit Transaktionselementen und ihren zugehörigen Parametern.</param>
        [HttpPost]
        public IActionResult ImportFile(IFormFile file)
        {
            // User Id vom aktuell eingeloggten User raussuchen, um die Transaktionsverknüpfung zum User zu Gewaehrleisten
            string name = HttpContext.User.Identity.Name;
            TransactionViewModel data = new TransactionViewModel();
            data._Customer = _context.Customer.FirstOrDefault(x => x.Username.ToLower() == name);
            int userId = data._Customer.Id;

            JsonImport jsonFile = new JsonImport();
            List<Transaction> transactionList = jsonFile.parseData(file);
            try
            {
                foreach (Transaction transaction in transactionList)
                {
                    transaction.OwnerId = userId;
                    _context.Transaction.Add(transaction);
                    _context.SaveChanges();
                }
                return RedirectToAction("TransactionsOfCurrentUser");
            }
            catch (NullReferenceException)
            {
                return View("TransactionsOfCurrentUser");
            }
            catch (Exception)
            {
                Console.WriteLine("Unknown Exception was thrown");
                return View("Error");
            }
        }


    }
}
