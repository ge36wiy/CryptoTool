﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;

namespace Projekt.Controllers;

/// <summary>
/// Controller zur Verwaltung der Startseite und für alle User zugänglichen Seiten
/// CodeOwner: Thomas Gerum
/// </summary>
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    /// <summary>
    /// Konstruktor für den HomeController
    /// CodeOwner: Thomas Gerum
    /// </summary>
    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// Startseite der Anwendung
    /// CodeOwner: Thomas Gerum
    /// </summary>
    /// <returns> Weiterleitung zur Login Methode im Account Controller</returns>
    public IActionResult Index()
    {
        return RedirectToAction("Login", "Account");
    }


    /// <summary>
    /// gibt ErrorView zurück
    /// CodeOwner: automatisch erstellt
    /// </summary>
    /// <returns> Weiterleitung zur ErrorView</returns>
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
