using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projekt.Models;
using Microsoft.AspNetCore.Authorization;
using Projekt.Utilities;
using Projekt.ViewModels;
using Projekt.Container;
using Microsoft.AspNetCore.Hosting;

namespace Projekt.Controllers
{
    /// <summary>
    /// Die Klasse <c>ConsultantController</c> regelt die Backend Aufgaben des Steuerberaters 
    /// Nutzer: Steuerberater
    /// CodeOwner: Thomas Gerum, Alexander Rioux, Christopher Glöckel
    /// </summary>
    [Authorize(Roles = "ConsultantRole")]
    public class ConsultantController : Controller
    {
        private readonly MvcAccountContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;
        TransactionContainer cont;
        /// <summary>
        /// Konstruktor
        /// </summary>
        public ConsultantController(MvcAccountContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            cont = new TransactionContainer(context);
            _webHostEnvironment = webHostEnvironment;
        }
        /// <summary>
        /// Initalisiert das <c>OwnCustomerViewModel</c> welches für die <c>OwnCustomers</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux, Christopher Glöckel
        /// </summary>
        /// <returns> OwnCustomers View </returns>
        public async Task<IActionResult> OwnCustomers()
        {
            string name = HttpContext.User.Identity.Name;
            OwnCustomersViewModel ownCustomersViewModel = new OwnCustomersViewModel();
            ownCustomersViewModel._Consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            ownCustomersViewModel.CustomerList = new List<CustomerWithInsightstatus>();
            List<ConsultantToCustomer> consultantToCustomer = _context.ConsultantToCustomer.Where(m => m.IdConsultant == ownCustomersViewModel._Consultant.Id).ToList();
            foreach (var selected in consultantToCustomer)
            {
                ownCustomersViewModel.CustomerList.Add(new CustomerWithInsightstatus()
                {
                    customer = await _context.Customer.FirstOrDefaultAsync(x => x.Id == selected.IdCustomer),
                    insightSimple = getInsight(ownCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightExtended = getExtendedInsight(ownCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightSimpleString = getInsightString(ownCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightExtendedString = getExtendedInsightString(ownCustomersViewModel._Consultant.Id, selected.IdCustomer)
                });
            }
            return View(ownCustomersViewModel);
        }


        /// <summary>
        /// Initalisiert das <c>AllCustomersViewModel</c> welches für die <c>AllCustomers</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <returns> AllCustomers View </returns>
        public async Task<IActionResult> AllCustomers()
        {
            int i = 0;
            string name = HttpContext.User.Identity.Name;
            AllCustomersViewModel allCustomersViewModel = new AllCustomersViewModel();
            allCustomersViewModel._Consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            if (allCustomersViewModel._Consultant == null)
            {
                return NotFound();
            }
            // Initialisierung
            allCustomersViewModel.ConsultantList = _context.Consultant.ToList();
            List<ConsultantToCustomer> consultantToCustomer = _context.ConsultantToCustomer.ToList();
            List<Customer> foundCustomer = new List<Customer>();
            allCustomersViewModel.MyCustomerList = new List<CustomerWithInsightstatus>() { };
            allCustomersViewModel.NoConsultant = new List<Customer>() { };
            allCustomersViewModel.ConsultantWithCustomerLists = new List<ConsultantWithCustomerList>() { };
            //Geht alle Customer durch um deren Kunden zu finden 
            foreach (var consultant in allCustomersViewModel.ConsultantList)
            {
                // Temporäre Objekte zur ermittlung der einzelnen Kunden
                IEnumerable<ConsultantToCustomer> temporaryConsultantToCustomer = consultantToCustomer.Where(m => m.IdConsultant == consultant.Id);
                List<Customer> temporaryCustomerList = new List<Customer>();
                ConsultantWithCustomerList temporaryConsultantWithCustomerList = new ConsultantWithCustomerList();
                temporaryConsultantWithCustomerList.Consultant = consultant;
                // Pro Steuerberater eigene Kunden finden
                foreach (var selected in temporaryConsultantToCustomer)
                {
                    temporaryCustomerList.Add(await _context.Customer.FirstOrDefaultAsync(x => x.Id == selected.IdCustomer));
                    foundCustomer.Add(await _context.Customer.FirstOrDefaultAsync(x => x.Id == selected.IdCustomer));
                }
                // Hinzufügen ins Model
                temporaryConsultantWithCustomerList.Customers = temporaryCustomerList;
                allCustomersViewModel.ConsultantWithCustomerLists.Add(temporaryConsultantWithCustomerList);
                i++;

            }
            //eigene Kunden ermitteln
            IEnumerable<ConsultantToCustomer> temporary2ConsultantToCustomer = consultantToCustomer.Where(m => m.IdConsultant == allCustomersViewModel._Consultant.Id);
            foreach (var selected in temporary2ConsultantToCustomer)
            {
                allCustomersViewModel.MyCustomerList.Add(new CustomerWithInsightstatus()
                {
                    customer = await _context.Customer.FirstOrDefaultAsync(x => x.Id == selected.IdCustomer),
                    insightSimple = getInsight(allCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightExtended = getExtendedInsight(allCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightSimpleString = getInsightString(allCustomersViewModel._Consultant.Id, selected.IdCustomer),
                    insightExtendedString = getExtendedInsightString(allCustomersViewModel._Consultant.Id, selected.IdCustomer)
                });
            }

            // Noch nicht zugeordnete Kunden finden
            foreach (var item in _context.Customer.ToList())
            {
                if (foundCustomer.FirstOrDefault(m => m.Id == item.Id) == null)
                    allCustomersViewModel.NoConsultant.Add(item);
            }
            return View(allCustomersViewModel);
        }

        /// <summary>
        /// Zeigt die Profilseite des jeweiligen Steuerberaters an
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <returns> Profile View</returns>
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            /*WICHTIG 

            Eingeloggter User für Navbar Layout eingeloggte User mitgegeben werden!!!!!
            Bei Fragen an Thomas wenden

            WICHTIG*/
            string name = HttpContext.User.Identity.Name;
            BasicViewModel data = new BasicViewModel();
            data._Consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            return View(data);
        }


        /// <summary>
        /// Validiert und setzt ggf. die Profildaten auf neue Werte. 
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="form_data">Die vom User übergebene neuen Profildaten </param>
        /// <returns> Profil View mit neuen Werten falls die Validierung erfolgreich war, sonst alte Werte</returns>
        [HttpPost]
        public async Task<IActionResult> Profile(BasicViewModel form_data)
        {
            if (ModelState.IsValid)
            {
                _context.updateConsultantData(form_data._Consultant);
            }
            else
            {
                // Um in der Navbar den korrekten Namen anzuzeigen.
                string current_username = User.Identity.Name;
                Consultant? correct_user = await _context.Consultant.FirstOrDefaultAsync(u => u.Username == current_username);
                ViewData["error-case-name"] = correct_user.Firstname;
                ViewData["error-case-surname"] = correct_user.Lastname;
                ViewData["error-case-pathToProfileImg"] = correct_user.Pathtoimage;
                return View(form_data);
            }
            return RedirectToAction("Profile");
        }

        /// <summary>
        /// Aktualisiert das Profilbild des Nutzers
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="imageFile">Die vom User übergebene Bilddatei</param>
        /// <returns> Profil View mit neuem Profilbild falls die Validierung erfolgreich war, sonst eine Fehlermeldung</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadProfileImg(IFormFile imageFile)
        {
            if (imageFile != null && isValidImage(imageFile))
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "img/profile_img");
                string uniqueFileName = Guid.NewGuid().ToString() + "_" + imageFile.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);

                string relativeFilePath = "~" + filePath.Replace(_webHostEnvironment.WebRootPath, string.Empty)
                                                      .Replace("\\", "/");
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    imageFile.CopyTo(stream);
                    string current_username = User.Identity.Name;
                    Consultant? current_user = await _context.Consultant.FirstOrDefaultAsync(u => u.Username == current_username);
                    if (current_user != null)
                    {
                        string PathToPrevImage = current_user.Pathtoimage;
                        current_user.Pathtoimage = relativeFilePath;
                        _context.Update(current_user);
                        _context.SaveChanges();
                        //Delete prev profile img
                        DeletePrevProfileImg(PathToPrevImage);
                    }
                }
            }

            BasicViewModel data = await getCurrentConsultant();
            return View("Profile", data);

        }

        /// <summary>
        /// Hilfsmethode: Löscht das aktuelle Profilbild des Steuerberaters. 
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="PathToPrevImage">Pfad zum aktuellen Profilbild des Steuerberaters</param>
        /// <returns></returns>
        private void DeletePrevProfileImg(string PathToPrevImage)
        {
            string fileName = Path.GetFileName(PathToPrevImage);
            string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "img/profile_img", fileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }

        /// <summary>
        /// Hilfsmethode: Überprüft ob die vom User übergebene Datei tatsächlich eine Bilddatei ist, ob diese eine gültige Extension und Größe(5 MB) hat.
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param name="ImageFile">Die vom User übergebene Datei</param>
        /// <returns>Boolean</returns>
        private bool isValidImage(IFormFile ImageFile)
        {
            long fileSizeLimit = 5 * 1024 * 1024;
            string[] allowedExtensions = { ".jpg", ".jpeg", ".png" };
            string fileExtension = Path.GetExtension(ImageFile.FileName).ToLowerInvariant();
            if (!allowedExtensions.Contains(fileExtension))
            {
                ViewData["ErrorMessage"] = "Falsche Extension";
                return false;

            }
            else if (ImageFile.Length > fileSizeLimit)
            {
                ViewData["ErrorMessage"] = "Die Datei ist zu groß";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hilfsmethode: Lädt das aktuelle Steuerberaterobjekt aus der Datenbank und erstellt eine BasicViewModel
        /// CodeOwner: Vladyslav Kovganko
        /// </summary>
        /// <param></param>
        /// <returns>BasicViewModel für die Navbar, die auf den aktuellen Steuerberater referenziert.</returns>
        //BasicViewModel für die Navbar
        private async Task<BasicViewModel> getCurrentConsultant()
        {
            string name = HttpContext.User.Identity.Name;
            BasicViewModel data = new BasicViewModel();
            data._Consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);
            return data;
        }



        /// <summary>
        /// Zeigt die Details eines bestimmten Consultant Users an
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den Jeweiligen User von dem die Details angezeigt werden sollen</param>
        /// <returns> Details view des spezifizierten Users</returns>

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Consultant == null)
            {
                return NotFound();
            }

            var consultant = await _context.Consultant
                .FirstOrDefaultAsync(m => m.Id == id);
            if (consultant == null)
            {
                return NotFound();
            }

            return View(consultant);
        }

        /// <summary>
        /// Get Methode um einen ConsultantUser zu erstellen
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <returns> View zum erstellen eines Consuiltants</returns>

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Post Methode um einen Consultant User zu erstellen, Hasht das Passwort mit SHA256, ändert den eingegeben Usernamen in Kleinbuchstaben(um Groß-leinschreibung beim Usernamen irrelevant zu machen) und legt die Daten in der consultantTabelle ab. Außerdem wird der Pfad für das Standarduserbild festgelegt
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="consultant"> es wird ein Consultantobjekt eingegeben mit Username und Passwort und allen weiteren Attributen das vom User auf der Create Seite eingegeben wurde</param>
        /// <returns> Entweder die Create seite bei einem Fehler bei der Validierung oder Die Admin Startseite</returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Firstname,Lastname,Username,Password,Address,Email,Pathtoimage")] Consultant consultant)
        {

            if (ModelState.IsValid)
            {
                //Standardbild wird hinterlegt
                consultant.Pathtoimage = "~/img/profile_img/customer_profile_img.png";
                //Passwort wird mit sha 256 gehasht
                consultant.Password = PasswordHash.CreateHash(consultant.Password);
                //username wird in kleinbuchstaben umgewandelt, um die groß kleinschreibung im username hinfällig zu machen
                consultant.Username = consultant.Username.ToLower();
                _context.Add(consultant);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Account");
            }
            return View(consultant);
        }

        /// <summary>
        /// Get Methode zum bearbeiten eines Consultant Users
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID zum spezifizieren welcher User bearbeitet werden Soll</param>
        /// <returns> Edit Page des spezifizierten Users</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Consultant == null)
            {
                return NotFound();
            }

            var consultant = await _context.Consultant.FindAsync(id);
            if (consultant == null)
            {
                return NotFound();
            }
            return View(consultant);
        }

        /// <summary>
        /// Post Methode um die bearbeiteten Daten in der Datenbank zu speichern. Falls sich das Passwort geändert hat, wird es neu gehasht und das Objekt wird in der Datenbank gespeichert
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> ID des zu bearbeitenden Consultantobjekts </param>
        /// <param name="consultant"> es wird ein Consultantobjekt eingegeben mit allen Attributen das vom User auf der Bearbeiten Seite eingegeben wurde</param>
        /// <returns> Bei fehlgeshclagener Validierung GET: Edit bei erfolgreicher Validierung die Admin Startseite</returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Firstname,Lastname,Username,Password,Address,Email,Pathtoimage")] Consultant consultant)
        {
            if (id != consultant.Id)
            {
                return NotFound();
            }
            consultant.Username = consultant.Username.ToLower();
            Consultant sb = await _context.Consultant.FirstOrDefaultAsync(x => x.Id == consultant.Id);
            //Falls sich Passwort geändert hat, neu hashen
            if (consultant.Password != sb.Password)
            {
                consultant.Password = PasswordHash.CreateHash(consultant.Password);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(consultant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!consultantExists(consultant.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Account"); ;
            }
            return View(consultant);
        }

        /// <summary>
        /// Get Methode für das Löschen eines Consultantusers
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> spezifiziert den zu löschenden Consultantuser</param>
        /// <returns> Löschview des jeweiligen Users</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Consultant == null)
            {
                return NotFound();
            }

            var consultant = await _context.Consultant
                .FirstOrDefaultAsync(m => m.Id == id);
            if (consultant == null)
            {
                return NotFound();
            }

            return View(consultant);
        }

        /// <summary>
        /// PostMethode zum Löschen eines Users nach Bestätigung durch den Benutzer
        /// CodeOwner: Thomas Gerum
        /// </summary>
        /// <param name="id"> Spezifiziert den zu Löschenden User</param>
        /// <returns> Startseite des eingeloggten Users</returns>
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Consultant == null)
            {
                return Problem("Entity set 'MvcAccountContext.Consultant'  is null.");
            }
            var consultant = await _context.Consultant.FindAsync(id);
            if (consultant != null)
            {
                _context.Consultant.Remove(consultant);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Account"); ;
        }

        private bool consultantExists(int id)
        {
            return _context.Consultant.Any(e => e.Id == id);
        }


        /// <summary>
        /// Initalisiert das <c>InsightViewModel</c> welches für die <c>Insight</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <returns> Insight View </returns>

        public IActionResult Insight(int customerId)
        {
            string name = HttpContext.User.Identity.Name;
            InsightViewModel insightvm = new InsightViewModel();
            insightvm._Consultant = _context.Consultant.FirstOrDefault(x => x.Username.ToLower() == name);
            insightvm._Customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);

            if (!_context.ConsultantToCustomer.Any(e => e.IdCustomer == customerId && e.IdConsultant == insightvm._Consultant.Id && e.InsightStatusSimple == InsightStatus.Insight))
                return RedirectToAction("AllCustomers", "consultant");

            return View(insightvm);
        }

        /// <summary>
        /// Initalisiert das <c>InsightViewModel</c> welches für die <c>ExtendedInsight</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <returns> ExtendedInsight View </returns>
        public IActionResult ExtendedInsight(int customerId)
        {
            string name = HttpContext.User.Identity.Name;
            InsightViewModel insightvm = new InsightViewModel();
            insightvm._Consultant = _context.Consultant.FirstOrDefault(x => x.Username.ToLower() == name);
            insightvm._Customer = _context.Customer.FirstOrDefault(x => x.Id == customerId);
            insightvm.AssetList = cont.CreateAssetsConsultant(insightvm._Customer.Id);

            if (!_context.ConsultantToCustomer.Any(e => e.IdCustomer == customerId && e.IdConsultant == insightvm._Consultant.Id && e.InsightStatusExtended == InsightStatus.Insight))
                return RedirectToAction("AllCustomers", "consultant");

            return View(insightvm);
        }


        /// <summary>
        /// Die funktion RemoveMessage entfernt eine Nachricht in der Datenbank 
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="messageId"> Spezifiziert die Nachricht</param>
        [HttpPost]
        public async Task RemoveMessage(int messageId)
        {
            MessageProcessor mp = new MessageProcessor() { };
            await mp.RemoveMessage(_context, messageId);
        }

        /// <summary>
        /// Initalisiert das <c>TaxReportViewModel</c> welches für die <c>ExtendedInsightTaxReport</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux (von Vlad Kopiert)
        /// </summary>
        /// <param name="cusId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="year"> spezifiziert das zu betrachtende Jahr</param>
        /// <returns> ExtendedInsightTaxReport View </returns>
        [HttpPost]
        public async Task<IActionResult> ExtendedInsightTaxReport(int year, int cusId)
        {
            //überprüfe das eingegebene Jahr
            if (year < 0)
            {
                return RedirectToAction("ExtendedInsight", new { customerId = cusId });
            }

            // übergibt den derzeitigen Nutzer an das View Model
            string name = HttpContext.User.Identity.Name;
            Consultant current_consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);

            var customer = await _context.Customer.FirstOrDefaultAsync(x => x.Id == cusId);

            // sichert ab ob die richtige einsicht überhaupt vorliegt
            if (!_context.ConsultantToCustomer.Any(e => e.IdCustomer == cusId && e.IdConsultant == current_consultant.Id && e.InsightStatusExtended == InsightStatus.Insight))
                return RedirectToAction("AllCustomers", "consultant");

            List<Transaction> transactions = cont.GetAllTransactionsCustomer(customer.Id);
            TaxReportViewModel taxReportViewModel = new TaxReportViewModel();
            taxReportViewModel._Consultant = current_consultant;
            taxReportViewModel._Customer = customer;

            // versucht einen TaxReport (Steuerbericht) zu erstellen
            try
            {
                TaxReport taxReport = new TaxReport(year, transactions);
                taxReportViewModel._taxReport = taxReport;

            }
            // sollte ein Coin einen ungültigen Status aufweisen wird umgehend auf den zuerst gefundenen Fehlercoin hingewiesen
            catch (GhostCoinException ex)
            {
                ViewData["Error-msg"] = "Fehler: Sie haben mehr " + ex.ghostCoin + " verkauft als gekauft.";
                return View(taxReportViewModel);
            }

            return View(taxReportViewModel);
        }


        /// <summary>
        /// Initalisiert das <c>TaxReportViewModel</c> welches für die <c>InsightTaxReport</c> View benötigt wird
        /// Nutzer: Steuerberater
        /// CodeOwner: Alexander Rioux (von Vlad Kopiert)
        /// </summary>
        /// <param name="cusId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="year"> spezifiziert das zu betrachtende Jahr</param>
        /// <returns> InsightTaxReport View </returns>
        [HttpPost]
        public async Task<IActionResult> InsightTaxReport(int year, int cusId)
        {
            //überprüfe das eingegebene Jahr
            if (year < 0)
            {
                return RedirectToAction("ExtendedInsight", new { customerId = cusId });
            }

            // übergibt den derzeitigen Nutzer an das View Model
            string name = HttpContext.User.Identity.Name;
            Consultant current_consultant = await _context.Consultant.FirstOrDefaultAsync(x => x.Username.ToLower() == name);

            var customer = await _context.Customer.FirstOrDefaultAsync(x => x.Id == cusId);

            // sichert ab ob die richtige einsicht überhaupt vorliegt
            if (!_context.ConsultantToCustomer.Any(e => e.IdCustomer == cusId && e.IdConsultant == current_consultant.Id && e.InsightStatusSimple == InsightStatus.Insight))
                return RedirectToAction("AllCustomers", "consultant");

            List<Transaction> transactions = cont.GetAllTransactionsCustomer(customer.Id);
            TaxReportViewModel taxReportViewModel = new TaxReportViewModel();
            taxReportViewModel._Consultant = current_consultant;
            taxReportViewModel._Customer = customer;

            // versucht einen TaxReport (Steuerbericht) zu erstellen
            try
            {
                TaxReport taxReport = new TaxReport(year, transactions);
                taxReportViewModel._taxReport = taxReport;

            }
            // sollte ein Coin einen ungültigen Status aufweisen wird umgehend auf den zuerst gefundenen Fehlercoin hingewiesen
            catch (GhostCoinException ex)
            {
                ViewData["Error-msg"] = "Fehler: Sie haben mehr " + ex.ghostCoin + " verkauft als gekauft.";
                return View(taxReportViewModel);
            }


            return View(taxReportViewModel);
        }

        /// <summary>
        /// Gibt den Status der einfachen Einsicht zurück
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>InsightStatus</returns>
        private InsightStatus getInsight(int consultantId, int customerId)
        {
            return _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId).InsightStatusSimple;
        }


        /// <summary>
        /// Gibt den Status der erweiterten Einsicht zurück
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>InsightStatus</returns>
        private InsightStatus getExtendedInsight(int consultantId, int customerId)
        {
            return _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId).InsightStatusExtended;
        }


        /// <summary>
        /// Gibt den Status als String der einfachen Einsicht zurück
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>String</returns>
        private String getInsightString(int consultantId, int customerId)
        {
            InsightStatus insight = _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId).InsightStatusSimple;
            if (insight == InsightStatus.None)
            {
                return "keine Einsicht";
            }
            else if (insight == InsightStatus.Insight)
            {
                return "Einsicht";
            }
            else
            {
                return "Angefragt";
            }
        }


        /// <summary>
        /// Gibt den Status als String der erweiterten Einsicht zurück
        /// CodeOwner: Alexander Rioux
        /// </summary>
        /// <param name="customerId"> spezifiziert den zu betrachtenden Kunden</param>
        /// <param name="consultantId"> spezifiziert den zu betrachtenden Steuerberater</param>
        /// <returns>String</returns>
        private String getExtendedInsightString(int consultantId, int customerId)
        {
            InsightStatus insight = _context.ConsultantToCustomer.Where(m => m.IdConsultant == consultantId).FirstOrDefault(m => m.IdCustomer == customerId).InsightStatusExtended;
            if (insight == InsightStatus.None)
            {
                return "keine Einsicht";
            }
            else if (insight == InsightStatus.Insight)
            {
                return "Einsicht";
            }
            else
            {
                return "Angefragt";
            }
        }

    }
}
