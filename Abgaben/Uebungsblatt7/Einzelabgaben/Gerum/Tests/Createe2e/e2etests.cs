using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Createe2e;


[TestClass]
public class e2etests
{
    [TestMethod]
    public void TestMethod1erfolgreich()
    {

        //applikation läuft in Docker
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl("http://localhost:5008/Teachingplan");
        System.Threading.Thread.Sleep(1000);
        var createButton = driver.FindElement(By.XPath("/html/body/div/main/p[1]/a"));
        createButton.Click();
        var name = driver.FindElement(By.Name("Title"));
        var link = driver.FindElement(By.Name("CardDeckLink"));
        var dauer = driver.FindElement(By.Name("TimeEstimation"));
        var create = driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[4]/input"));
        String namen = "test123";
        String links = "http://youtube.de";
        String dauern = "00:30:00";
        name.SendKeys("test123");
        link.SendKeys("http://youtube.de");
        dauer.SendKeys("0:30");
        create.Click();
        var addedName = driver.FindElement(By.XPath("/html/body/div/main/table/tbody/tr/td[2]"));
        var addedLink = driver.FindElement(By.XPath("/html/body/div/main/table/tbody/tr/td[3]"));
        var addedDauer = driver.FindElement(By.XPath("/html/body/div/main/table/tbody/tr/td[4]"));
        Assert.AreEqual(namen, addedName.Text);
        Assert.AreEqual(links, addedLink.Text);
        Assert.AreEqual(dauern, addedDauer.Text);
        
    }

    [TestMethod]
    public void TestMethod1fehlerhafteeingaben()
    {

        //applikation läuft in Docker
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl("http://localhost:5008/Teachingplan");
        System.Threading.Thread.Sleep(1000);
        var createButton = driver.FindElement(By.XPath("/html/body/div/main/p[1]/a"));
        createButton.Click();
        var name = driver.FindElement(By.Name("Title"));
        var link = driver.FindElement(By.Name("CardDeckLink"));
        var dauer = driver.FindElement(By.Name("TimeEstimation"));
        var create = driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[4]/input"));
        name.SendKeys("");
        link.SendKeys("youtube.de");
        dauer.SendKeys("");
        create.Click();
        var nameError = driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[1]/span"));
        var linkError = driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[2]/span"));
        var dauerError = driver.FindElement(By.XPath("/html/body/div/main/div[1]/div/form/div[3]/span"));
        Assert.AreEqual("Geben Sie einen Titel ein", nameError.Text);
        Assert.AreEqual("The Link field is not a valid fully-qualified http, https, or ftp URL.", linkError.Text);
        Assert.AreEqual("Geben Sie eine gültige TimeSpan Dauer ein", dauerError.Text);
        //Assert.AreEqual("123", "123");
        
    }
}