using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;
using SoPro23Team08.Controllers;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace Reader.Tests;

public class UnitTest1
{

    [Fact]
    public void Test1positiv()
    {
        using var stream = new MemoryStream(File.ReadAllBytes("c:/curriculum-cards.json").ToArray());
        var formFile = new FormFile(stream, 0, stream.Length, "streamFile", "curriculum-cards");
        
        var mockContext = new Mock<MvcLessonContext>();
        var testlesson = new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var testlessonzwei = new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var lessonslist = new List<Lesson>
        {
            new Lesson {Id = 123, Title = "tesst", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
            new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
        }.AsQueryable();

                
        
        var mockSet = new Mock<DbSet<Lesson>>();
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Provider).Returns(lessonslist.Provider);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Expression).Returns(lessonslist.Expression);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.ElementType).Returns(lessonslist.ElementType);
        mockSet.As<IQueryable<Lesson>>().Setup(m => m.GetEnumerator()).Returns(() => lessonslist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        //mockContext.Setup(m => m.Remove()).Verifiable();
        //mockContext.Setup(m => m.Add()).Verifiable();
        mockContext.Setup(c => c.Lesson).Returns(mockSet.Object);

        
        var service = new SoPro23Team08.Controllers.TeachingplanController(mockContext.Object);
        service.Read(formFile);
        List<Lesson> finaltestlist = service.getLessons();


        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(2));
        mockContext.Verify(m => m.Add(It.IsAny<Lesson>()), Times.Exactly(1));
        mockContext.Verify(m => m.Remove(It.IsAny<Lesson>()), Times.Exactly(1));
        //Assert.Equal(testlesson, finaltestlist[0]);
    }

    [Fact]
    public void Test2negativ()
    {
        using var stream = new MemoryStream(File.ReadAllBytes("c:/curriculum-cardstwo.json").ToArray());
        var formFile = new FormFile(stream, 0, stream.Length, "streamFile", "curriculum-cards");
        
        var mockContext = new Mock<MvcLessonContext>();
        var testlesson = new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var testlessonzwei = new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var lessonslist = new List<Lesson>
        {
            new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
            new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
        }.AsQueryable();

                
        
        var mockSet = new Mock<DbSet<Lesson>>();
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Provider).Returns(lessonslist.Provider);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Expression).Returns(lessonslist.Expression);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.ElementType).Returns(lessonslist.ElementType);
        mockSet.As<IQueryable<Lesson>>().Setup(m => m.GetEnumerator()).Returns(() => lessonslist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        //mockContext.Setup(m => m.Remove(It.IsAny<Lesson>())).Verifiable();
        //mockContext.Setup(m => m.Add(It.IsAny<Lesson>()).Verifiable());
        mockContext.Setup(c => c.Lesson).Returns(mockSet.Object);

        
        var service = new SoPro23Team08.Controllers.TeachingplanController(mockContext.Object);
        service.Read(formFile);


        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(1));
        mockContext.Verify(m => m.Add(It.IsAny<Lesson>()), Times.Exactly(1));
        mockContext.Verify(m => m.Remove(It.IsAny<Lesson>()), Times.Exactly(0));
        Assert.NotNull(stream);
        Assert.NotNull(service);
    }

    [Fact]
    public void Test3emptyfile()
    {
        using var stream = new MemoryStream(File.ReadAllBytes("c:/curriculum-cardsthree.json").ToArray());
        var formFile = new FormFile(stream, 0, stream.Length, "streamFile", "curriculum-cards");
        
        var mockContext = new Mock<MvcLessonContext>();
        var testlesson = new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var testlessonzwei = new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)};
        var lessonslist = new List<Lesson>
        {
            new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
            new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = TimeSpan.FromHours((double) 1)},
        }.AsQueryable();

                
        
        var mockSet = new Mock<DbSet<Lesson>>();
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Provider).Returns(lessonslist.Provider);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.Expression).Returns(lessonslist.Expression);
        mockSet.As<IQueryable<MvcLessonContext>>().Setup(m => m.ElementType).Returns(lessonslist.ElementType);
        mockSet.As<IQueryable<Lesson>>().Setup(m => m.GetEnumerator()).Returns(() => lessonslist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Lesson).Returns(mockSet.Object);

        
        var service = new SoPro23Team08.Controllers.TeachingplanController(mockContext.Object);
        service.Read(formFile);


        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(0));
        mockContext.Verify(m => m.Add(It.IsAny<Lesson>()), Times.Exactly(0));
        mockContext.Verify(m => m.Remove(It.IsAny<Lesson>()), Times.Exactly(0));
        Assert.NotNull(stream);
        Assert.NotNull(service);
    }
}