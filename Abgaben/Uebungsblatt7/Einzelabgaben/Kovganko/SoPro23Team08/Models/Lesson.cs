﻿
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Xml.Linq;

namespace SoPro23Team08.Models
{
    public class Lesson
    {
       // [Display(Name = "Id")]
       // [Required(ErrorMessage = "Geben Sie eine Id ein")]
        [Range(uint.MinValue, uint.MaxValue, ErrorMessage = "Eine Id darf nicht negativ sein")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Geben Sie einen Titel ein")]
        [MinLength(1, ErrorMessage = "Geben Sie einen Titel ein")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Geben Sie einen gültigen Link ein")]
        [Url]
        public string url { get; set; }
 
        [Required(ErrorMessage = "Geben Sie einen Aufwand ein")]
        [Range(0, double.MaxValue, ErrorMessage = "Ein Aufwand darf nicht negativ sein")]
        public double? estimate { get; set; }

      /*  public Lesson(string Title, string CardDeckLink, int TimeEstimation){
            this.Id = nextId++;
            this.Title = Title;
            this.CardDeckLink = CardDeckLink;
            this.TimeEstimation = TimeEstimation;
        }*/

        public string toString()
        {
            return Id + " " + Title + " " + url + " " + estimate;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Lesson other = (Lesson)obj;
            return Id == other.Id && Title== other.Title && url == other.url && estimate == other.estimate;
        }

        public static Boolean checkContraints(Lesson l)
        {
            if (((l.Id != null) && l.Id < 0) || (l.Title == null) || (l.Title == "") || (l.url == null) || (l.url == "") || (l.estimate == null) || l.estimate <= 0)
                return false; 
            return true;
        }
    }
}