﻿using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;

namespace SoPro23Team08.Data
{
    public class LessonContext : DbContext
    {
        public string DbPath { get; }
        public DbSet<Lesson> Lessons { get; set; }

        public LessonContext()
        {
            var folder = Environment.CurrentDirectory;
            var path = System.IO.Path.Join(folder, "persistence");

            DbPath = System.IO.Path.Join(path, "lesson.db");
        }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        /*
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
        */
        
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("LessonDatabase");
            //optionsBuilder.UseSqlite($"Data Source={DbPath}");
            optionsBuilder.EnableThreadSafetyChecks(false);
        }
    }
}
