﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SoPro23Team08.Migrations
{
    /// <inheritdoc />
    public partial class change_col_names : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TimeEstimation",
                table: "Lessons",
                newName: "estimate");

            migrationBuilder.RenameColumn(
                name: "CardDeckLink",
                table: "Lessons",
                newName: "url");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "url",
                table: "Lessons",
                newName: "CardDeckLink");

            migrationBuilder.RenameColumn(
                name: "estimate",
                table: "Lessons",
                newName: "TimeEstimation");
        }
    }
}
