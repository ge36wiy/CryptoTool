using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace E2E_Tests
{
    public class UnitTest1
    {
        [Fact]
        public void TestCreateLection()
        {
            IWebDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl("https://localhost:7163/Teachingplan/CreateLesson");

            IWebElement titleField = driver.FindElement(By.Name("Title"));
            IWebElement estimateField = driver.FindElement(By.Name("estimate"));
            IWebElement urlField = driver.FindElement(By.Name("url"));
            IWebElement addBtn = driver.FindElement(By.ClassName("btn-primary"));

            //Enter data
            titleField.SendKeys("Python");
            estimateField.SendKeys("15");
            urlField.SendKeys("https://www.python.org/");
            addBtn.Click();
            Thread.Sleep(1000);


            IReadOnlyList<IWebElement> allCards = driver.FindElements(By.ClassName("card"));
            IWebElement addedCard = allCards.Last();

            

            string addedTitle = addedCard.FindElement(By.ClassName("card-title")).Text.Split(' ')[1];
            string addedEstimation = addedCard.FindElement(By.ClassName("badge-primary")).Text.Split(' ')[0];
            string addedURL = addedCard.FindElement(By.ClassName("card-link")).GetAttribute("href");


            Assert.Equal("Python", addedTitle);
            Assert.Equal("15", addedEstimation);
            Assert.Equal("https://www.python.org/", addedURL);
        }


        [Fact]
        public void TestCreateLection_IncorrectInput()
        {
            IWebDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl("https://localhost:7163/Teachingplan/CreateLesson");

            IWebElement titleField = driver.FindElement(By.Name("Title"));
            IWebElement estimateField = driver.FindElement(By.Name("estimate"));
            IWebElement urlField = driver.FindElement(By.Name("url"));
            IWebElement addBtn = driver.FindElement(By.ClassName("btn-primary"));

            //Enter data
            titleField.SendKeys("Python");
            estimateField.SendKeys("-100");
            urlField.SendKeys("https://www.python.org/");
            addBtn.Click();
            Thread.Sleep(1000);


            string errorMsg = driver.FindElement(By.ClassName("field-validation-error")).Text;



            Assert.Equal("Ein Aufwand darf nicht negativ sein", errorMsg);
     
        }
    }
}