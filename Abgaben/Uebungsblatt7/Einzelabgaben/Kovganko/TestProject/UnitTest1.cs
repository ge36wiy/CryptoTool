﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SoPro23Team08.Controllers;
using SoPro23Team08.Models;
using System;
using System.Text;
using TestProject.persistence;

namespace TestProject
{
    public class UnitTest1
    {

        private string GetJsonFromFile(string filePath)
        {
            string jsonString = string.Empty;

            using (StreamReader reader = new StreamReader(filePath))
            {
                jsonString = reader.ReadToEnd();
            }

            return jsonString;
        }

        private IFormFile CreateJsonFormFile(string json)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(json);
            MemoryStream stream = new MemoryStream(data);

            FormFile formFile = new FormFile(stream, 0, data.Length, "jsonFile", "data.json");
            formFile.Headers = new HeaderDictionary();
            formFile.ContentType = "application/json";

            return formFile;
        }

        private IFormFile emulateHttpRequest()
        {
            string solutionPath = AppDomain.CurrentDomain.BaseDirectory;  
            string relativePath = "\\Data\\curriculum-cards.json"; 

            string filePath = solutionPath + relativePath;

            string jsonStr = GetJsonFromFile("D:\\Uni\\SoPro\\Blatt7\\TestProject\\Data\\curriculum-cards.json");
            IFormFile jsonFile = CreateJsonFormFile(jsonStr);
            return jsonFile;    
        }

        private List<Lesson> expectedLessons()
        {
            string lessonsJson = GetJsonFromFile("D:\\Uni\\SoPro\\Blatt7\\TestProject\\Data\\curriculum-cards.json");
            List<Lesson> LessonList = JsonConvert.DeserializeObject<List<Lesson>>(lessonsJson);
            return LessonList;
        }

        // Test ob alle Lektionen hinzugefügt wurden
        [Fact]
        public void Test_SaveLessonsFromJson()
        {
            using(TeachingplanController teachingplanController = new TeachingplanController())
            {
                //Arrange
                IFormFile uploadedJsonFile = emulateHttpRequest();
                LessonContainer lessonContainer = teachingplanController.GetLessonContainer();
                //Try
                teachingplanController.UploadJson(uploadedJsonFile);



                //Assert
                Assert.Equal(5, lessonContainer.getLessons().Count());
            }
        }


        //Test ob die hinzugefügten Lektionen korrekt sind
        [Fact]
        public void Test_checkLessonsFromJson()
        {
            using (TeachingplanController teachingplanController = new TeachingplanController())
            {
                //Arrange
                IFormFile uploadedJsonFile = emulateHttpRequest();
                teachingplanController.UploadJson(uploadedJsonFile);
                List<Lesson> uploadedLessons = teachingplanController.GetLessonContainer().getLessons();

                List<Lesson> expected_lessons = expectedLessons();

                foreach (Lesson l in expected_lessons)
                {
                  
                    Assert.Contains(l, uploadedLessons);
                }
            }
        }
        
        // Test ob eine fehlerhafte Lektion hinzugefügt wird
        [Fact]
        public void Test_trashJson()
        {
            //Arrange
            string trashJson = JsonConvert.
            SerializeObject(new Lesson {Id=-100, Title = "Error", url="google.com", estimate=-888 });
            IFormFile trashJsonFile = CreateJsonFormFile("[" + trashJson + "]");

            using (TeachingplanController teachingplanController = new TeachingplanController())
            {
                List<Lesson> uploadedLessons = teachingplanController.GetLessonContainer().getLessons();
                int lessonsCountBefore = uploadedLessons.Count();
                IFormFile uploadedJsonFile = emulateHttpRequest();
                //Try
                teachingplanController.UploadJson(trashJsonFile);


                int lessonsCountAfter = uploadedLessons.Count();
                //Assert
                Assert.Equal(lessonsCountBefore, lessonsCountAfter);
                //=> keine neuen Lektionen wurden hinzugefügt! 
            }

        }
      
    }
}