﻿using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;

namespace TestProject.persistence
{
    internal class LessonContext : DbContext
    {
        public LessonContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Lesson> lessons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("LessonDatabase");
        }
    }
}
