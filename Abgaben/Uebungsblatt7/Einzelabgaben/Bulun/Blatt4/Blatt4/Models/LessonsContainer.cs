﻿using Blatt4.Data;

namespace Blatt4.Models
{
    public class LessonsContainer
    {
        public static Lessoncontext db = new Lessoncontext();
        
        public List<Lesson> getLessons()
        {
            return db.Lessons.ToList();
        }

        public void addLesson(Lesson lesson)
        {
            db.Lessons.Add(lesson);
            db.SaveChanges();
        }

        public void removeLesson(int id)
        {
            Lesson? remove = db.Find<Lesson>(id);
            if (remove != null) { 
                db.Remove(remove);
                db.SaveChanges();
            }

        }

        public void updateLesson(Lesson lesson)
        {
            if (lesson != null)
            {
                db.Update(lesson);
                db.SaveChanges();  
            }

        }

        public Lesson getLesson(int id)
        {
            Lesson? lesson = db.Find<Lesson>(id);
            return lesson;
        }
        public Boolean ContainsLesson(Lesson lesson)
        {
            foreach (Lesson abc in db.Lessons.ToList())
            {
                if (abc.Id == lesson.Id)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
