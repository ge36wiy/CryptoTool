using Blatt4.Controllers;
using Blatt4.Data;
using Blatt4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using System.IO;
using Moq.EntityFrameworkCore;
using Moq;
using Microsoft.AspNetCore.Http;

namespace Blatt4.UnitTests
{
    public class TeachingPlanUnitTest
    {
        /*public IFormFile createFileOne()
        {
            using (StreamWriter sw = new StreamWriter("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson.json"))
            {
                sw.WriteLine("[");
                sw.WriteLine("{");
                sw.WriteLine("\"Id\": 36342,");
                sw.WriteLine("\"Title\": \"000 Start here!\",");
                sw.WriteLine("\"CardDeckLink\": \"https://www.uni-augsburg.de/de/fakultaet/fai/isse/prof/swtse/teaching/students/\",");
                sw.WriteLine("\"TimeEstimation\": 0.0");
                sw.WriteLine(" },");
                sw.WriteLine("]");
                using (var stream = File.OpenRead("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson.json"))
                {
                    var file = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));
                    return file;
                }
            }
        }
        public IFormFile createFileTwo()
        {
            {

                    using (StreamWriter sw = new StreamWriter("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson2.json"))
                    {
                        sw.WriteLine("[");
                        sw.WriteLine("{");
                        sw.WriteLine("\"Id\": 36342,");
                        sw.WriteLine("\"Title\": \"HelloWorld\",");
                        sw.WriteLine("\"CardDeckLink\": \"https://Google.de\",");
                        sw.WriteLine("\"TimeEstimation\": 2.0");
                        sw.WriteLine(" },");
                        sw.WriteLine("]");
                    }
                    using (var stream = File.OpenRead("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson2.json"))
                    {
                        var file = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));
                        return file;
                    }
                }
            }
        



        
        
        public void deleteFiles() 
        {
            File.Delete("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson2.json");
            File.Delete("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\Lesson.json");

        }
        public IFormFile getFileOne()
        {
            using (var stream = File.OpenRead("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards.json"))
            {
                var file = new FormFile(stream, 0, stream.Length, null, "curriculum-cards.json");
                return file;
            }
        }
        public IFormFile getFileTwo()
        {
            using (var stream = File.OpenRead("C:\\Users\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards2.json"))
            {
                var file = new FormFile(stream, 0, stream.Length, null, "curriculum-cards2.json");
                return file;
            }
        }
        */
        [Fact]
        public void ImportJsonFile_EditLesson_Success()
        {
            //Arrange
            IFormFile a;
            var stream = File.OpenRead("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards.json");
            
                IFormFile file = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));
                a = file;
            
            IFormFile b;
            var stream2 = File.OpenRead("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards3.json"); 
            
                IFormFile file2= new FormFile(stream2, 0, stream2.Length, null, Path.GetFileName(stream2.Name));
                b = file2;
            
            LessonsContainer lessonsContainer = new LessonsContainer();
            TeachingplanController Controller = new TeachingplanController();



            //Act
            Controller.ImportFile(a);
            Controller.ImportFile(b);


            //Assert
            Assert.Equal("HelloWorld", lessonsContainer.getLesson(36342).Title);
            Assert.Equal("https://www.uni-augsburg.de/de/fakultaet/fai/isse/prof/swtse/teaching/students/", lessonsContainer.getLesson(36342).CardDeckLink);
            Assert.Equal(12.0, lessonsContainer.getLesson(36342).TimeEstimation);

            //File.Delete("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards.json");
            //File.Delete("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards2.json");
        }


        [Fact] //Validation Attribute funktionieren aktuell noch nicht im Import. Ich habe nachgelesen und die Methode SaveUpdates sollte das Problem eigentlich l�sen, tut es aber leider nicht...
        public void ImportJsonFile_NoURL_Exception()
        {
            //Arrange
            IFormFile a;
            var stream = File.OpenRead("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards.json");

            IFormFile file = new FormFile(stream, 0, stream.Length, null, Path.GetFileName(stream.Name));
            a = file;

            IFormFile b;
            var stream2 = File.OpenRead("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards2.json");

            IFormFile file2 = new FormFile(stream2, 0, stream2.Length, null, Path.GetFileName(stream2.Name));
            b = file2;

            LessonsContainer lessonsContainer = new LessonsContainer();
            TeachingplanController Controller = new TeachingplanController();



            //Act
            Controller.ImportFile(a);
            Controller.ImportFile(b);


            //Assert
            Assert.Equal("HelloWorld", lessonsContainer.getLesson(36342).Title);
            Assert.Equal("https://www.uni-augsburg.de/de/fakultaet/fai/isse/prof/swtse/teaching/students/", lessonsContainer.getLesson(36342).CardDeckLink);

            //File.Delete("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards.json");
            //File.Delete("C:\\Users\\bulun\\Desktop\\Blatt4\\Blatt4\\Blatt4.UnitTests\\JSONFiles\\curriculum-cards2.json");
        }
    }
}