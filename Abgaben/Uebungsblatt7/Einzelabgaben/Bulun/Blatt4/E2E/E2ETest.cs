using Microsoft.AspNetCore.Mvc.Testing;
using OpenQA.Selenium.Edge;

namespace E2E
{
    public class E2ETest
    {
        // Mein erster Testfall funktioniert aktuell nicht korrekt, weil ich das letzte Element meiner Liste brauche. Alle Elemente besitzen aber die Selbe Class-Name / Class-ID, weshalb ich das Element nicht f�r Selenium ausw�hlen kann.
        [Fact]
        public void CreateLesson()
        {
            IWebDriver webDriver = new EdgeDriver();

            webDriver.Navigate().GoToUrl("https://localhost:7149/Teachingplan/CreateLesson");

            IWebElement Title = webDriver.FindElement(By.Id("Title"));
            IWebElement CardDeckLink = webDriver.FindElement(By.Id("CardDeckLink"));
            IWebElement TimeEstimation = webDriver.FindElement(By.Id("TimeEstimation"));

            Title.SendKeys("Selenium Test Item");
            CardDeckLink.SendKeys("https://www.guru99.com/accessing-forms-in-webdriver.html");
            TimeEstimation.SendKeys("13");

            IWebElement button = webDriver.FindElement(By.ClassName("btn"));
            button.Click();
            string TitleAssert = webDriver.FindElement(By.ClassName("card-title")).Text;

            string TitleAssert2 = webDriver.FindElement(By.ClassName("card-title")).Text;
            Assert.Equal("Selenium Test Item [13 d]", (string) TitleAssert);
        }

        [Fact]
        public void CreateFalseLesson()
        {
            {
                IWebDriver webDriver = new EdgeDriver();

                webDriver.Navigate().GoToUrl("https://localhost:7149/Teachingplan/CreateLesson");

                IWebElement Title = webDriver.FindElement(By.Id("Title"));
                IWebElement CardDeckLink = webDriver.FindElement(By.Id("CardDeckLink"));
                IWebElement TimeEstimation = webDriver.FindElement(By.Id("TimeEstimation"));

                Title.SendKeys("");
                CardDeckLink.SendKeys("https://www.guru99.com/accessing-forms-in-webdriver.html");
                TimeEstimation.SendKeys("13");

                IWebElement button = webDriver.FindElement(By.ClassName("btn"));
                button.Click();
                string Error = webDriver.FindElement(By.XPath("/html/body/div/main/div/div[2]/form/div[2]/span")).Text;

                Assert.Equal("The Title field is required.", (string)Error);
            }
        }
    }
}