using Microsoft.AspNetCore.Http;
using SoPro23Team08.Controllers;
using SoPro23Team08.Models;
using SoPro23Team08.Data;
using Moq;
using Microsoft.EntityFrameworkCore;


namespace SoPro23Team08.Tests;

public class UnitTest1
{
    [Fact]
    public void TestValidJson()
    {
        using var stream = new MemoryStream(File.ReadAllBytes("/home/alexanderrioux/Downloads/Lecture_Softwareprojekt/Übung/").ToArray());
        var formFile = new FormFile(stream, 0, stream.Length, "streamFile", "curriculum-cards");
        
        var mockContext = new Mock<LessonContext>();
        var testlesson = new Lesson {Id = 123, Title = "test", CardDeckLink = "youtube.de", TimeEstimation = 1};
        var testlessonzwei = new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = 1};
        var lessonslist = new List<Lesson>
        {
            new Lesson {Id = 123, Title = "tesst", CardDeckLink = "youtube.de", TimeEstimation = 1},
            new Lesson {Id = 124, Title = "test2", CardDeckLink = "youtube.de", TimeEstimation = 1},
        }.AsQueryable();

                
        
        var mockSet = new Mock<DbSet<Lesson>>();
        mockSet.As<IQueryable<LessonContext>>().Setup(m => m.Provider).Returns(lessonslist.Provider);
        mockSet.As<IQueryable<LessonContext>>().Setup(m => m.Expression).Returns(lessonslist.Expression);
        mockSet.As<IQueryable<LessonContext>>().Setup(m => m.ElementType).Returns(lessonslist.ElementType);
        mockSet.As<IQueryable<Lesson>>().Setup(m => m.GetEnumerator()).Returns(() => lessonslist.GetEnumerator());

        mockContext.Setup(m => m.SaveChanges()).Verifiable();
        mockContext.Setup(c => c.Lesson).Returns(mockSet.Object);

        
        var service = new SoPro23Team08.Controllers.TeachingplanController(mockContext.Object);
        service.Read(formFile);
        List<Lesson> finaltestlist = service.Get();


        mockContext.Verify(m => m.SaveChanges(), Times.Exactly(2));
        mockContext.Verify(m => m.Add(It.IsAny<Lesson>()), Times.Exactly(1));
    }
}