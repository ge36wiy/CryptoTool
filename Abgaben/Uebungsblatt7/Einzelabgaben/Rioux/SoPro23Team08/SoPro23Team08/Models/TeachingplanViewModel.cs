﻿using Microsoft.AspNetCore.Mvc;

namespace SoPro23Team08.Models
{
    public class TeachingplanViewModel
    {
        public List<Lesson>? Lesson { get; set; }
        public TeachingplanViewModel (List<Lesson> li)
        {
            Lesson = li;
        }
    }
}
