﻿using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;
using System.Reflection.Metadata;

namespace SoPro23Team08.Data
{
    public class LessonContext : DbContext
    {
        public DbSet<Lesson> Lessons { get; set; }
        public string DbPath { get; }
        public LessonContext()
        {
            var folder = Environment.CurrentDirectory;
            var path = System.IO.Path.Join(folder, "persistence");
            DbPath = System.IO.Path.Join(path, "lessons.db");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");
    }
}
