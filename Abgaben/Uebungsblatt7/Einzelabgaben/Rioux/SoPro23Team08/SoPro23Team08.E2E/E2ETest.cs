using System;
using System.Linq;

namespace E2E;

[TestClass]
public class E2ETest
{
    [TestMethod]
    public void korrekteLektionErstellen()
    {
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl("https://localhost:7207/Teachingplan/Create");
        IWebElement title = driver.FindElement(By.Name("Title"));
        IWebElement cardDeckLink = driver.FindElement(By.Name("CardDeckLink"));
        IWebElement timeEstimation = driver.FindElement(By.Name("TimeEstimation"));
        IWebElement submit = driver.FindElement(By.Name("Create"));

        String valTitle = "Test";
        String valCardDeckLink = "http://www.google.de/";
        String valTimeEstimation = "5.4";

        title.SendKeys(valTitle);
        cardDeckLink.SendKeys(valCardDeckLink);
        timeEstimation.SendKeys(valTimeEstimation);
        submit.Click();
        System.Threading.Thread.Sleep(1000);

        Assert.IsTrue(driver.Url.ToString().EndsWith("/Index"));

        IReadOnlyList<IWebElement> lessons = driver.FindElements(By.ClassName("card-body"));
        IWebElement lesson = lessons.Last();
        IWebElement idAndName = lesson.FindElement(By.TagName("h4"));
        String dbTitle = idAndName.Text.Split(' ')[1];
        String dbCardDeckLink = lesson.FindElement(By.TagName("a")).GetAttribute("href");
        String dbTimeEstimation = lesson.FindElement(By.TagName("span")).Text.Split(' ')[0];

        Assert.AreEqual(valTitle, dbTitle);
        Assert.AreEqual(valCardDeckLink, dbCardDeckLink);
        Assert.AreEqual(valTimeEstimation, dbTimeEstimation);

    }

    [TestMethod]
    public void inkorrekteLektionErstellen()
    {
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl("hhttps://localhost:7207/Teachingplan/Create");
        IWebElement title = driver.FindElement(By.Name("Title"));
        IWebElement cardDeckLink = driver.FindElement(By.Name("CardDeckLink"));
        IWebElement timeEstimation = driver.FindElement(By.Name("TimeEstimation"));
        IWebElement submit = driver.FindElement(By.Name("create"));

        String valTitle = "";
        String valCardDeckLink = "http://www.google.de";
        String valTimeEstimation = "3";
        
        title.SendKeys(valTitle);
        cardDeckLink.SendKeys(valCardDeckLink);
        timeEstimation.SendKeys(valTimeEstimation);
        submit.Click();

        IWebElement errorMsg = driver.FindElement(By.ClassName("text-danger"));

        Assert.AreEqual("Title is required.", errorMsg.Text);

    }

}