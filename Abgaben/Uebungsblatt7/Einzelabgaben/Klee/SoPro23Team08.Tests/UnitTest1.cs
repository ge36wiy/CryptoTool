using Microsoft.AspNetCore.Http;
using SoPro23team08.Controllers;
using SoPro23team08.ViewModels;
using SoPro23team08.Models;
using SoPro23Team08.Data;
using Moq;
using Microsoft.EntityFrameworkCore;


namespace SoPro23Team08.Tests;

public class UnitTest1
{
    //Test empty json
    [Fact]
    public void Test1()
    {
        var content = "";
        var fileName = "test.json";
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(content);
        writer.Flush();
        stream.Position = 0;

        IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);


        Mock<LessonContainer> mock = new Mock <LessonContainer>();
        mock.Setup(s => s.addJsonLessons(It.IsAny<Lesson[]>()));
        TeachingplanViewModel.lessonContainer = mock.Object;
        TeachingplanController testController = new TeachingplanController();
        testController.AddJson(file);
        mock.Verify(s => s.addJsonLessons(It.IsAny<Lesson[]>()), Times.Never);
    }

    //Test null
    [Fact]
    public void Test2()
    {
        IFormFile? file = null;
        Mock<LessonContainer> mock = new Mock <LessonContainer>();
        mock.Setup(s => s.addJsonLessons(It.IsAny<Lesson[]>()));
        TeachingplanViewModel.lessonContainer = mock.Object;
        TeachingplanController testController = new TeachingplanController();
        testController.AddJson(file);
        mock.Verify(s => s.addJsonLessons(It.IsAny<Lesson[]>()), Times.Never);    
    }

    //Test trash input
    [Fact]
    public void Test3()
    {
        var content = "{{{";
        var fileName = "test.json";
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(content);
        writer.Flush();
        stream.Position = 0;

        IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);

        Mock<LessonContainer> mock = new Mock <LessonContainer>();
        mock.Setup(s => s.addJsonLessons(It.IsAny<Lesson[]>()));
        TeachingplanViewModel.lessonContainer = mock.Object;
        TeachingplanController testController = new TeachingplanController();
        testController.AddJson(file);
        mock.Verify(s => s.addJsonLessons(It.IsAny<Lesson[]>()), Times.Never); 
    }

    //Test valid input
    [Fact]
    public void Test4()
    {
        StreamReader sr = new StreamReader("C:\\Users\\sejok\\Downloads\\Blatt6\\curriculum-cards.json");
        var fileName = "test.json";
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(sr.ReadToEnd());
        writer.Flush();
        stream.Position = 0;

        IFormFile file = new FormFile(stream, 0, stream.Length, "id_from_form", fileName);

        Mock<LessonContainer> mock = new Mock <LessonContainer>();
        mock.Setup(s => s.addJsonLessons(It.IsAny<Lesson[]>()));
        TeachingplanViewModel.lessonContainer = mock.Object;
        TeachingplanController testController = new TeachingplanController();
        testController.AddJson(file);
        mock.Verify(s => s.addJsonLessons(It.IsAny<Lesson[]>()), Times.Once()); 
    }

    //Test LessonContainer empty
    [Fact]
    public void Test5()
    {
        Lesson lesson1 = new Lesson{Id = 1, Title = "A", CardDeckLink = "http://www.google.de", TimeEstimation = 3};
        Lesson lesson2 = new Lesson{Id = 2, Title = "B", CardDeckLink = "http://www.google.de", TimeEstimation = 3};
        Lesson lesson3 = new Lesson{Id = 3, Title = "C", CardDeckLink = "http://www.google.de", TimeEstimation = 3};

        Lesson[] lessons = {lesson1, lesson2, lesson3};

        Mock<LessonContext> mock = new Mock<LessonContext>();
        mock.Setup(s => s.Update(It.IsAny<Lesson>()));
        mock.Setup(s => s.SaveChanges());
        mock.Setup(s => s.Add(It.IsAny<Lesson>()));
        mock.Setup(s => s.Find<Lesson>(It.IsAny<int>())).Returns((Lesson)null);
        //mock.Setup(s => s.ChangeTracker.Clear());
        LessonContainer.database = mock.Object;

        LessonContainer testContainer = new LessonContainer();
        testContainer.addJsonLessons(lessons);

        //mock.Verify(s => s.ChangeTracker.Clear(), Times.Never());
        mock.Verify(s => s.SaveChanges(), Times.Exactly(3));
        mock.Verify(s => s.Update(It.IsAny<Lesson>()), Times.Never());
        mock.Verify(s => s.Add(It.IsAny<Lesson>()), Times.Exactly(3));
        mock.Verify(s => s.Find<Lesson>(It.IsAny<int>()), Times.Exactly(3));    
    }
}