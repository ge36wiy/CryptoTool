using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace SoPro23Team08.Models
{
    public class Lesson
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("ID")]
        public int Id {get; set; }
        
        [Required]
        [DisplayName("Name")]
        public string Title {get; set; }

        [Url]
        [DisplayName("Link")]
        public string CardDeckLink {get; set; }

        [Required]
        [DisplayName("Dauer")]
        public TimeSpan TimeEstimation {get; set; }   
    }
}