using SoPro23team08.Models;
namespace SoPro23team08.ViewModels
{
    public class LessonDetailsViewModel
    {
        public Lesson? Lesson { get; set; }
        public string? Title { get; set; }
        public string? Header { get; set; }
    }
}