﻿
using System.ComponentModel.DataAnnotations;

namespace SoPro23Team08.Models
{
    public class Lesson
    {
       // [Display(Name = "Id")]
       // [Required(ErrorMessage = "Geben Sie eine Id ein")]
       // [Range(uint.MinValue, uint.MaxValue, ErrorMessage = "Eine Id darf nicht negativ sein")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Geben Sie einen Titel ein")]
        [MinLength(1, ErrorMessage = "Geben Sie einen Titel ein")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Geben Sie einen gültigen Link ein")]
        [Url]
        public string CardDeckLink { get; set; }
 
        [Required(ErrorMessage = "Geben Sie einen Aufwand ein")]
        [Range(0, double.MaxValue, ErrorMessage = "Ein Aufwand darf nicht negativ sein")]
        public double? TimeEstimation { get; set; }

      /*  public Lesson(string Title, string CardDeckLink, int TimeEstimation){
            this.Id = nextId++;
            this.Title = Title;
            this.CardDeckLink = CardDeckLink;
            this.TimeEstimation = TimeEstimation;
        }*/

        public string toString()
        {
            return Id + " " + Title + " " + CardDeckLink + " " + TimeEstimation;
        }
    }
}