﻿using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace Blatt4.Models
{
    public class Lesson
    {
        [Key]
        [Range(0, uint.MaxValue, ErrorMessage = "Gib eine ID ein die größer 0 ist")]
        public int Id { get; set; }

        [MinLength(1, ErrorMessage ="Darf nicht Leer sein")]
        [Required]
        public string Title{ get; set; }

        [Url(ErrorMessage ="Muss eine gültige URL sein")]
        public string CardDeckLink{ get; set; }

          [Required(ErrorMessage ="Muss angegeben werden")]
        [Range(0, uint.MaxValue, ErrorMessage = "Gib eine Zeiteinschätzung ein")]
        public string TimeEstimation { get; set; }

        public Lesson(int id, string title, string cardDeckLink, string timeEstimation)
        {
            Id = id;
            Title = title;
            CardDeckLink = cardDeckLink;
            TimeEstimation = timeEstimation;
        }
        public Lesson() { }
    }
}
