﻿using Blatt4.Models;
using Blatt4.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Blatt4.Controllers
{
    public class TeachingplanController : Controller
    {
        public LessonsContainer LessonsContainer = new LessonsContainer();

        public IActionResult Index()
        {
            return View(LessonsContainer.getLessons());
        }

        public IActionResult CreateLesson()
        {
            Lesson teachingplan = new Lesson();
            return View(teachingplan);
        }

        [HttpPost]
        public IActionResult CreateLesson(Lesson Lessonnew)
        {
            if (ModelState.IsValid)
            {
                LessonsContainer.addLesson(Lessonnew);
                return RedirectToAction("Index");
            }
            return View(Lessonnew);
            
        }

        public IActionResult EditLesson(int lessonId)
        {
            Lesson edit = LessonsContainer.getLesson(lessonId);
            if (edit == null)
            {
                return NotFound();
            }
            return View("EditLesson", edit);
        }

        [HttpPost]
        public IActionResult EditLesson(Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in LessonsContainer.getLessons())
                {
                    if (item.Id == lesson.Id)
                    {
                        item.CardDeckLink = lesson.CardDeckLink;
                        item.TimeEstimation = lesson.TimeEstimation;
                        item.Title = lesson.Title;
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(lesson);
        }

        public IActionResult RemoveLesson(int lessonId)
        {
            LessonsContainer.removeLesson(lessonId);
            return RedirectToAction("Index");
        }
    }

}
