﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SoPro23Team08.Data;
using System.Reflection.Metadata;


namespace SoPro23Team08.Models
{
    public class LessonContainer
    {
        readonly LessonContext db = new();
        public LessonContainer()
        {
            if (db.Lessons.ToList().IsNullOrEmpty())
            {
                db.Add(new Lesson()
                {
                    Id = 101,
                    Title = "C++",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.50
                });
                db.Add(new Lesson()
                {
                    Id = 102,
                    Title = "C",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 1.50
                });
                db.Add(new Lesson()
                {
                    Id = 103,
                    Title = "Bootstrap",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 3.50

                });
                db.Add(new Lesson()
                {
                    Id = 104,
                    Title = "Python",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.50
                });
                db.Add(new Lesson()
                {
                    Id = 105,
                    Title = "Java",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.25
                });
                db.SaveChanges();
            }
        }
        public DbSet<Lesson> Get()
        {
            return db.Lessons;
        }
        public DbSet<Lesson> Add(Lesson lesson)
        {
            db.Lessons.Add(lesson);
            db.SaveChanges();
            return db.Lessons;
        }
        public DbSet<Lesson> Edit(Lesson lesson)
        {
            Lesson les = db.Lessons.Find(lesson.Id);
            les.Title = lesson.Title;
            les.TimeEstimation = lesson.TimeEstimation;
            les.CardDeckLink = lesson.CardDeckLink;
            db.SaveChanges();
            return db.Lessons;
        }
        public DbSet<Lesson> Remove(int id)
        {
            db.Lessons.Remove(db.Lessons.Where(s => s.Id == id).First());
            db.SaveChanges();
            return db.Lessons;
        }

    }
}
