﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;
using System.Diagnostics.Eventing.Reader;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {

        private static List<Lesson> tpvm;
        private readonly LessonContainer lessonContainer = new();
        public TeachingplanController() => tpvm = lessonContainer.Get().ToList();
        [HttpGet]
        public IActionResult Index()
        {
            tpvm = lessonContainer.Get().ToList();
            TeachingplanViewModel teachingplanViewModel = new(tpvm);
            return View(teachingplanViewModel);
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(tpvm.Where(tpvm => tpvm.Id == id).First());
        }
       
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Lesson lesson)
        {
            lessonContainer.Add(lesson);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Delete(int id)
        {
            lessonContainer.Remove(id);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Edit(Lesson lesson)
        {
            lessonContainer.Edit(lesson);
            return RedirectToAction("Index");
        }
    }
}



