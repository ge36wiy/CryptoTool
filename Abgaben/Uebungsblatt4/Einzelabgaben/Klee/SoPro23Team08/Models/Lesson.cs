using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SoPro23team08.Models
{
    
    public class Lesson
    {
        [Required(ErrorMessage = "Geben Sie eine Id ein")]
        [Range(uint.MinValue, uint.MaxValue, ErrorMessage = "Id muss positiv sein")]
        public uint? Id { get; set; }

        [Required(ErrorMessage = "Geben Sie einen Titel ein")]
        public string? Title { get; set; }

        [Required(ErrorMessage = "Geben Sie eine gültige Url ein")]
        [Url(ErrorMessage = "Geben Sie eine gültige Url ein")]
        public string? CardDeckLink { get; set; }
 
        [Required(ErrorMessage = "Geben Sie einen Aufwand ein")]
        [Range(0.0, double.MaxValue, ErrorMessage = "Ein Aufwand darf nicht negativ sein")]
        public double? TimeEstimation { get; set; }

    }

}