using Microsoft.AspNetCore.Mvc;
using SoPro23team08.Models;
using SoPro23team08.ViewModels;

namespace SoPro23team08.Controllers
{
    public class TeachingplanController : Controller
    {

        public IActionResult Overview()
        {
            return View(TeachingplanViewModel.lessons);
        }

        public IActionResult Neuanlegen()
        {
            return View();
        }

        public IActionResult Bearbeiten(int lessonId)
        {
            var less = TeachingplanViewModel.lessons.Find(l => l.Id == lessonId);
            if(less != null){
                return View(less);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Bearbeiten(Lesson lesson)
        {
            int position = TeachingplanViewModel.lessons.FindIndex(l => l.Id == lesson.Id);
            TeachingplanViewModel.lessons[position] = lesson;
            return RedirectToAction("Overview");
        }

        public IActionResult Loeschen(int lessonId)
        {
            var less = TeachingplanViewModel.lessons.Find(l => l.Id == lessonId);
            if(less != null){
                TeachingplanViewModel.lessons.Remove(less);
            }
            return RedirectToAction("Overview");
        }

        [HttpPost]
        public ActionResult Create(Lesson lesson)
        {
            TeachingplanViewModel.lessons.Add(lesson);
            return RedirectToAction("Overview");
        }
    }
}