using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoPro23team08.Models;

namespace SoPro23team08.ViewModels
{
    public class TeachingplanViewModel
    {
        public static List<Lesson> lessons = new List<Lesson>
        {
            new Lesson{ Id = 0, Title = "Tutorial", CardDeckLink = "https://www.google.com/",  TimeEstimation = 0.2 },
            new Lesson{ Id = 1, Title = "Anfänger", CardDeckLink = "https://www.google.com/",  TimeEstimation = 1 },
            new Lesson{ Id = 2, Title = "Fortgeschrittene", CardDeckLink = "https://www.google.com/",  TimeEstimation = 5 },
        };

         public List<Lesson> getLessons(){
            return lessons;
        }
    }
}
