using Microsoft.AspNetCore.Mvc;
using SoPro23Team08.Models;
using SoPro23Team08.ViewModels;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {
       
        public IActionResult Index()
        {
           
            return View(TeachingplanViewModel.lessons);
        }

        public IActionResult CreateLesson()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateLesson(Lesson lesson)
        {
           if(lesson != null)
            {
                if (ModelState.IsValid)
                {
                    TeachingplanViewModel.lessons.Add(lesson);
                    return RedirectToAction("Index");
                }
            }
            return View(lesson);
        }

        public IActionResult RemoveLesson(int lessonId){
            var lessonToRemove = TeachingplanViewModel.lessons.Find(l => l.Id == lessonId);
            if(lessonToRemove != null)
            {
                TeachingplanViewModel.lessons.Remove(lessonToRemove);
            }
           return RedirectToAction("Index");
        }

        public IActionResult EditLesson(int lessonId)
        {
            var lessonToEdit = TeachingplanViewModel.lessons.Find(l => l.Id == lessonId);

            if(lessonToEdit == null)
            {
                return NotFound();
            }
            return View("EditLesson", lessonToEdit);
        }

        [HttpPost]
        public IActionResult EditLesson(Lesson lesson)
        {
            if (!ModelState.IsValid)
            {
                return View(lesson);
            }


            if(lesson != null)
            {
               Lesson lessonToEdit = TeachingplanViewModel.lessons.Find(l => l.Id == lesson.Id);
                if (lessonToEdit != null)
                {
                    lessonToEdit.Title = lesson.Title;
                    lessonToEdit.TimeEstimation = lesson.TimeEstimation;
                    lessonToEdit.CardDeckLink = lesson.CardDeckLink;
                }
            }
            return RedirectToAction("Index");
        }
    } 
}