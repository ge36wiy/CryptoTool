using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;


namespace SoPro23Team08.Models
{
    public class Lesson
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("ID")]
        public int Id {get; set; }
        
        [Required(ErrorMessage = "Geben Sie einen Titel ein")]
        [MinLength(1, ErrorMessage = "Geben Sie einen Titel ein")]
        [DisplayName("Name")]
        public string Title {get; set; }

        [Url]
        [DisplayName("Link")]
        [JsonProperty("url")]
        [Required(ErrorMessage = "Geben Sie einen gültigen Link ein")]
        public string CardDeckLink {get; set; }

        [DisplayName("Dauer")]
        [Required(ErrorMessage = "Geben Sie eine gültige TimeSpan Dauer ein")]
        [JsonProperty("estimate")]
        public TimeSpan TimeEstimation { get; set; }   

        public static List<Lesson> readLessons(IFormFile file)
        {
            List<Lesson> parselist = new List<Lesson>();
            if (file == null)
            {
                return parselist;
            }
            try {
                StreamReader reader = new StreamReader(file.OpenReadStream());
                string json = reader.ReadToEnd();  
                dynamic stuff = JsonConvert.DeserializeObject(json);
                foreach(var lesson in stuff){
                    var dummy = new Lesson();
                    dummy.Id = lesson.id;
                    dummy.Title = lesson.title;
                    dummy.CardDeckLink = lesson.url;
                    dummy.TimeEstimation = TimeSpan.FromHours((double)lesson.estimate);
                    parselist.Add(dummy);
                }
            } catch (Exception e) {
                return parselist;
            }
            

            //Zeile hier drunter wäre die einfache lösung, wenn man nicht manuell parsen müsste.
            //List<Lesson> parselist = JsonConvert.DeserializeObject<List<Lesson>>(json);

            return parselist;
        }

    }
}