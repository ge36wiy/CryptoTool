Cross-site request forgery ist eine Sicherheitsluecke. Angreifer imitieren Nutzer, ohne dass dieser es bemerkt. Mithilfe von Post Methoden kann man dies verhindern. 

Get Methoden -> lesen
Post Methoden -> aendern
Put Methoden -> erstellen
Delete Methoden -> loeschen
Patch Methoden -> aktualisieren
Head Methoden -> Daten erhalten
Connect -> verbindungsaufbau
Options -> optionen für anfragen verwalten?
Trace -> debugging (Anfrage wird zurück geschickt)

Die Get anfrage wäre in sofern falsch, da diese zum Datenaufruf geacht ist. 
Wie oben erwähnt könnte rein theorethisch großer schaden entstehen aufgrunf von cross-site rewquest forgery.
		