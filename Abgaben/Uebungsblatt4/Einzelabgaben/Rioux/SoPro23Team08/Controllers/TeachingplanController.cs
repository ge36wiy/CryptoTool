﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;
using System.Diagnostics.Eventing.Reader;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {
        private static List<Lesson> tpvm;
        public TeachingplanController() {
            if(tpvm == null){
                Lesson lesson1 = new Lesson {
                        Id = 101,
                        Title = "C++",
                        CardDeckLink = "https://www.google.de/",
                        TimeEstimation = 0.50
                };
                Lesson lesson2 = new Lesson {
                        Id = 102,
                        Title = "C",
                        CardDeckLink = "https://www.google.de/",
                        TimeEstimation = 1.50
                };
                Lesson lesson3 = new Lesson {
                        Id = 103,
                        Title = "Bootstrap",
                        CardDeckLink = "https://www.google.de/",
                        TimeEstimation = 3.50
                };
                Lesson lesson4 = new Lesson {
                        Id = 104,
                        Title = "Python",
                        CardDeckLink = "https://www.google.de/",
                        TimeEstimation = 0.50
                };
                Lesson lesson5 = new Lesson {
                        Id = 105,
                        Title = "Java",
                        CardDeckLink = "https://www.google.de/",
                        TimeEstimation = 0.25
                };
            tpvm = new List<Lesson> {lesson1, lesson2, lesson3, lesson4, lesson5};
            }
        }
        public IActionResult Index()
        {
            TeachingplanViewModel teachingplanViewModel = new(tpvm);
            return View(teachingplanViewModel);
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(tpvm.Where(tpvm => tpvm.Id == id).First());
        }
       
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Lesson lesson)
        {
            tpvm.Add(lesson);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Delete(int id)
        {
            tpvm.RemoveAll(tpvm => tpvm.Id == id);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Edit(Lesson lesson)
        {
            tpvm.Where(tpvm => tpvm.Id == lesson.Id).First().Title = lesson.Title;
            tpvm.Where(tpvm => tpvm.Id == lesson.Id).First().TimeEstimation = lesson.TimeEstimation;
            tpvm.Where(tpvm => tpvm.Id == lesson.Id).First().CardDeckLink = lesson.CardDeckLink;
            return RedirectToAction("Index");
        }
    }
}



