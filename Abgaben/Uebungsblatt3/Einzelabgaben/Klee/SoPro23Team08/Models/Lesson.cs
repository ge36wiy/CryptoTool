using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoPro23team08.Models
{
    
    public class Lesson
    {
        public string? Id { get; set; }
        public string? Title { get; set; }
        public string? CardDeckLink { get; set; }
        public double TimeEstimation { get; set; }
    }
}