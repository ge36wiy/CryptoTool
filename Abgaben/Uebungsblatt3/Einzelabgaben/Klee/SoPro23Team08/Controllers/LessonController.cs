using SoPro23team08.Models;
using SoPro23team08.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace SoPro23team08.Controllers
{
    public class LessonController : Controller
    {
        public ViewResult Details()
        {
            Lesson lesson = new Lesson()
                {
                Id = "0",
                Title = "Tutorial",
                CardDeckLink = "https://www.google.com/",
                TimeEstimation = 5
                };
            LessonDetailsViewModel lessonDetailsViewModel = new LessonDetailsViewModel()
            {
                Lesson = lesson,
                Title = "Lesson Details Page",
                Header = "Lesson Details",
            };

            return View(lessonDetailsViewModel);
        }
    }
}