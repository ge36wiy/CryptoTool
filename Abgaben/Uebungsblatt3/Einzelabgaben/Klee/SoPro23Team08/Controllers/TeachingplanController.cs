using Microsoft.AspNetCore.Mvc;
using SoPro23team08.Models;
using SoPro23team08.ViewModels;

namespace SoPro23team08.Controllers
{
    public class TeachingplanController : Controller
    {
        
         public List<Lesson> lessons = new List<Lesson>
        {
            new Lesson{ Id = "0", Title = "Tutorial", CardDeckLink = "https://www.google.com/",  TimeEstimation = 0.2 },
            new Lesson{ Id = "1", Title = "Anfänger", CardDeckLink = "https://www.google.com/",  TimeEstimation = 1 },
            new Lesson{ Id = "2", Title = "Fortgeschrittene", CardDeckLink = "https://www.google.com/",  TimeEstimation = 5 },
        };

        public List<Lesson> getLessons(){
            return this.lessons;
        }

        public IActionResult Overview()
        {
            TeachingplanViewModel teachingplanViewModel = new TeachingplanViewModel(lessons);
            return View(teachingplanViewModel);
        }
    }
}
