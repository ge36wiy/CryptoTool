﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SoPro23team08.Models;

namespace SoPro23team08.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Studium()
    {
        return View();
    }

    public IActionResult Lehrplan()
    {
        return View("~/Views/Teachingplan/Index.cshtml");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
