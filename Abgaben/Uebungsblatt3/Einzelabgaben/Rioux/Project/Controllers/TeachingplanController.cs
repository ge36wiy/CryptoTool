using Microsoft.AspNetCore.Mvc;
using Project.Models;


namespace Project.Controllers
{
    public class TeachingplanController : Controller
    {
        private static List<Lesson> tpvm;
        public TeachingplanController() {
            Lesson lesson1 = new Lesson {
Id = 101,
                    Title = "C++",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.50
            };
            Lesson lesson2 = new Lesson {
Id = 102,
                    Title = "C",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 1.50
            };
            Lesson lesson3 = new Lesson {
Id = 103,
                    Title = "Bootstrap",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 3.50
            };
            Lesson lesson4 = new Lesson {
                    Id = 104,
                    Title = "Python",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.50
            };
            Lesson lesson5 = new Lesson {
Id = 105,
                    Title = "Java",
                    CardDeckLink = "https://www.google.de/",
                    TimeEstimation = 0.25
            };
            tpvm = new List<Lesson> {lesson1, lesson2, lesson3, lesson4, lesson5};
        }
        public IActionResult Index()
        {
            TeachingplanViewModel teachingplanViewModel = new(tpvm);
            return View(teachingplanViewModel);
        }
        
    }
}



