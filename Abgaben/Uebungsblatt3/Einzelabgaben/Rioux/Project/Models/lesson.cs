using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class Lesson
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")] 
        public string? Title { get; set; }

        [Url(ErrorMessage = "Enter a valid Url")] 
        public string? CardDeckLink { get; set; }
        
        [Required(ErrorMessage = "Time estimation is required.")] 
        public double TimeEstimation { get; set; }
    }
}
