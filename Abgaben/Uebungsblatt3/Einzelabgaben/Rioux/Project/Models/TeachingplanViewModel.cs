using Microsoft.AspNetCore.Mvc;

namespace Project.Models
{
    public class TeachingplanViewModel
    {
        public List<Lesson>? Lesson { get; set; }
        public TeachingplanViewModel (List<Lesson> li)
        {
            Lesson = li;
        }
    }
}
