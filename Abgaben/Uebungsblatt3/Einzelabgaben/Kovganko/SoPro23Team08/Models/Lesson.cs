using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoPro23Team08.Models
{
    public class Lesson
    {
        public int Id { get; set;}
        public string Title { get; set; }
        public string CardDeckLink { get; set; }
        public double TimeEstimation { get; set; }

        //public Lesson(int Id, string Title, string CardDeckLink, int TimeEstimation){
        //    this.Id = Id;
        //    this.Title = Title;
        //    this.CardDeckLink = CardDeckLink;
        //    this.TimeEstimation = TimeEstimation;
        //}
    }
}