using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoPro23Team08.Models;

namespace SoPro23Team08.ViewModels
{
    public class TeachingplanViewModel
    {
        public List<Lesson> lessons = new List<Lesson>();
         public List<Lesson> getLessons(){
            return this.lessons;
        }

        public TeachingplanViewModel(List<Lesson> lessons){
            this.lessons = lessons;
        }
    }
}