using Microsoft.AspNetCore.Mvc;
using SoPro23Team08.Models;
using SoPro23Team08.ViewModels;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {
        
         public List<Lesson> lessons = new List<Lesson>
        {
            new Lesson{ Id = 0, Title = "Start Here![0d]", CardDeckLink = "",  TimeEstimation = 0 },
            new Lesson{ Id = 105, Title = "Ruby basics [2d]", CardDeckLink = "",  TimeEstimation = 2 },
            new Lesson{ Id = 110, Title = "Where to find API documentation [0.5d]", CardDeckLink = "",  TimeEstimation = 20 },
            new Lesson{ Id = 120, Title = "Java [30d]", CardDeckLink = "",  TimeEstimation = 30 },
            new Lesson{ Id = 888, Title = "C++ [80d]", CardDeckLink = "",  TimeEstimation = 80 },
            new Lesson{ Id = 322, Title = "HTML/CSS/JS [90d]", CardDeckLink = "",  TimeEstimation = 90},
        };

        public List<Lesson> getLessons(){
            return this.lessons;
        }

        public IActionResult Index()
        {
            TeachingplanViewModel teachingplanViewModel = new TeachingplanViewModel(lessons);
            return View(teachingplanViewModel);
        }
    }
}