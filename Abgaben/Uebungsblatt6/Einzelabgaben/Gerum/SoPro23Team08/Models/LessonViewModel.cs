using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoPro23Team08.Models
{
    public class LessonViewModel
    {
        public List<Lesson> Lessonlist { get; set; }
        public string Titel { get; set; }
    }
}