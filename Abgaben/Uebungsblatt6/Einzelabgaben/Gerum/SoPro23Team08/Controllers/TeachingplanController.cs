using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {
       private readonly MvcLessonContext _context;

        public TeachingplanController(MvcLessonContext context)
        {
            _context = context;
        }

        // GET: Movies
        public async Task<IActionResult> Index()
        {
              return View(await _context.Lesson.ToListAsync());
        }

        // GET: Movies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Lesson == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lesson == null)
            {
                return NotFound();
            }

            return View(lesson);
        }

        // GET: Teachingplan/Create
        public IActionResult Create()
        {
            return View();
        }

        // GET: Teachingplan/Read
        public IActionResult Read()
        {
            return View();
        }


        // POST: Movies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,CardDeckLink,TimeEstimation")] Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lesson);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lesson);
        }

        [HttpPost]
        public async Task<IActionResult> Read(IFormFile file)
        {
            StreamReader reader = new StreamReader(file.OpenReadStream());
            string json = reader.ReadToEnd();  
            List<Lesson> parselist = new List<Lesson>();
            dynamic stuff = JsonConvert.DeserializeObject(json);
            foreach(var lesson in stuff){
                var dummy = new Lesson();
                dummy.Id = lesson.id;
                dummy.Title = lesson.title;
                dummy.CardDeckLink = lesson.url;
                dummy.TimeEstimation = TimeSpan.FromHours((double)lesson.estimate);
                parselist.Add(dummy);
            }  
            //Zeile hier drunter wäre die einfache lösung, wenn man nicht manuell parsen müsste.
            //List<Lesson> parselist = JsonConvert.DeserializeObject<List<Lesson>>(json);


                foreach(var lesson in parselist)
                {
                    var lessonexist = _context.Lesson.Any(x => x.Id == lesson.Id);
                    if(lessonexist)
                    {
                        try
                        {
                            _context.Update(lesson);
                            await _context.SaveChangesAsync(); //muss vermutlich mit batch updates arbeiten, update funktioniert nicht, ich gehe davon aus dass es an async liegt, habe aber keine zeit mehr :(
                        }
                        catch (DbUpdateConcurrencyException) 
                        {       
                            if (!LessonExists(lesson.Id))
                            {
                                return NotFound();
                            }
                            else
                            {
                                throw;
                            }
                        }
                        return RedirectToAction(nameof(Index));
                        
                    } else {
                        _context.Add(lesson);
                        await _context.SaveChangesAsync();
                    }
                }
            return RedirectToAction("Index");
        }

        // GET: Movies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Lesson == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson.FindAsync(id);
            if (lesson == null)
            {
                return NotFound();
            }
            return View(lesson);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,CardDeckLink,TimeEstimation")] Lesson lesson)
        {
            if (id != lesson.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lesson);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LessonExists(lesson.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lesson);
        }

        // GET: Movies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Lesson == null)
            {
                return NotFound();
            }

            var lesson = await _context.Lesson
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lesson == null)
            {
                return NotFound();
            }

            return View(lesson);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Lesson == null)
            {
                return Problem("Entity set 'MvcLessonContext.Lesson'  is null.");
            }
            var lesson = await _context.Lesson.FindAsync(id);
            if (lesson != null)
            {
                _context.Lesson.Remove(lesson);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LessonExists(int id)
        {
          return _context.Lesson.Any(e => e.Id == id);
        }
    }
}
