using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SoPro23Team08.Models;

    public class MvcLessonContext : DbContext
    {
        public MvcLessonContext (DbContextOptions<MvcLessonContext> options)
            : base(options)
        {
        }

        public DbSet<SoPro23Team08.Models.Lesson> Lesson { get; set; } = default!;
    }
