using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoPro23Team08.Data;

namespace SoPro23team08.Models
{
    public class LessonContainer
    {
        private static LessonContext database = new LessonContext();

        public void addLesson(Lesson lesson)
        {
            database.Add(lesson);
            database.SaveChanges();
        }

        public void removeLesson(int Id)
        {
            Lesson? less = database.Find<Lesson>(Id);
            if (less != null)
            {
                database.Remove(less);
                database.SaveChanges();
            }
        }

        public void updateLesson(Lesson lesson)
        {
            if (lesson != null)
            {
                database.ChangeTracker.Clear();
                database.Update(lesson);
                database.SaveChanges();
            }
        }

        public Lesson? getLesson(int id)
        {
            return database.Find<Lesson>(id);
        }

        public List<Lesson> getLessons()
        {
            return database.Lessons.ToList();
        }

        public void addJsonLessons(Lesson[]? lessons)
        {
            if (lessons == null)
            {
                return;
            }
            foreach (Lesson lesson in lessons)
            {
                if (getLesson(lesson.Id) == null)
                {
                    addLesson(lesson);
                }
                else
                {
                    updateLesson(lesson);
                }
            }
        }
    }
}