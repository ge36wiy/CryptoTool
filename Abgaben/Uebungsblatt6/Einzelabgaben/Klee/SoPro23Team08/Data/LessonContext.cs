using SoPro23team08.Models;
using Microsoft.EntityFrameworkCore;

namespace SoPro23Team08.Data
{
    public class LessonContext : DbContext
    {
        public DbSet<Lesson> Lessons { get; set; }
        public string DbPath { get; }
        public LessonContext()
        {
            var folder = Directory.GetCurrentDirectory();
            var path = System.IO.Path.Join(folder, "persistence");


            DbPath = System.IO.Path.Join(path, "lesson.db");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");
    }

}