using Microsoft.AspNetCore.Mvc;
using SoPro23team08.Models;
using SoPro23team08.ViewModels;
using Newtonsoft.Json;

namespace SoPro23team08.Controllers
{
    public class TeachingplanController : Controller
    {

        public IActionResult Overview()
        {
            return View(TeachingplanViewModel.lessonContainer.getLessons());
        }

        public IActionResult Neuanlegen()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Lesson lesson)
        {
            TeachingplanViewModel.lessonContainer.addLesson(lesson);
            return RedirectToAction("Overview");
        }

        public IActionResult Bearbeiten(int lessonId)
        {
            var less = TeachingplanViewModel.lessonContainer.getLesson(lessonId);
            if (less != null)
            {
                return View(less);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult Bearbeiten(Lesson lesson)
        {
            TeachingplanViewModel.lessonContainer.updateLesson(lesson);
            return RedirectToAction("Overview");
        }

        public IActionResult Loeschen(int lessonId)
        {
            TeachingplanViewModel.lessonContainer.removeLesson(lessonId);
            return RedirectToAction("Overview");
        }

        public IActionResult AddJson()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddJson(IFormFile jsonFile)
        {
            if (jsonFile != null && jsonFile.Length > 0)
            {
                try
                {
                    using (StreamReader streamReader = new StreamReader(jsonFile.OpenReadStream()))
                    {
                        var inhalt = streamReader.ReadToEnd();
                        Lesson[]? inputs = JsonConvert.DeserializeObject<Lesson[]>(inhalt);
                        TeachingplanViewModel.lessonContainer.addJsonLessons(inputs);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Es gab einen Fehler beim lesen der json Datei");
                }
            }
            return RedirectToAction("Overview");
        }
    }
}