﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.Internal;
using SoPro23Team08.Models;
using System.Diagnostics.Eventing.Reader;
using System.Text.Json;
using System;


namespace SoPro23Team08.Controllers
{

    public class TeachingplanController : Controller
    {
        private IWebHostEnvironment Environment;
        private static List<Lesson> tpvm;
        private readonly LessonContainer lessonContainer = new();
        public TeachingplanController(IWebHostEnvironment _environment)
        {
            Environment = _environment;
            tpvm = lessonContainer.Get().ToList();
        }
        [HttpGet]
        public IActionResult Index()
        {
            tpvm = lessonContainer.Get().ToList();
            TeachingplanViewModel teachingplanViewModel = new(tpvm);
            return View(teachingplanViewModel);
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(tpvm.Where(tpvm => tpvm.Id == id).First());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Lesson lesson)
        {
            lessonContainer.Add(lesson);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Delete(int id)
        {
            lessonContainer.Remove(id);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Edit(Lesson lesson)
        {
            lessonContainer.Edit(lesson);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult AddJson(upload model)
        {
            if (model.MyJson != null)
            {                
                try
                {
                    var uniqueFileName = GetUniqueFileName(model.MyJson.FileName);
                    var uploads = Path.Combine(Environment.WebRootPath, "uploads");
                    var filePath = Path.Combine(uploads, uniqueFileName);
                    FileStream fileStream = new FileStream(filePath, FileMode.Create);
                    model.MyJson.CopyTo(fileStream);
                    fileStream.Close();
                    StreamReader r = new StreamReader(filePath);
                    string jsonString = r.ReadToEnd();
                    r.Close();
                    Lesson[] list = JsonSerializer.Deserialize<Lesson[]>(jsonString)!;
                    foreach (var item in list)
                    {
                        if(lessonContainer.exsits(item))
                        {
                            lessonContainer.Edit(item);
                        } else {
                            lessonContainer.Add(item);
                        }
                    }
                    System.IO.File.Delete(filePath);
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult AddJson()
        {
            return View();
        }
        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }
    }
}



