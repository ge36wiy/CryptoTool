﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SoPro23Team08.Models
{
    public class Lesson
    {
        [Key]
        [Range(0, uint.MaxValue, ErrorMessage = "id needs to be greater than 0")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")] 
        public string? Title { get; set; }

        [Url(ErrorMessage = "Enter a valid Url")] 
        public string? CardDeckLink { get; set; }
        
        [Required(ErrorMessage = "Time estimation is required.")] 
        public double TimeEstimation { get; set; }
    }
}
