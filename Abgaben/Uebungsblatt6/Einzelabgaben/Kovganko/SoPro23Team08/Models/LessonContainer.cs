﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SoPro23Team08.Data;
using System.Reflection.Metadata;

namespace SoPro23Team08.Models
{
    public class LessonContainer
    {
        private static LessonContext db = new LessonContext();   
        public List<Lesson> getLessons()
        {
            return db.Lessons.ToList(); 
        }

        public void addLesson(Lesson lesson) {
            db.Add(lesson);
            db.SaveChanges();
            Console.WriteLine("New lesson with id " + lesson.Id + " inserted");
        }

        public void RemoveLesson(int id)
        {
            Lesson? lToRemove = db.Find<Lesson>(id);
            if(lToRemove != null) { 
                db.Remove(lToRemove);      
            }
            db.SaveChanges();
            Console.WriteLine("Lesson with id " + id + " removed");
            Lesson? idOne = db.Find<Lesson>(1);
        }

        public void UpdateLesson(Lesson lesson)
        {
            if(lesson != null)
            {
                db.ChangeTracker.Clear();
                db.Update(lesson);    
                db.SaveChanges();
                Console.WriteLine("Lesson with id " + lesson.Id + " updated");
            }
        }

        public Lesson? getLesson(int id) {
            return db.Find<Lesson>(id);
        }

        public Boolean containsLesson(int id) { 
            return db.Find<Lesson>(id) == null ? false : true;   
        }
    }
}
