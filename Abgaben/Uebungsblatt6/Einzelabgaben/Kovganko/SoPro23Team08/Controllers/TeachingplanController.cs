using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SoPro23Team08.Models;
using SoPro23Team08.ViewModels;

namespace SoPro23Team08.Controllers
{
    public class TeachingplanController : Controller
    {
       
        public IActionResult Index()
        {
            return View(TeachingplanViewModel.lessonContainer.getLessons());
        }

        public IActionResult CreateLesson()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateLesson(Lesson lesson)
        {
           if(lesson != null)
            {
                if (ModelState.IsValid)
                {
                    TeachingplanViewModel.lessonContainer.addLesson(lesson);    
                    return RedirectToAction("Index");
                }
            }
            return View(lesson);
        }

        public IActionResult RemoveLesson(int lessonId){
            TeachingplanViewModel.lessonContainer.RemoveLesson(lessonId);  
         
           return RedirectToAction("Index");
        }

        public IActionResult EditLesson(int lessonId)
        {
            var lessonToEdit = TeachingplanViewModel.lessonContainer.getLesson(lessonId);   

            if(lessonToEdit == null)
            {
                return NotFound();
            }
            return View("EditLesson", lessonToEdit);
        }

        [HttpPost]
        public IActionResult EditLesson(Lesson lesson)
        {
            if (!ModelState.IsValid)
            {
                return View(lesson);
            }


            if(lesson != null)
            {
                TeachingplanViewModel.lessonContainer.UpdateLesson(lesson);
            }
          
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadJson(IFormFile jsonFile)
        {
            if(jsonFile != null && jsonFile.Length > 0 && jsonFile.ContentType == "application/json")
            {
                using(var streamReader = new StreamReader(jsonFile.OpenReadStream()))
                {
                    var jsonString = streamReader.ReadToEnd();
                    Lesson[]? lessons = JsonConvert.DeserializeObject<Lesson[]>(jsonString);
                    TeachingplanViewModel.lessonInsertOrUpdateIfExist(lessons); 
                    
                }
            }
            return RedirectToAction("Index");
        }

    } 
}