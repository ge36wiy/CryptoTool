using SoPro23Team08.Models;

namespace SoPro23Team08.ViewModels
{
    public class TeachingplanViewModel
    {
        public static LessonContainer lessonContainer = new LessonContainer(); 

        public static void lessonInsertOrUpdateIfExist(Lesson[] lessons)
        {
            foreach(Lesson l in lessons){
                if(lessonContainer.containsLesson(l.Id)) { 
                    lessonContainer.UpdateLesson(l);
                }
                else
                {
                    lessonContainer.addLesson(l);
                }
            }
        }
    }

   
}