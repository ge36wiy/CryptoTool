﻿/*using Blatt4.Models;
using Blatt4.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Blatt4.Controllers
{
    public class TeachingplanController : Controller
    {
        public static List<Lesson> lessonsnew = new List<Lesson>();
        public IActionResult Index()
        {
            return View(lessonsnew);
        }

        public IActionResult CreateLesson()
        {
            Lesson teachingplan = new Lesson();
            return View(teachingplan);
        }

        [HttpPost]
        public IActionResult CreateLesson(Lesson Lessonnew)
        {
            if (ModelState.IsValid)
            {
                lessonsnew.Add(Lessonnew);
                return RedirectToAction("Index");
            }
            return View(Lessonnew);

        }

        public IActionResult EditLesson(int lessonId)
        {
            var edit = lessonsnew.Find(x => lessonId == x.Id);
            if (edit == null)
            {
                return NotFound();
            }
            return View("EditLesson", edit);
        }

        [HttpPost]
        public IActionResult EditLesson(Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                if (lesson != null)
                {
                    foreach (var item in lessonsnew)
                    {
                        if (item.Id == lesson.Id)
                        {
                            item.CardDeckLink = lesson.CardDeckLink;
                            item.TimeEstimation = lesson.TimeEstimation;
                            item.Title = lesson.Title;
                            return RedirectToAction("Index");
                        }
                    }

                }

            }
            return View(lesson);
        }

        public IActionResult RemoveLesson(int lessonId)
        {
            var edit = lessonsnew.Find(x => lessonId == x.Id);
            if (edit == null)
            {
                return NotFound();
            }
            else
            {
                lessonsnew.Remove(edit);
            }
            return RedirectToAction("Index");
        }
    }

}
*/

