﻿using Blatt4.Models;
using Blatt4.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.IO;
using System.Text.Json.Serialization;
using Microsoft.Identity.Client;

namespace Blatt4.Controllers
{
    public class TeachingplanController : Controller
    {
        public LessonsContainer LessonsContainer = new LessonsContainer();

        public IActionResult Index()
        {
            return View(LessonsContainer.getLessons());
        }

        public IActionResult CreateLesson()
        {
            Lesson teachingplan = new Lesson();
            return View(teachingplan);
        }

        [HttpPost]
        public IActionResult CreateLesson(Lesson Lessonnew)
        {
            if (ModelState.IsValid)
            {
                LessonsContainer.addLesson(Lessonnew);
                return RedirectToAction("Index");
            }
            return View(Lessonnew);
            
        }

        public IActionResult EditLesson(int lessonId)
        {
            Lesson edit = LessonsContainer.getLesson(lessonId);
            if (edit == null)
            {
                return NotFound();
            }
            return View("EditLesson", edit);
        }

        [HttpPost]
        public IActionResult EditLesson(Lesson lesson)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in LessonsContainer.getLessons())
                {
                    if (item.Id == lesson.Id)
                    {
                        item.CardDeckLink = lesson.CardDeckLink;
                        item.TimeEstimation = lesson.TimeEstimation;
                        item.Title = lesson.Title;
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(lesson);
        }

        public IActionResult RemoveLesson(int lessonId)
        {
            LessonsContainer.removeLesson(lessonId);
            return RedirectToAction("Index");
        }



        [HttpPost]
        public IActionResult ImportFile(IFormFile Read)
        {


                try
                {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                StreamReader sr = new StreamReader(Read.OpenReadStream());
                    
                        string text = sr.ReadToEnd();
                        List<Lesson> list = new List<Lesson>();
                
                Lesson[] a = JsonSerializer.Deserialize<Lesson[]>(text);

                
                foreach (Lesson abc in a)
                {
                    if (!LessonsContainer.ContainsLesson(abc))
                    {
                        LessonsContainer.addLesson(abc);
                    }
                    else // Edit existing Lessons
                    {
                        foreach (Lesson abc2 in LessonsContainer.getLessons())
                        {
                            if (abc2.Id == abc.Id)
                            {
                                abc2.Title = abc.Title;
                                abc2.CardDeckLink = abc.CardDeckLink;
                                abc2.TimeEstimation = abc.TimeEstimation;
                            }
                        }
                    }
                }


                return RedirectToAction("Index");

            }
            catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                    return RedirectToAction("Index");
                }
            
        }
    }

}
