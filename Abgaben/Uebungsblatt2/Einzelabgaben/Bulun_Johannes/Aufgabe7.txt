Alle Parameter zum Befehl: https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-new-sdk-templates#web-options

Default Values:
-au --> None
--aad-b2c-instance <INSTANCE> --> https://login.microsoftonline.com/tfp/.
--aad-instance <INSTANCE> --> https://login.microsoftonline.com/.
--client-id <ID> --> 11111111-1111-1111-11111111111111111.
--domain <DOMAIN> --> qualified.domain.name
...

c)
-au|--auth <AUTHENTICATION_TYPE>
Art der Authentifizierung

--aad-b2c-instance <INSTANCE>
Spezieller Modus für Authentifizierung b2c

--aad-b2c-instance <INSTANCE>
Spezieller Modus für Authentifizierung
-rp|--reset-password-policy-id <ID>
Spezieller Modus für Authentifizierung

-ep|--edit-profile-policy-id <ID>
Spezieller Modus für Authentifizierung

--aad-instance <INSTANCE>
Spezieller Modus für Authentifizierung
--client-id <ID>
Spezieller Modus für Authentifizierung
--domain <DOMAIN>
Spezieller Modus für Authentifizierung
--tenant-id <ID>
Spezieller Modus für Authentifizierung
--callback-path <PATH>
Spezieller Modus für Authentifizierung
-r|--org-read-access
Spezieller Modus für Authentifizierung
--exclude-launch-settings
exkludiert die Settings Datei
-uld|--use-local-db
LocalDB anstatt SQLite
-f|--framework <FRAMEWORK>
welches Framework (Version von .net)
--no-restore
Kein Restore beim ertstellen
--use-browserlink
Browserlink für alte .Net versionen


-rrc|--razor-runtime-compilation

--kestrelHttpPort

--kestrelHttpsPort

--use-program-main