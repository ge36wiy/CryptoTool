

namespace SoPro23Team08.Models
{
    public class Lesson
    {
        public Lesson(int Id, string Title, string CardDeckLink, string TimeEstimation){
            this.Id = Id;
            this.Title = Title;
            this.CardDeckLink = CardDeckLink;
            this.TimeEstimation = TimeEstimation;
        }
        public int Id{get; set;}
        public string ?Title{get; set;}
        public string ?CardDeckLink{get;set;}
        public string ?TimeEstimation{get;set;}
        
    }
}