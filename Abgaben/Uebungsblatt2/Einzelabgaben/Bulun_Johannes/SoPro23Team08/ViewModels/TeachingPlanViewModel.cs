using SoPro23Team08.Models;
using SoPro23Team08.Controllers;

namespace SoPro23Team08.ViewModels
{
    public class TeachingPlanViewModel
    {
            public List<Lesson> Lesson{get; set;}
            
            public TeachingPlanViewModel(List<Lesson> lessons) {
                this.Lesson = lessons;
            }
            public List<Lesson> GetLessons(){
                return this.Lesson;
            }
    }
}