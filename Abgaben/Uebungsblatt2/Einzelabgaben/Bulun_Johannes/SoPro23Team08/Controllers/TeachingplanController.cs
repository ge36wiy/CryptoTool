using SoPro23Team08.Models;
using System.Collections.Generic;
using SoPro23Team08.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace SoPro23Team08.Controllers
{
    public class TeachingPlanController : Controller
    {
        List<Lesson> lessonList = new List<Lesson>{
            new Lesson(000, "Start here!", "", "[0d]"),
            new Lesson(105, "Ruby basics", "", "[2d]"),
            new Lesson(110, "Where to find API documentation", "", "[0.5d]"),
            new Lesson(110, "Where to find API documentation", "", "[0.5d]"),
            new Lesson(110, "Where to find API documentation", "", "[0.5d]"),
            new Lesson(110, "Where to find API documentation", "", "[0.5d]")
        };

        public List<Lesson> GetLessons(){
            return this.lessonList;
        }

        public IActionResult Index(){
            TeachingPlanViewModel teachingPlanViewModel = new TeachingPlanViewModel(lessonList);
            return View(teachingPlanViewModel);
        }
    }
}