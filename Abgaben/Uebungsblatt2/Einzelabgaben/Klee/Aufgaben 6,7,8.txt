Aufgabe 6:
	-das Projekt buildet weiterhin normal
Aufgabe 7:
  -au;
      None;
      Der zu verwendende Authentifizierungstyp
  --aad-b2c-instance;
      https://login.microsoftonline.com/tfp/;
      Die Azure Active Directory B2C-Instanz, mit der eine Verbindung hergestellt werden soll
  -ssp;
      b2c_1_susi;
      Die Anmelde und Registrierungsrichtlinien ID für dieses Projekt
  -socp;
      /signout/B2C_1_susi;
      Der Rückruf für die globale Abmeldung
  -rp;
      b2c_1_reset;
      Die Richtlinien ID zum Zurücksetzen des Kennworts für dieses Projekt
  -ep;
      b2c_1_edit_profile;
      Die Bearbeitungsprofil-Richtlinien-ID für dieses Projekt
  --aad-instance;
      https://login.microsoftonline.com/;
      Die Azure Active Directory-Instanz, mit der eine Verbindung hergestellt werden soll
  --client-id;
      11111111-1111-1111-11111111111111111;
      Die Client ID für dieses Projekt
  --domain;
      qualified.domain.name;
      Die Domäne für den Verzeichnismandanten
  --tenant-id;
      22222222-2222-2222-2222-222222222222;
      Die TenantId ID des Verzeichnisses, mit dem eine Verbindung hergestellt werden soll
  --callback-path;
      /signin-oidc;
      Der Anforderungspfad innerhalb des Basispfads der Anwendung des Umleitungs-URI
  -r; 
      false;
      Ob dieser Anwendung Lesezugriff auf das Verzeichnis gewährt werden soll oder nicht
  --exclude-launch-settings;
      false;
      Ob launchSettings.json aus der generierten Vorlage ausgeschlossen werden soll
  --no-https;
      false;
      Ob HTTPS deaktiviert werden soll
  -uld;
      false;
      Ob LocalDB anstelle von SQLite verwendet werden soll
  -f;
      net8.0;
      Das Zielframework für das Projekt
  --no-restore;
      false;
      Wenn angegeben, wird die automatische Wiederherstellung des Projekts beim Erstellen übersprungen
  --called-api-url;
      https://graph.microsoft.com/v1.0;
      URL der API, die von der Web-App aufgerufen werden soll
  --calls-graph;
      false;
      Gibt an, ob die Web-App Microsoft Graph aufruft
  --called-api-scopes;
      user.read;
      Anzufordernde Bereiche zum Aufrufen der API von der Web-App
  --use-program-main;
      false;
      Gibt an, ob anstelle von Anweisungen der obersten Ebene eine explizite Programmklasse und eine Main-Methode generiert werden soll
  -n;
      Name des Ausgabeverzeichnisses;
      Der Name für die erstellte Ausgabe
  -o;
      aktuelles Verzeichnis;
      Speicherort für die generierte Ausgabe
  --dry-run;
    false;
    Zeigt eine Zusammenfassung der Ergebnisse der Ausführung der angegebenen Befehlszeile an, wenn dies zu einer Vorlagenerstellung führen würde.
  --force;
      false;
      Erzwingt, dass der Inhalt generiert wird, auch wenn vorhandene Dateien geändert würden.
  --no-update-check;
      false;
      Deaktiviert die Suche nach Vorlagenpaketaktualisierungen beim Instanziieren einer Vorlage.
  --project;
      aktuelles Verzeichnis;
      Das Projekt, das für die Kontextauswertung verwendet werden soll.
  -lang;
      C#;
      Gibt die zu instanziierende Vorlagensprache an.
  --type;
      mvc;
      Gibt den zu instanziierende Vorlagentyp an.