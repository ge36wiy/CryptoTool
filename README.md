Sebastian Klee: Sebastian Klee und ge36wiy

Projekt: 

Code ist im Projektordner auf Toplevelebene, Code für die Tests liegt im Testordner
Portainer: http://sopro.isse.de:49835

User anlegen und Verwalten geht über den admin User: admin  PW: Team08
Es gibt einen Testkunden und einen Teststeuerberater, um E2E Tests zu realisieren


Wichtige Infos:
Transaktionsverwaltung: Preise etc. werden dynamisch per Javascript befüllt, ebenso werden die Zahlformatierungen durch Javascript umgesetzt

Admin: Um User zu entfernen, müssen zuerst alle Zuordnungen des Users entfernt werden

Zugangsdaten:
    Kunden:
        Kennung: bergmann
        Passwort: Bergmann123!"§

        Kennung: spekulatius
        Passwort: Spekulatius123!"§

        Kennung: neuberg
        Passwort: Neuberg123!"§

    Steuerberater:
        Kennung: fiskus
        Passwort: Fiskus123!"§

        Kennung: geier
        Passwort: Geier123!"§
